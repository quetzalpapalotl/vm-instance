﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoToTraining.Api.Components
{
    public class Training
    {
        public Training()
        {
            Times = new List<SessionTime>();
            Trainers = new List<Organizer>();
        }
        public string Name { get; set; }
        public string TrainingKey { get; set; }
        public string TimeZone { get; set; }
        public string Description { get; set; }
        public List<SessionTime> Times { get; set; }
        public List<Organizer> Trainers { get; set; }
    }
}
