﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoToTraining.Api.Components
{
    public class Attendee
    {
        public string Surname { get; set; }
        public string GivenName { get; set; }
        public string Email { get; set; }
        public int TimeInSession { get; set; }

        // values from the session
        public string SessionKey { get; set; }
        public DateTime SessionStartTime { get; set; }
        public DateTime SessionEndTime { get; set; }
        public string TrainingKey { get; set; }
        public string TrainingName { get; set; }

        internal void MapSession(Session session, string trainingKey)
        {
            SessionKey = session.SessionKey;
            SessionStartTime = session.SessionStartTime;
            SessionEndTime = session.SessionEndTime;
            TrainingKey = session.TrainingKey;
            TrainingName = session.TrainingName;
        }
    }
}
