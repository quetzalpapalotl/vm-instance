﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Data;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;

using OData = Omnicell.Data.Model;

namespace Omnicell.Custom.Components
{
    public interface IMediaFormatService
    {
        MediaFormat Get(int id);
        MediaFormat Save(MediaFormat format);
        IList<MediaFormat> List();
        IList<MediaFormat> List(int coursePostId);
        void Delete(int id);
    }
    public class MediaFormatService : IMediaFormatService
    {
        private readonly ICacheService _cache = null;

        public MediaFormatService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
        }

        public MediaFormat Get(int id)
        {
            MediaFormat format = GetMediaFormatFromCache(id);
            if (format == null)
            {
                format = MediaFormatDataProvider.Instance.Get(id);
                if (format != null)
                    PushMediaFormatToCache(format);
            }
            return format;
        }
        public MediaFormat Save(MediaFormat format)
        {
            RemoveMediaFormatListFromCache();
            int id = format.Id;
            if (id > 0)
            {
                RemoveMediaFormatFromCache(format.Id);
                MediaFormatDataProvider.Instance.Update(format);
            }
            else
            {
                id = MediaFormatDataProvider.Instance.Add(format);
            }

            // keep the custom db in sync
            using (var db = new OData.OmnicellEntities())
            {
                var dbEntity = db.Omnicell_MediaFormat.SingleOrDefault(x => x.Id == format.Id);
                if (dbEntity == null)
                {
                    dbEntity = new OData.Omnicell_MediaFormat();
                    db.Omnicell_MediaFormat.AddObject(dbEntity);
                }

                dbEntity.Value = format.Value;
                db.SaveChanges();
            }

            return Get(id);
        }
        public IList<MediaFormat> List()
        {
            IList<MediaFormat> list = GetMediaFormatListFromCache();
            if (list == null)
            {
                var newList = MediaFormatDataProvider.Instance.List();
                list = new PagedList<MediaFormat>(newList);
                if (list.Count > 0)
                    PushMediaFormatListToCache(list);
            }
            return list;
        }
        public IList<MediaFormat> List(int galleryId)
        {
            IList<MediaFormat> list = GetMediaFormatListFromCache(galleryId);
            if (list == null)
            {
                Gallery gallery = PublicApi.Galleries.Get(new GalleriesGetOptions { Id = galleryId });
                if (gallery != null)
                {
                    list = gallery.MediaFormatFilters().ToList();

                    if (list != null && list.Count > 0)
                        PushMediaFormatListToCache(galleryId, list);
                }
            }
            return list;
        }
        public void Delete(int id)
        {
            RemoveMediaFormatListFromCache();
            RemoveMediaFormatFromCache(id);
            MediaFormatDataProvider.Instance.Delete(id);

            // keep the custom db in sync
            using (var db = new OData.OmnicellEntities())
            {
                var dbEntity = db.Omnicell_MediaFormat.SingleOrDefault(x => x.Id == id);
                if (dbEntity != null)
                {
                    db.DeleteObject(dbEntity);
                    db.SaveChanges();
                }
            }
        }

        #region Cache

        private string GetMediaFormatListCacheKey(int galleryId)
        {
            return string.Format("GALLERY::{0}", galleryId);
        }
        private string GetMediaFormatListCacheKey(string key)
        {
            if (string.IsNullOrEmpty(key))
                key = "ALL";
            return string.Format("PK_FORMAT_FILTER::LIST-{0}", key);
        }
        private string GetMediaFormatCacheKey(int id)
        {
            return string.Format("PK_FORMAT_FILTER::{0}", id);
        }

        private void PushMediaFormatListToCache(IList<MediaFormat> filters)
        {
            PushMediaFormatListToCache("ALL-FORMATS", filters);
        }
        private void PushMediaFormatListToCache(int galleryId, IList<MediaFormat> list)
        {
            PushMediaFormatListToCache(GetMediaFormatListCacheKey(galleryId), list);
        }
        private void PushMediaFormatListToCache(string key, IList<MediaFormat> filters)
        {
            _cache.Put(GetMediaFormatListCacheKey(key), filters, CacheScope.All);
        }
        private void PushMediaFormatToCache(MediaFormat format)
        {
            _cache.Put(GetMediaFormatCacheKey(format.Id), format, CacheScope.All);
        }

        private IList<MediaFormat> GetMediaFormatListFromCache()
        {
            return GetMediaFormatListFromCache("ALL-FORMATS");
        }
        private IList<MediaFormat> GetMediaFormatListFromCache(int galleryId)
        {
            return GetMediaFormatListFromCache(GetMediaFormatListCacheKey(galleryId)) as IList<MediaFormat>;
        }
        private IList<MediaFormat> GetMediaFormatListFromCache(string key)
        {
            return _cache.Get(GetMediaFormatListCacheKey(key), CacheScope.All) as IList<MediaFormat>;
        }
        private MediaFormat GetMediaFormatFromCache(int id)
        {
            return _cache.Get(GetMediaFormatCacheKey(id), CacheScope.All) as MediaFormat;
        }

        private void RemoveMediaFormatListFromCache()
        {
            RemoveMediaFormatListFromCache("ALL-FORMATS");
        }
        private void ReomveMediaFormatListFromCache(int galleryId)
        {
            RemoveMediaFormatListFromCache(GetMediaFormatListCacheKey(galleryId));
        }
        private void RemoveMediaFormatListFromCache(string key)
        {
            _cache.Remove(GetMediaFormatListCacheKey(key), CacheScope.All);
        }
        private void RemoveMediaFormatFromCache(int id)
        {
            _cache.Remove(GetMediaFormatCacheKey(id), CacheScope.All);
        }

        #endregion Cache

    }
}
