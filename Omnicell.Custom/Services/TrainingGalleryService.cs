﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

namespace Omnicell.Custom.Components
{
    public interface ITrainingGalleryService
    {
        TrainingGalleryFilters GetFilters(int trainingGalleryId);
        void SaveFilters(TrainingGalleryFilters filters);
    }
    public class TrainingGalleryService : ITrainingGalleryService
    {
        private readonly ICacheService _cache = null;
        private IMediaFormatService _svcMediaFormats = null;
        private ICourseTypeService _svcCourseTypes = null;

        public TrainingGalleryService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
            _svcMediaFormats = new MediaFormatService();
            _svcCourseTypes = new CourseTypeService();
        }

        public TrainingGalleryFilters GetFilters(int trainingGalleryId)
        {
            TrainingGalleryFilters filters = GetTrainingGalleryFiltersFromCache(trainingGalleryId);
            if (filters == null)
            {
                var gallery = PublicApi.Galleries.Get(new GalleriesGetOptions { Id = trainingGalleryId });
                if (gallery != null)
                {
                    filters = new TrainingGalleryFilters{GalleryId = trainingGalleryId};

                    filters.GalleryType = gallery.GalleryType();
                    filters.ShowCourseInfo = gallery.ShowCourseInfo();
                    filters.CourseDateType = gallery.CourseDateType();

                    PutTrainingGalleryFiltersInCache(filters);

                    filters.MediaFormatFilters = new List<MediaFormat>(gallery.MediaFormatFilters());
                    filters.CourseTypeFilters = new List<CourseType>(gallery.CourseTypeFilters());
                }

            }
            return filters;
        }
        public void SaveFilters(TrainingGalleryFilters filters)
        {
            RemoveTrainingGalleryFiltersFromCache(filters.GalleryId);
            var gallery = PublicApi.Galleries.Get(new GalleriesGetOptions { Id = filters.GalleryId });
            if (gallery != null)
            {
                List<MediaFormat> formatList = new List<MediaFormat>();
                List<CourseType> typeList = new List<CourseType>();

                if (filters.MediaFormatFilters != null)
                    filters.MediaFormatFilters.ForEach(f => formatList.Add(_svcMediaFormats.Get(f.Id)));
                if (filters.CourseTypeFilters != null)
                    filters.CourseTypeFilters.ForEach(c => typeList.Add(_svcCourseTypes.Get(c.Id)));

                GalleriesUpdateOptions options = new GalleriesUpdateOptions();
                options.MediaFormatFilters(formatList.OrderBy(f=> f.Value).ToList());
                options.CourseTypeFilters(typeList.OrderBy(t=>t.Value).ToList());

                options.GalleryType(filters.GalleryType);
                options.ShowCourseInfo(filters.ShowCourseInfo);
                options.CourseDateType(filters.CourseDateType);

                PublicApi.Galleries.Update(filters.GalleryId, options);

            }
        }

        #region Caching

        public string GetTrainingGalleryFilterCacheKey(int trainingGalleryId)
        {
            return string.Format("TRAINING-GALLERY-FILTERS:{0}", trainingGalleryId);
        }
        public TrainingGalleryFilters GetTrainingGalleryFiltersFromCache(int trainingGalleryId)
        {
            return _cache.Get(GetTrainingGalleryFilterCacheKey(trainingGalleryId), CacheScope.All) as TrainingGalleryFilters;
        }
        public void PutTrainingGalleryFiltersInCache(TrainingGalleryFilters filters)
        {
            _cache.Put(GetTrainingGalleryFilterCacheKey(filters.GalleryId), filters, CacheScope.All);
        }
        public void RemoveTrainingGalleryFiltersFromCache(int trainingGalleryId)
        {
            _cache.Remove(GetTrainingGalleryFilterCacheKey(trainingGalleryId), CacheScope.All);
        }

        #endregion Caching

    }
}
