﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Components;
using Omnicell.Custom.Data;
using Omnicell.Custom.Search;

using OData = Omnicell.Data.Model;

namespace Omnicell.Custom.Components
{
    public interface IAudienceTypeService
    {
        AudienceType Get(int id);
        AudienceType Save(AudienceType audienceType);
        IList<AudienceType> List();
        void Delete(int id);
    }
    public class AudienceTypeService : IAudienceTypeService
    {
        public readonly ICacheService _cache = null;

        public AudienceTypeService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
        }

        public AudienceType Get(int id)
        {
            AudienceType type = GetAudienceTypeFromCache(id);
            if (type == null)
            {
                type = AudienceTypeDataProvider.Instance.Get(id);
                if (type != null)
                    PushAudienceTypeToCache(type);
            }
            return type;
        }
        public AudienceType Save(AudienceType audienceType)
        {
            RemoveAudienceTypeListFromCache();

            if (audienceType.Id > 0)
            {
                RemoveAudienceTypeFromCache(audienceType.Id);
                AudienceTypeDataProvider.Instance.Update(audienceType);
            }
            else
            {
                int newId = AudienceTypeDataProvider.Instance.Add(audienceType);
                audienceType.Id = newId;
            }

            // keep the custom db in sync
            using (var db = new OData.OmnicellEntities())
            {
                var dbEntity = db.Omnicell_MediaAudienceType.SingleOrDefault(x => x.Id == audienceType.Id);
                if (dbEntity == null)
                {
                    dbEntity = new OData.Omnicell_MediaAudienceType();
                    db.Omnicell_MediaAudienceType.AddObject(dbEntity);
                }

                dbEntity.Value = audienceType.Value;
                db.SaveChanges();
            }

            return audienceType;
        }
        public IList<AudienceType> List()
        {
            List<AudienceType> list = GetAudienceTypeListFromCache();
            if (list == null)
            {
                list = AudienceTypeDataProvider.Instance.List();
                if (list != null && list.Count > 0)
                    PushAudienceTypeListToCache(list);
            }
            return list;
        }
        public void Delete(int id)
        {
            RemoveAudienceTypeListFromCache();
            RemoveAudienceTypeFromCache(id);
            AudienceTypeDataProvider.Instance.Delete(id);

            // keep the custom db in sync
            using (var db = new OData.OmnicellEntities())
            {
                var dbEntity = db.Omnicell_MediaAudienceType.SingleOrDefault(x => x.Id == id);
                if (dbEntity != null)
                {
                    db.DeleteObject(dbEntity);
                    db.SaveChanges();
                }
            }
        }

        #region Cache

        private string GetAudienceTypeCacheKey(string key)
        {
            return string.Format("PK_AUDIENCE_TYPE::{0}", key);
        }
        private string GetAudienceTypeCacheKey(int id)
        {
            return GetAudienceTypeCacheKey("ID:" + id);
        }

        private void PushAudienceTypeListToCache(IList<AudienceType> list)
        {
            PushAudienceTypeListToCache("ALL-TYPES", list);
        }
        private void PushAudienceTypeListToCache(string key, IList<AudienceType> list)
        {
            _cache.Put(GetAudienceTypeCacheKey(key), list, CacheScope.All);
        }
        private void PushAudienceTypeToCache(AudienceType type)
        {
            _cache.Put(GetAudienceTypeCacheKey(type.Id), type, CacheScope.All);
        }

        private List<AudienceType> GetAudienceTypeListFromCache()
        {
            return GetAudienceTypeListFromCache("ALL-TYPES") as List<AudienceType>;
        }
        private List<AudienceType> GetAudienceTypeListFromCache(string key)
        {
            return _cache.Get(GetAudienceTypeCacheKey(key), CacheScope.All) as List<AudienceType>;
        }
        private AudienceType GetAudienceTypeFromCache(int id)
        {
            return _cache.Get(GetAudienceTypeCacheKey(id), CacheScope.All) as AudienceType;
        }

        private void RemoveAudienceTypeListFromCache()
        {
            RemoveAudienceTypeListFromCache("ALL-TYPES");
        }
        private void RemoveAudienceTypeListFromCache(string key)
        {
            _cache.Remove(GetAudienceTypeCacheKey(key), CacheScope.All);
        }
        private void RemoveAudienceTypeFromCache(int id)
        {
            _cache.Remove(GetAudienceTypeCacheKey(id), CacheScope.All);
        }

        #endregion Cache
    }
}
