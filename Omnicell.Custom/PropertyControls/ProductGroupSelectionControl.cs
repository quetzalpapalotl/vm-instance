﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using Omnicell.Custom.Components;

using Telligent.Common;
using Telligent.DynamicConfiguration.Components;
using TControls = Telligent.Evolution.Controls;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

[assembly: WebResource("Omnicell.Custom.PropertyControls.ProductGroupSelectionControl.js", "text/javascript")]
namespace Omnicell.Custom
{
    public class ProductGroupSelectionControl : Control , IPropertyControl
    {

        #region Register Script

        private const string REGISTER_SCRIPT = "<scr" + "ipt type=\"text/javascript\">\n"
                                             + "$(function() {{\n"
                                             + "\t$.omnicell.propertyControls.productGroupSelection.register( {{\n"
                                             + "\t\tallProductsId: '#{0}',\n"
                                             + "\t\tfilterTypeListId: '#{1}',\n"
                                             + "\t\tproductListId: '#{2}',\n"
                                             + "\t\tproductListCheckBoxes: '#{2} input',\n"
                                             + "\t}});\n"
                                             + "}});\n"
                                             + "</scr" + "ipt>";

        #endregion Register Script

        CheckBoxList _productList = null;
        DropDownList _filterTypeList = null;
        CheckBox _checkAllProds = null;
        PlaceHolder _wrapper = null;

        #region Control Members

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            EnsureChildControls();
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            TControls.CSControlUtility.Instance().RegisterClientScriptResource(this, typeof(ProductGroupSelectionControl), "Omnicell.Custom.PropertyControls.ProductGroupSelectionControl.js");
            TControls.CSControlUtility.Instance().RegisterStartupScript(this, typeof(ProductGroupSelectionControl), this.ClientID + "-initialize",
                string.Format(REGISTER_SCRIPT, _checkAllProds.ClientID, _filterTypeList.ClientID, _productList.ClientID), false);
        }
        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            OmnicellTrainingGalleryPlugin otg = PluginManager.Get<OmnicellTrainingGalleryPlugin>().FirstOrDefault();
            var products = PublicApi.Groups.List(new GroupsListOptions { ParentGroupId = otg.ProductGroupId, PageSize = int.MaxValue });

            _wrapper = new PlaceHolder();
            _filterTypeList = new DropDownList();
            _filterTypeList.Items.Add(new ListItem("Show All Products", "0"));
            _filterTypeList.Items.Add(new ListItem("Show All Products Except Selected", "1"));
            _filterTypeList.Items.Add(new ListItem("Show Only Selected Products", "2"));

            _checkAllProds = new CheckBox();
            _checkAllProds.TextAlign = TextAlign.Left;
            _checkAllProds.Text = "All Products";
            _checkAllProds.Style.Add("float", "right");

            var listContainer = new PlaceHolder();
            _productList = new CheckBoxList();
            _productList.RepeatColumns = 3;
            _productList.Style.Add(HtmlTextWriterStyle.BorderCollapse, "collapse");
            _productList.Style.Add(HtmlTextWriterStyle.Width, "99%");
            _productList.Style.Add(HtmlTextWriterStyle.MarginTop, "15px");

            products.ToList().ForEach(g => _productList.Items.Add(new ListItem(g.Name, g.Id.ToString())));

            listContainer.Controls.Add(_productList);

            _wrapper.Controls.Add(_filterTypeList);
            _wrapper.Controls.Add(_checkAllProds);
            _wrapper.Controls.Add(listContainer);

            this.Controls.Add(_wrapper);
        }

        #endregion Control Members

        #region IPropertyControl

        public ConfigurationDataBase ConfigurationData { get; set; }
        public Property ConfigurationProperty { get; set; }
        public event ConfigurationPropertyChanged ConfigurationValueChanged
        {
            add { throw new NotSupportedException(); }
            remove { }
        }
        public new Control Control { get { return this; } }
        public object GetConfigurationPropertyValue()
        {
            EnsureChildControls();

            int filterType = int.Parse(_filterTypeList.SelectedValue);
            List<int> chosenProducts = _productList.Items.Cast<ListItem>().Where(i => i.Selected).Select(i => int.Parse( i.Value)).ToList();

            return SerializeProducts(filterType, chosenProducts);
        }
        public void SetConfigurationPropertyValue(object value)
        {
            EnsureChildControls();

            var o = DeserializeProducts(value == null ? string.Empty : value.ToString());

            if(o!=null)
            {
                _filterTypeList.SelectedValue = o.FilterType.ToString();
                _productList.Items.Cast<ListItem>().ToList().ForEach(i => i.Selected = (o.SelectedProducts.Contains(int.Parse(i.Value))));
            }   
           
        }

        #endregion IPropertyControl

        public static string SerializeProducts(int filterType, List<int> productIds)
        {
            StringBuilder output = new StringBuilder();
            output.Append(string.Format("FilterType={0}", filterType));
            if (productIds != null && productIds.Count > 0)
            {
                output.Append("&" + string.Join("&", productIds.Select(i => string.Format("Product={0}", i))));
            }
            return output.ToString();
        }
        public static ProductSelectorSettings DeserializeProducts(string serialized)
        {
            ProductSelectorSettings settings = new ProductSelectorSettings();

            if (!string.IsNullOrEmpty(serialized))
            {
                System.Collections.Specialized.NameValueCollection queryString = TControls.CSControlUtility.Instance().ParseQueryString(serialized);

                settings.FilterType = int.Parse(queryString.Get("FilterType"));
                if (queryString.Get("Product") != null)
                    settings.SelectedProducts = queryString.GetValues("Product").Select(i => int.Parse(i)).ToList();
            }

            return settings;
        }

        public class ProductSelectorSettings
        {
            public ProductSelectorSettings()
            {
                SelectedProducts = new List<int>();
            }
            public int FilterType { get; set; }
            public List<int> SelectedProducts { get; set; }
        }

    }
}
