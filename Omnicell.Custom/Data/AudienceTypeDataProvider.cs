﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

using Omnicell.Custom.Components;

namespace Omnicell.Custom.Data
{
    public class AudienceTypeDataProvider : SqlDataProvider<AudienceTypeDataProvider, AudienceType>
    {
        public int Add(AudienceType type)
        {
            string procName = ProcNameWithOwner("omnicell_AudienceType_Add");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Value", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = type.Value}
            };
            return Add(procName, parameters);
        }
        public void Update(AudienceType type)
        {
            string procName = ProcNameWithOwner("omnicell_AudienceType_Update");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = type.Id},
                new SqlParameter{ParameterName = "@Value", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = type.Value}
            };
            ExecuteNonQuery(procName, parameters);
        }
        public AudienceType Get(int id)
        {
            string procName = ProcNameWithOwner("omnicell_AudienceType_Get");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = id}
            };
            return Get(procName, parameters);
        }
        public List<AudienceType> List()
        {
            string procName = ProcNameWithOwner("omnicell_AudienceType_List");
            List<SqlParameter> parameters = new List<SqlParameter>();
            return GetList(procName, parameters);
        }
        public void Delete(int id)
        {
            string procName = ProcNameWithOwner("omnicell_AudienceType_Delete");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = id}
            };
            Delete(procName, parameters);
        }
    }
}
