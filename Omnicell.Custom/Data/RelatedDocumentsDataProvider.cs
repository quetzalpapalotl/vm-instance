﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Omnicell.Custom.Components;

namespace Omnicell.Custom.Data
{
    public class RelatedDocumentsDataProvider : SqlDataProvider<RelatedDocumentsDataProvider, RelatedDocuments>
    {

        public void Save(RelatedDocuments relatedDocuments)
        {
            string procName = ProcNameWithOwner("omnicell_RelatedDocuments_Save");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
	            new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = relatedDocuments.ID},
	            new SqlParameter{ParameterName = "@groupId", SqlDbType = SqlDbType.Int, Value = relatedDocuments.GroupId},
	            new SqlParameter{ParameterName = "@docvalues", SqlDbType = SqlDbType.VarChar, Size = 5000, Value = relatedDocuments.DocumentValues}
            };

            ExecuteNonQuery(procName, parameters);
        }


        public RelatedDocuments Get(int groupId)
        {
            string procName = ProcNameWithOwner("omnicell_RelatedDocuments_Get");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@groupId", SqlDbType = SqlDbType.VarChar, Value = groupId}
            };

            RelatedDocuments rd = Get(procName, parameters);

            return (rd == null)? new RelatedDocuments() : rd;

        }



    }
}
