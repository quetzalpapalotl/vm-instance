﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;

namespace Omnicell.Custom.Data
{
    public class SqlDataProvider<PT, RT>
        where PT : class, new()
        where RT: class, new()
    {
        private static PT _instance = null;
        public static PT Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PT();
                }
                return _instance;
            }
        }

        public string DatabaseOwner = "dbo";
        private string _connectionString = null;

        public string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                    _connectionString = DataProvider.GetConnectionString("SiteSqlServer");
                return _connectionString;
            }
        }
        public string ProcNameWithOwner(string procedureName)
        {
            return string.Format("{0}.{1}", DatabaseOwner, procedureName);
        }
        private SqlConnection GetSqlConnection()
        {

            try
            {
                return new SqlConnection(ConnectionString);
            }
            catch
            {
                throw new CSException(CSExceptionType.DataProvider, "SQL Connection String is invalid.");
            }

        }

        public virtual RT Get(string procedureName, List<SqlParameter> parameters)
        {
            return Get<RT>(procedureName, parameters);
        }
       
        public virtual T Get<T>(string procedureName, List<SqlParameter> parameters) where T : class, new()
        {
            T newObject = null;

            try
            {
                using (SqlConnection conn = GetSqlConnection())
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(procedureName, conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.Clear();
                        command.Parameters.AddRange(parameters.ToArray());

                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            newObject = reader.MapTo<T>();
                        }
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, "COURSES-DATA-ERROR: " + procedureName, ex).Log();
                throw;
            }
            return newObject;
        }
        public virtual List<RT> GetList(string procedureName, List<SqlParameter> parameters)
        {
            return GetList<RT>(procedureName, parameters);
        }
        public virtual List<RT> GetList(string procedureName, List<SqlParameter> parameters, out int totalCount)
        {
            return GetList<RT>(procedureName, parameters, out totalCount);
        }
        public virtual List<T> GetList<T>(string procedureName, List<SqlParameter> parameters) where T : class, new()
        {
            int temp;
            return GetList<T>(procedureName, parameters, out temp);
        }
        public virtual List<T> GetList<T>(string procedureName, List<SqlParameter> parameters, out int totalCount) where T : class, new()
        {

            List<T> list = new List<T>();

            try
            {
                using (SqlConnection conn = GetSqlConnection())
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(procedureName, conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.Clear();
                        command.Parameters.AddRange(parameters.ToArray());

                        SqlParameter returnValue = new SqlParameter();
                        returnValue.Direction = System.Data.ParameterDirection.ReturnValue;
                        command.Parameters.Add(returnValue);

                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                list.Add(reader.MapTo<T>());
                            }
                        }

                        if (returnValue.Value != null)
                            totalCount = (int)returnValue.Value;
                        else
                            totalCount = list.Count;

                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, "COURSES-DATA-ERROR: " + procedureName, ex).Log();
                throw;
            }
            return list;
        }
        public virtual int ExecuteNonQuery(string procedureName, List<SqlParameter> parameters)
        {
            int rowsAffected = 0;

            try
            {
                using (SqlConnection conn = GetSqlConnection())
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(procedureName, conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.Clear();
                        command.Parameters.AddRange(parameters.ToArray());

                        rowsAffected = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, "COURSES-DATA-ERROR: " + procedureName, ex).Log();
                throw;
            }
            return rowsAffected;
        }
        public virtual object ExecuteScalar(string procedureName, List<SqlParameter> parameters)
        {
            object returnValue = null;

            try
            {
                using (SqlConnection conn = GetSqlConnection())
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(procedureName, conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.Clear();
                        command.Parameters.AddRange(parameters.ToArray());

                        returnValue = command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, "COURSES-DATA-ERROR: " + procedureName, ex).Log();
                throw;
            }
            return returnValue;
        }
        public virtual int Add(string procedureName, List<SqlParameter> parameters)
        {
            int newId = -1;
            object returnValue = ExecuteScalar(procedureName, parameters);
            if (returnValue != null)
            {
                if (!int.TryParse(returnValue.ToString(), out newId))
                    newId = -1;
            }
            return newId;
        }
        public virtual int Edit(string procedureName, List<SqlParameter> parameters)
        {
            int itemId = -1;
            object returnValue = ExecuteScalar(procedureName, parameters);
            if (returnValue != null)
            {
                if (!int.TryParse(returnValue.ToString(), out itemId))
                    itemId = -1;
            }
            return itemId;
        }
        public virtual int Delete(string procedureName, List<SqlParameter> parameters)
        {
            return ExecuteNonQuery(procedureName, parameters);
        }
    }
}
