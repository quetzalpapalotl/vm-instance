(function ($) {
    if (typeof $.omnicell === 'undefined') { $.omnicell = {}; }
    if (typeof $.omnicell.widgets === 'undefined') { $.omnicell.widgets = {}; }

	var _omniUploadAttachHandlers = function (context) {
		$(context.fileUploadId).click(function () {
			$.glowModal(context.uploadFileUrl, {
				width : 500,
				height : 300,
				onClose : function (returnValue) {
					_omniUploadModalClosed(context, returnValue);
				}
			});
			return false;
		});

		var selectTagsBox = $(context.selectTagsId).click(function () {
			$(context.tagBoxId).evolutionTagTextBox('openTagSelector');
			return false;
		});
		if (!context.tags || context.tags.length === 0) {
			selectTagsBox.hide();
		}

		$(context.featuredImageUrl).evolutionUserFileTextBox({
			removeText : context.removeResource,
			selectText : context.selectUploadResource,
			noFileText : context.noFileSelectedResource,
			initialPreviewHtml : context.featuredImagePreview
		});

		$(context.featuredPostId).click(function () {
			if (this.checked) {
				$(context.featuredImageId).show();
			} else {
				$(context.featuredImageId).hide();
			}
		});
        
        var saveButton = $(context.saveButton);
        
        if(context.galleryType == 1) {
            $(context.courseTypeId).change(function(e){
                saveButton.evolutionValidation('validate');
            });
    	}

		$.telligent.evolution.navigationConfirmation.enable();
		$.telligent.evolution.navigationConfirmation.register(saveButton);
	},
    _omniUploadAddValidation = function(context) {
        var saveButton = $(context.saveButton);
        
        saveButton.evolutionValidation({
           validateOnLoad: context.mediaId <= 0 ? false : null,
           onValidated: function(isValid, buttonClicked, c) {
               if(isValid) {
                   saveButton.removeClass('disabled');
               } else {
                   saveButton.addClass('disabled');
       			var tabbedPane = $(context.tabId).glowTabbedPanes('getByIndex', 0);
       			if (tabbedPane) {
       				$(context.tabId).glowTabbedPanes('selected', tabbedPane);
       			}
       		}
       	},
       	onSuccessfulClick: function(e) {
               e.preventDefault();
       		saveButton.parent().addClass('processing');
       		saveButton.addClass('disabled');
               _omniUploadSave(context);
       	}
        });
        
    	// File uploaded
        saveButton.evolutionValidation('addCustomValidation', 'mediafileuploaded', function()
        	{
        		return context.fileUploaded;
        	},
        	context.noFileSelectedResource,
        	context.wrapperId + ' .field-item.post-attachment .field-item-validation',
        	null
        );
        
                // Has name
        saveButton.evolutionValidation('addField', context.postNameId,
            {
        		required: true,
        		messages: { required: context.postNameMissing }
        	},
        	context.wrapperId + ' .field-item.post-name .field-item-validation',
        	null
        );

        if( $(context.beginDate).length > 0) {
        saveButton.evolutionValidation( 'addCustomValidation', 'validBeginDate', function() {
                var isValid = true;
                
                // if gallery type is instructor led and course type is 
                var required = (context.galleryType == 1 && !(/onsite/gi).test( $(context.courseTypeId + " option:selected").text() )) || (context.galleryType != 1 && context.requireDates);
                
                
                
                if($.trim($(context.beginDate).val()).length > 0) {
                    isValid = /^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$/.test($(context.beginDate).val());
                } else {
                    isValid = !required;
                }
                return isValid;
            },
            'Please enter a valid begin date.',
            context.wrapperId + ' .field-item.begin-date .field-item-validation', null );
        
        saveButton.evolutionValidation( 'addCustomValidation', 'validEndDate', function() {
                var isValid = true;
                // if gallery type is instructor led and course type is 
                var required = (context.galleryType == 1 && !(/onsite/gi).test( $(context.courseTypeId + " option:selected").text() )) || (context.galleryType != 1 && context.requireDates);
                
                if($.trim($(context.endDate).val()).length > 0) {
                    isValid = /^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$/.test($(context.endDate).val());
                } else {                    
                    // end date is required if begin date is present
                    isValid = !required && ($(context.beginDate).val().length == 0);
                }
                return isValid;
            },
            'Please enter a valid end date.',
            context.wrapperId + ' .field-item.end-date .field-item-validation', null );
        }

    },
    _omniUploadModalClosed = function(context, file) {
    	if(file) {
        	$(context.fileNameId).text(file.fileName);
        	context.fileUploaded = true;
        	context.file = file;
        	$(context.saveButton).evolutionValidation('validateCustom', 'mediafileuploaded');
        }        
    },
    _omniUploadSave = function(context) {
    	var data = _omniUploadCreatePostRequestData(context);
    
    	$.telligent.evolution.post({
    		url: context.saveUrl,
    		data: data,
    		success: function(response)
    		{
    			if (response.redirectUrl) {
    				window.location = response.redirectUrl;
    			}
    		},
    		defaultErrorMessage: context.saveErrorText,
    		error: function(xhr, desc, ex)
    		{
    			$.telligent.evolution.notifications.show(desc,{type:'error'});
    			$(context.saveButton).parent().removeClass('processing');
    			$(context.saveButton).removeClass('disabled');
    		}
    	});
    },
    _omniUploadCreatePostRequestData = function(context) {
        var inTags = $(context.tagBoxId).val().split(/[,;]/g);
        var tags = [];
        for(var i = 0; i < inTags.length; i++)
        {
            var tag = $.trim(inTags[i]);
            if(tag) {
                tags[tags.length] = tag;
            }
        }
        tags = tags.join(',');
        
    	var groups = new Array();
        $('input[name^=' + context.selectedProductGroups + ']').each(function() {
        	if ($(this).attr('checked') == 'checked')
        	{
        		groups[groups.length] = $(this).val();
        	}
        });
        var groupString = groups.join(',');
        
        var data = {
            Title: $(context.postNameId).val(),
            Body: context.getBody(),
            Tags: tags,
            GalleryId: context.galleryId,
            FileChanged: context.file ? '1' : '0',
            SelectedProductGroups: groupString
        };
        
        if($.isFunction(context.getCourseInfo))        
            data.CourseInfo = context.getCourseInfo();
        
        var courseType = $(context.courseTypeId);
        if(courseType.length > 0)
            data.CourseTypeId = courseType.val();
            
        var mediaType = $(context.mediaFormatId);
        if(mediaType.length > 0)
            data.MediaFormatId = mediaType.val();
            
        var beginDate = $(context.beginDate);
        if(beginDate.length > 0 && beginDate.val().length > 0)            
            data.BeginDate = beginDate.val();
        
        var endDate = $(context.endDate);
        if(endDate.length > 0 && endDate.val().length > 0)
            data.EndDate = endDate.val();
        
        var subscribe = $(context.subscribeId);
        data.Subscribe = subscribe.length > 0 ? (subscribe.is(':checked') ? 1 : 0) : -1;
        
        var featured = $(context.featuredPostId);
        if (featured.length > 0)
        {
        	data.Featured = featured.is(':checked') ? 1 : 0;
        	var featuredImage = $(context.featuredImageUrl);
        	if (featuredImage.length > 0)
        		data.FeaturedImageUrl = featuredImage.val();
        }
        else
        	data.Featured = -1;

    	if (context.file)
        {
        	if (context.file.isRemote)
        	{
        		data.FileName = context.file.fileName;
        		data.FileUrl = context.file.url;
        		data.FileIsRemote = '1';
        	}
        	else
        	{
        		data.FileName = context.file.fileName;
        		data.FileContextId = context.file.contextId;
        		data.FileIsRemote = '0';
        	}
        }
        
        if (context.mediaId > 0)
        {
        	data.Id = context.mediaId;
        }
        
        return data;
    },
    _omniUploadPreview = function(context) {
    	var data = _omniUploadCreatePostRequestData(context);
    
    	$(context.previewTabId).html('<div class="message loading loading__message">' + context.previewLoadingText + '</div>');
    
    	$.telligent.evolution.post({
    		url: context.previewUrl,
    		data: data,
    		success: function(response)
    		{
    			$(context.previewTabId).hide().html(response).fadeIn('fast');
    		},
    		defaultErrorMessage: context.previewErrorText,
    		error: function(xhr, desc, ex)
    		{
    			$(context.previewTabId).html('<div class="message error error__message">' + desc + '</div>');
    		}
    	});
    };
	$.omnicell.widgets.uploadEditMediaGalleryPost = {
		register : function (context) {
			$(context.tabId).glowTabbedPanes({
				cssClass : 'tab-pane',
				tabSetCssClass : 'tab-set with-panes',
				tabCssClasses : ['tab'],
				tabSelectedCssClasses : ['tab selected'],
				tabHoverCssClasses : ['tab hover'],
				enableResizing : false,
				tabs :
				[
					[context.composeTabId, context.composeTabText, null],
					[context.previewTabId, context.previewTabText, function () {
							_omniUploadPreview(context);
						}
					]
				]
			});

			$(context.tagBoxId).evolutionTagTextBox({
				allTags : context.tags
			});

			_omniUploadAttachHandlers(context);
            _omniUploadAddValidation(context);
		}
	};

})(jQuery);
