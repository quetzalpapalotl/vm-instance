(function($)
{
    if (typeof $.omnicell === 'undefined') { $.omnicell = {}; }
	if (typeof $.omnicell.training === 'undefined') { $.omnicell.training = {}; }
	if (typeof $.omnicell.training.widgets === 'undefined') { $.omnicell.training.widgets = {}; }
     
    $.omnicell.training.widgets.subscriptionCourseSearch = {
        register: function(context) {
            context.searchButton.click(function()
            {
        	    var searchCriteria = context.searchTextbox.val();
                window.location = '?crs=' + encodeURIComponent(searchCriteria);
            	return false;
            });    
        }
    };
}(jQuery));