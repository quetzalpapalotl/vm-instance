﻿//#define TRACE
#undef TRACE
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.SharePoint.Client;
using Omnicell.Data.Model;
using Telligent.Evolution.Components;
using Telligent.Evolution.Components.Search;
using Telligent.Evolution.Extensibility.Api.Version1;
using SPApi = Telligent.Evolution.Extensions.SharePoint.Client.Api.Version1;
using SPClient = Telligent.Evolution.Extensions.SharePoint.Client;
using SPExtv1 = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TExV1 = Telligent.Evolution.Extensibility.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Omnicell.Services;
using System.Net.Mail;

namespace Omnicell.Custom.Components
{
    public class MoxiPDFExtensions
    {
        private readonly ICacheService _cache = null;
        private readonly OmnicellSharepointPlugin _osp = null;
        private OmnicellSharepointPlugin _plgnSP = null;

        public MoxiPDFExtensions()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
            _osp = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<OmnicellSharepointPlugin>().FirstOrDefault();
            _plgnSP = TExV1.PluginManager.Get<OmnicellSharepointPlugin>().FirstOrDefault();
        }
        public SPExtv1.PagedList<SPApi.Library> GetLibraryList(int groupID)
        {
            SPApi.LibraryListOptions options = new SPApi.LibraryListOptions();
            options.PageIndex = 0;
            options.PageSize = 5;
            SPExtv1.PagedList<SPApi.Library> lstLibraries = SPApi.PublicApi.Libraries.List(groupID, options);

            return lstLibraries;
        }
        public SPExtv1.PagedList<SPApi.Document> GetSharepointLibrary(string guidID)
        {
            // connect to Sharepoint and get a list of the libraries
            SPClient.version2.SharePointLibrary spLib2 = new SPClient.version2.SharePointLibrary();
            SPExtv1.PagedList<SPApi.Document> lstDocs = new SPExtv1.PagedList<SPApi.Document>();

            SPApi.Library splib = new SPApi.Library();
            try
            {
                // convert the library ID to GUID
                Guid LibIDGuid = new Guid(guidID);
                splib = spLib2.Get(LibIDGuid);
                Guid AppGuidID = new Guid(splib.ApplicationId.ToString());
                // this library has a groupID, get all the files associated with the group ID
                int igroupID = splib.GroupId;
                SPApi.DocumentListOptions options = new SPApi.DocumentListOptions();
                options.FolderPath = splib.Root;  // /Repository/Publications /?
                options.PageSize = 5000;
                lstDocs = SPApi.PublicApi.Documents.List(LibIDGuid, options);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstDocs;
        }
        public DateTime GetWebCreatedDate()
        {
            using (ClientContext context = new ClientContext(_osp.SPFullUrl))
            {
                Web web = context.Web;
                context.Load(web, w => w.Created);
                context.ExecuteQuery();
                return web.Created;
            }
        }
        public ZipInputStream GetRemoteZipStream(string remoteUrl, string domain, string userName, string passWord)
        {
            ZipInputStream zistream;
            WebResponse response = null;
            WebRequest request = WebRequest.Create(remoteUrl);
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userName + ":" + passWord));

            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential(userName, passWord, domain);

            if (request != null)
            {
                try
                {
                    response = request.GetResponse();
                    if (response != null)
                    {
                        zistream = new ZipInputStream(response.GetResponseStream());
                        return zistream;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;

        }
        // method to get a stream of the file from sharepoint when a url is provided
        public Stream GetRemoteFileStream(string remoteUrl)
        {
            WebResponse response = null;
            WebRequest request = WebRequest.Create(remoteUrl);
            if (request != null)
            {
                try
                {
                    response = request.GetResponse();
                    return (response != null) ? response.GetResponseStream() : null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }

        // method to get a stream of the file from sharepoint when a url is provided along with credentials.
        public Stream GetRemoteFileStream(string remoteUrl, string domain, string userName, string passWord)
        {
            WebResponse response = null;
            WebRequest request = WebRequest.Create(remoteUrl);
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userName + ":" + passWord));

            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential(userName, passWord, domain);
            if (request != null)
            {
                try
                {
                    response = request.GetResponse();
                    return (response != null) ? response.GetResponseStream() : null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }
        // function to convert utf8 to utf16
        public string ConvertUTF16(string value)
        {

            char[] chars = HttpUtility.HtmlEncode(value).ToCharArray();
            StringBuilder sb = new StringBuilder(value.Length + (int)(value.Length * 0.1));

            foreach (char c in chars)
            {
                int cValue = Convert.ToInt32(c);
                if (cValue > 127)
                {
                    sb.AppendFormat("&#{0};", cValue);
                }
                else
                {
                    sb.Append(c);
                }

            }

            return sb.ToString();
        }

        public SharepointDITA.DitaDocMetaData ExtractMetaDataFromZipStream(ZipInputStream ziStream, string targetFileName)
        {
            SharepointDITA.DitaDocMetaData ddmd = new SharepointDITA.DitaDocMetaData();
            ZipEntry theEntry;
            while ((theEntry = ziStream.GetNextEntry()) != null)
            {
                // if the file has a name, attempt to process it
                if (theEntry.Name.Trim().Length > 1 && theEntry.Name.Trim() == "toc.html")
                {
                    try
                    {
                        // Html Agility Code
                        HtmlDocument doc = new HtmlDocument();
                        doc.Load(ziStream, Encoding.UTF8);

                        IEnumerable<HtmlNode> collection = doc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
                        HtmlNode title = doc.DocumentNode.SelectSingleNode("//title");
                        ddmd.CoreProperties.Title = title.InnerHtml;
                        foreach (var meta in collection)
                        {
                            string nodeName = meta.Attributes["name"].Value;
                            string value = meta.Attributes["content"].Value.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(');

                            switch (nodeName)
                            {

                                case "Id": ddmd.CustomProperties.Id = meta.Attributes["content"].Value; break;
                                case "Product": ddmd.CustomProperties.Product = meta.Attributes["content"].Value; break;
                                case "productversion": ddmd.CustomProperties.ProductVersion = meta.Attributes["content"].Value; break;
                                case "Audience": ddmd.CustomProperties.Audience = value; break;
                                case "Interfaces": ddmd.CustomProperties.Interfaces = value; break;
                                case "OmniFeatures": ddmd.CustomProperties.Features = value; break;
                                case "OmniDocumentType": ddmd.CustomProperties.DocumentType = meta.Attributes["content"].Value; break;
                                case "ShortDescription": ddmd.CoreProperties.Description = meta.Attributes["content"].Value; break;
                                case "targetgroup": ddmd.CustomProperties.TargetGroup = meta.Attributes["content"].Value; break;
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        throw ex;

                    }
                } // if entry match
                // if we have data then break out, otherwise, go to the next entry.
                if (ddmd.CoreProperties.Title != null)
                    break;
            } // ziStream next entry
            return ddmd;
        } // end of method
        //
        // Get list of documents in the SP library
        //
        public SPExtv1.PagedList<SPApi.Document> GetLibraryListing(SPApi.Document objDocument)
        {
            SPExtv1.PagedList<SPApi.Document> lstDocs = new SPExtv1.PagedList<SPApi.Document>();
            // get the applicaiton/library ID for the document that was passed in.
            lstDocs = GetSharepointLibrary(objDocument.Application.ApplicationId.ToString());
            return lstDocs;
        } // end of GetLibraryListing
        //
        // Get Zip where name contains N
        //
        public SPApi.Document GetCorrespondingZip(SPApi.Document objDocument)
        {
            SPApi.Document zipDoc = new SPApi.Document();
            SPExtv1.PagedList<SPApi.Document> lstDocs = new SPExtv1.PagedList<SPApi.Document>();
            lstDocs = GetSharepointLibrary(objDocument.Application.ApplicationId.ToString());
            // find the document with the same name but of type zip in this library
            foreach (SPApi.Document doc in lstDocs)
            {
                if (doc.DisplayName.Contains(objDocument.DisplayName.Replace(".PDF", "").ToString()))
                {
                    zipDoc = doc;
                }
            }
            return zipDoc;
        } // end of get zip where name contains N
        //
        // function to see if this document already exists in a media gallery
        //
        public bool FileAlreadyExists(SPApi.Document objDocument)
        {
            bool FileExists = false;

            SearchResultsListOptions searchOptions = new SearchResultsListOptions();
            //             searchOptions.Tags = options["Tags"].ToString();
            List<String> lstTags = new List<String>();
            lstTags.Add("docid:" + objDocument.Id.ToString());
            searchOptions.Tags = lstTags;
            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);

            if (results.Count > 0)
                FileExists = true;

            return FileExists;
        }

        public string GetFileNameFromUrl(string url)
        {
            try
            {
                string[] fileUrl = url.Split('/');
                return fileUrl[fileUrl.Count() - 1];
            }
            catch (Exception)
            {
            }

            return String.Empty;
        }
        //public void DeleteDocument(Guid contentId)
        //{

        //    // get the document from the sharepoint library
        //    SPApi.Document spPDFdoc = SPApi.PublicApi.Documents.Get(contentId);
        //    // ge the ID of the file from the media gallery and then pass to the function to delete.
        //    int fileID = -1;
        //    int MediaGalleryId = GetMediaGalleryID(_plgnSP.DLMGMaps, spPDFdoc.Library.Id.ToString());
        //    fileID = MediaGalleryDocExists(MediaGalleryId, contentId);
        //    if (fileID > -1)
        //        DeleteMediaGalleryDocument(fileID);
        //}
        
        // retrieves metadata from the database when the pdf is out of sync with the XHTML zip file
        public SharepointDITA.DitaDocMetaData RetrieveMetadata(Guid wikiGuid, string docName, int groupId)
        {
            SharepointDITA.DitaDocMetaData ddmd = new SharepointDITA.DitaDocMetaData();
            SharepointDITA.ParseMetaData dita = new SharepointDITA.ParseMetaData();
            try
            {
                
                var db = new OmnicellEntities();
                var wiki = db.Omnicell_WikiMetaData2.FirstOrDefault(x => x.ContentId == wikiGuid);
                TEntities.Wiki toWiki = TApi.PublicApi.Wikis.Get(new TApi.WikisGetOptions { GroupId = groupId, Key = docName });

                if (toWiki != null)
                {
                    ddmd.CoreProperties.Title = toWiki.Name.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(').Replace("&amp;", "&").Replace("&#39;", @"'"); 
                }

                if (wiki != null)
                {
                    string pageName = "DefaultWikiPage";
                    var wikiPage = db.WikiPageMetaDatas.FirstOrDefault(x => x.PageKey == pageName);
                    

                    if (wikiPage != null)
                        ddmd.CoreProperties.Description = wikiPage.ShortDesc;
                
                    if(!string.IsNullOrEmpty(wiki.Audiences))
                        ddmd.CustomProperties.Audience = dita.TranslateAudienceIds(wiki.Audiences);

                    if(!string.IsNullOrEmpty(wiki.DocumentType))
                        ddmd.CustomProperties.DocumentType = dita.ParseDocumentType((Convert.ToInt32(wiki.DocumentType)));
                    
                    if(!string.IsNullOrEmpty(wiki.Features))
                        ddmd.CustomProperties.Features = wiki.Features;

                    if(!string.IsNullOrEmpty(wiki.Interfaces))
                        ddmd.CustomProperties.Interfaces = wiki.Interfaces;

                    if(!string.IsNullOrEmpty(wiki.PartName))
                        ddmd.CustomProperties.PartName = wiki.PartName;

                    if(!string.IsNullOrEmpty(wiki.Product))
                        ddmd.CustomProperties.Product = dita.TranslateProductIds(wiki.Product);

                    if(!string.IsNullOrEmpty(wiki.ProductVersion))
                        ddmd.CustomProperties.ProductVersion = wiki.ProductVersion;

                    if(!string.IsNullOrEmpty(wiki.RevNum))
                        ddmd.CustomProperties.RevNum = wiki.RevNum;
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
           return ddmd;
        }
        
        // Creates or Updates Document depending of if Document exists
        public void CreateUpdateDocument(SPApi.Document doc, SharepointDITA.DitaDocMetaData ddmd, Guid wikiGuid, string groupName, string docName, int groupId)
        {

                try
                {
                    #if TRACE
                    WriteMessageToFile(string.Format("Beginning of pdf processing: {0}, time: {1}", docName, DateTime.Now.ToString()));
                    #endif

                    string strSharepointRootURL = _plgnSP.SPFullUrl;
                    Telligent.Evolution.Components.User admUser = Telligent.Evolution.Users.GetUser("admin");
                    // if this doc exists, the update, otherwise create
                    int fileID = -1;


                    int MediaGalleryID = GetMediaGalleryID(_plgnSP.MoxiMediaGalleryMap, groupName);

                    #if TRACE
                    WriteMessageToFile(string.Format("Media Gallery Id: {0}, time: {1}", MediaGalleryID, DateTime.Now.ToString()));
                    #endif

                    if (MediaGalleryID > 0)
                    {
                        if (ddmd == null)
                        {

                            ddmd = RetrieveMetadata(wikiGuid, docName, groupId);

                        }

                        fileID = MediaGalleryDocExists(MediaGalleryID, doc.ContentId);

                        #if TRACE
                        WriteMessageToFile(string.Format("File Id: {0} : {1}", fileID, DateTime.Now.ToString()));
                       #endif

                        if (fileID > 0)
                        {
                            TEntities.Media cFile = TApi.PublicApi.Media.Get(fileID);
                            TimeZoneInfo sTimeZone = TimeZoneInfo.FindSystemTimeZoneById(_plgnSP.omTimeZone);
                            DateTime cdate = Convert.ToDateTime(cFile.ExtendedAttributes.FirstOrDefault(x => x.Key == "PublishDate_dt").Value);
                            DateTime pdate = TimeZoneInfo.ConvertTime(cdate, sTimeZone);
                            bool isDayLight = sTimeZone.IsDaylightSavingTime(pdate);
                            int offset = isDayLight == true ? 5 : 6;
                            DateTime date = doc.Modified;
                            var result = DateTime.Compare(date, pdate);

                            #if TRACE
                            WriteMessageToFile(string.Format("SharePoint Date: {0} : Zimbra Date {1} : Result: {2}", date.ToString(), pdate.ToString(), result.ToString()));
                            #endif
                     
                            if (result > 0)
                            {
                                Stream strmfiledata = GetRemoteFileStream(strSharepointRootURL.ToString() + doc.Path, _plgnSP.SPDomain, _plgnSP.SPLogin, _plgnSP.SPPassword);

                                #if TRACE
                                WriteMessageToFile(string.Format("Update Media Method Called: {0} : {1}", cFile.Title, result.ToString()));
                                #endif

                                RunAsUser(() => UpdateMediaGalleryDocument(MediaGalleryID, strmfiledata, ddmd, doc, doc.ContentId, fileID), new Telligent.Evolution.Components.ContextService().GetExecutionContext(), admUser);
                            }

                        }
                        else
                        {
                            Stream strmfiledata = GetRemoteFileStream(strSharepointRootURL.ToString() + doc.Path, _plgnSP.SPDomain, _plgnSP.SPLogin, _plgnSP.SPPassword);

                            #if TRACE
                            WriteMessageToFile(string.Format("Create Media Method Called: {0} : {1}", docName, DateTime.Now.ToString()));
                            #endif

                            RunAsUser(() => CreateMediaGalleryDocument(MediaGalleryID, strmfiledata, ddmd, doc, doc.ContentId, wikiGuid.ToString()), new Telligent.Evolution.Components.ContextService().GetExecutionContext(), admUser); 
                        }
                    }
                }
                catch (Exception ex)
                {
                    new CSException(CSExceptionType.UnknownError, string.Format("Error processing document with name: {0}, and contentId: {1}", doc.DisplayName, doc.ContentId), ex).Log();
                }
        }

        // Create Media Gallery Document
        public bool CreateMediaGalleryDocument(int TargetGalleryId, Stream filedata, SharepointDITA.DitaDocMetaData propsDITA, SPApi.Document objDocument, Guid contentId, string wikiGuid)
        {
            bool actionstatus = false;
            string strFileDisplayName = objDocument.DisplayName.ToString();

            try
            {
                TEntities.Gallery toGallery = TApi.PublicApi.Galleries.Get(new TApi.GalleriesGetOptions { Id = TargetGalleryId });
                if (toGallery != null && toGallery.Id == TargetGalleryId)
                {
                    if (strFileDisplayName.Substring(objDocument.DisplayName.Length - 4).ToLower() != ".pdf")
                        strFileDisplayName += ".pdf";

                    CreateDocument(TargetGalleryId, propsDITA, "2", strFileDisplayName.ToString(), filedata, contentId, wikiGuid, objDocument);

                }
                actionstatus = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actionstatus;
        }

        // this method is for the OmnicellDocument Type both create and update
        private OmnicellMediaOptions updateOmnicellMediaOptions(SharepointDITA.DitaDocMetaData propsDITA, int fileId)
        {
            SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();

            int docType = 0;
            string audienceTypes = string.Empty;
            string productFilters = string.Empty;
            DateTime publishDate = DateTime.Now;
            string productVersion = string.Empty;
            string features = string.Empty;
            string interfaces = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.DocumentType))
                {
                    // attempt to get document type id from the sharepoint metadata fields.
                    docType = pmeta.ParseDocumentId(propsDITA.CustomProperties.DocumentType);
                }
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Audience))
                    audienceTypes = TranslateAudienceTypes(propsDITA.CustomProperties.Audience);
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Product))
                    productFilters = TranslateProductsGroup(propsDITA.CustomProperties.Product);

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.ProductVersion))
                    productVersion = propsDITA.CustomProperties.ProductVersion;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Features))
                    features = propsDITA.CustomProperties.Features.Replace(";", ",");
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Interfaces))
                    interfaces = propsDITA.CustomProperties.Interfaces.Replace(";", ",");

            }
            catch (Exception ex)
            {
                throw ex;
            }

            OmnicellMediaOptions omnicellOptions = new OmnicellMediaOptions
            {
                PostId = fileId,
                DocumentTypeId = docType,
                AudienceTypeIdList = audienceTypes,
                ProductIdList = productFilters,
                PublishDate = publishDate,
                Versions = productVersion,
                FeatureList = features,
                Interfaces = interfaces
            };

            return omnicellOptions;

        }
        private MediaUpdateOptions updateMediaCreateOptions(SharepointDITA.DitaDocMetaData propsDITA, Stream stream, Guid contentId, SPApi.Document objDocument)
        {
            SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();

            MediaUpdateOptions updateOptions = new MediaUpdateOptions
            {
                Description = propsDITA.CoreProperties.Description,
                FileData = stream.ToByteArray(),
                ContentType = "application/pdf",
                FileName = objDocument.DisplayName + ".pdf",
                Name = propsDITA.CoreProperties.Title
            };

            try
            {
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.DocumentType))
                {
                    // attempt to get document type id from the sharepoint metadata fields.
                    int docType = pmeta.ParseDocumentId(propsDITA.CustomProperties.DocumentType);
                    updateOptions.DocumentTypeId(docType.ToString());
                }
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Audience))
                    updateOptions.AudienceTypes(TranslateAudienceTypes(propsDITA.CustomProperties.Audience));
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Product))
                    updateOptions.ProductFilters(TranslateProductsGroup(propsDITA.CustomProperties.Product));

                // use current date/ time
                updateOptions.PublishDate(DateTime.Now);
               // updateOptions.PublishDate(DateTime.UtcNow);

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.ProductVersion))
                    updateOptions.Versions(propsDITA.CustomProperties.ProductVersion);
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Features))
                    updateOptions.FeatureList(propsDITA.CustomProperties.Features.Replace(";", ","));
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Interfaces))
                    updateOptions.Interfaces(propsDITA.CustomProperties.Interfaces.Replace(";", ","));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return updateOptions;
        }

        private MediaCreateOptions createMediaCreateOptions(SharepointDITA.DitaDocMetaData propsDITA, Stream stream, Guid contentId, string wikiGuid, SPApi.Document doc)
        {
            SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();

            MediaCreateOptions createOptions = new MediaCreateOptions
            {
                Description = propsDITA.CoreProperties.Description,
                IsFeatured = false,
                FileData = stream.ToByteArray(),
                Tags = "DocGUID:" + contentId.ToString() + "," + "WikiGUID:" + wikiGuid
            };
            try
            {
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.DocumentType))
                {
                    int docType = pmeta.ParseDocumentId(propsDITA.CustomProperties.DocumentType);

                    createOptions.DocumentTypeId(docType.ToString());
                }

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Audience))
                    createOptions.AudienceTypes(TranslateAudienceTypes(propsDITA.CustomProperties.Audience));

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Product))
                    createOptions.ProductFilters(TranslateProductsGroup(propsDITA.CustomProperties.Product));

                // use current date/ time
               // TimeZone zone = TimeZone.CurrentTimeZone;
              //  TimeSpan addTime = new TimeSpan(6, 0, 0);
              //  DateTime now = zone.ToLocalTime(DateTime.Now);
               // now.Add(addTime);
                createOptions.PublishDate(DateTime.Now);
               // createOptions.PublishDate(DateTime.UtcNow);

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.ProductVersion))
                    createOptions.Versions(propsDITA.CustomProperties.ProductVersion);
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Features))
                    createOptions.FeatureList(propsDITA.CustomProperties.Features.Replace(";", ","));
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Interfaces))
                    createOptions.Interfaces(propsDITA.CustomProperties.Interfaces.Replace(";", ","));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return createOptions;
        }
        private void CreateDocument(int TargetGalleryID, SharepointDITA.DitaDocMetaData propsDITA, string strContentType, string strDisplayName, Stream stream, Guid contentId, string wikiGuid, SPApi.Document doc)
        {
            try
            {
                // testing to see if caching is the issue.
                IDocumentService _svc = Telligent.Common.Services.Get<DocumentService>();
                MediaCreateOptions createOptions = createMediaCreateOptions(propsDITA, stream, contentId, wikiGuid, doc);
                Telligent.Evolution.Components.User admUser = Telligent.Evolution.Users.GetUser("admin");
               
                var media = TApi.PublicApi.Media.Create(TargetGalleryID, propsDITA.CoreProperties.Title, "2", strDisplayName, createOptions);

                #if TRACE
                WriteMessageToFile(string.Format("Create new wiki errors: {0}, time: {1}", media.HasErrors(), DateTime.Now.ToString()));
                #endif

                OmnicellDocumentPost post = new OmnicellDocumentPost();
                OmnicellMediaOptions omnicellOptions = updateOmnicellMediaOptions(propsDITA, media.Id.Value);
                post = _svc.Save(omnicellOptions);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // update media gallery document
        public bool UpdateMediaGalleryDocument(int TargetGalleryId, Stream filedata, SharepointDITA.DitaDocMetaData propsDITA, SPApi.Document objDocument, Guid contentId, int fileID)
        {
            bool actionstatus = false;

            TEntities.Gallery toGallery = TApi.PublicApi.Galleries.Get(new TApi.GalleriesGetOptions { Id = TargetGalleryId });

            if (toGallery != null && toGallery.Id == TargetGalleryId)
            {
                try
                {

                    UpdateDocument(TargetGalleryId, fileID, propsDITA, filedata, contentId, objDocument);
                    actionstatus = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return actionstatus;
        }
        private void UpdateDocument(int TargetGalleryID, int fileID, SharepointDITA.DitaDocMetaData propsDITA, Stream fs, Guid contentId, SPApi.Document objDocument)
        {
            try
            {

                // testing to see if caching is the issue.
                IDocumentService _svc = Telligent.Common.Services.Get<DocumentService>();
                SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();
                OmnicellDocumentPost post = new OmnicellDocumentPost();

                MediaUpdateOptions updateOptions = updateMediaCreateOptions(propsDITA, fs, contentId, objDocument);
                var media = PublicApi.Media.Update(TargetGalleryID, fileID, updateOptions);

                #if TRACE
                WriteMessageToFile(string.Format("Create new wiki errors: {0}, time: {1}", media.HasErrors(), DateTime.Now.ToString()));
                #endif

                OmnicellMediaOptions omnicellOptions = updateOmnicellMediaOptions(propsDITA, media.Id.Value);

                post = _svc.Save(omnicellOptions);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // delete media gallery document
        public bool DeleteMediaGalleryDocument(int fileID)
        {
            bool actionstatus = false;
            // check to make sure the doc exists
            try
            {
                if (fileID > 0)
                {
                    // actually delete the item
                    TApi.PublicApi.Media.Delete(fileID);
                }
                actionstatus = true;
            }
            catch (Exception)
            {

                actionstatus = false;
            }
            return actionstatus;
        }
        private void DeleteDocument(int fileID)
        {
            try
            {
                TEntities.AdditionalInfo additionalInfo = new TEntities.AdditionalInfo();
                additionalInfo = TApi.PublicApi.Media.Delete(fileID);
                // call SOLR? - Tthe PublicApi should handle it for us
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int MediaGalleryDocExists(int mediaGalleryId, Guid contentId)
        {
            int fileID = -1;
            try
            {
                // get a list of docs in the media gallery. and see if this one exists.
                List<FieldFilter> fieldFilters = new List<FieldFilter>
            { 
                new FieldFilter { Id = mediaGalleryId, FieldName = SearchFields.SectionID, FieldValue = mediaGalleryId.ToString() }
            };
                List<String> lstTags = new List<String>();
                lstTags.Add("DocGUID:" + contentId.ToString());

                SearchResultsListOptions searchOptions = new SearchResultsListOptions
                {
                    PageIndex = 0,
                    PageSize = 10000,
                    FieldFilters = fieldFilters,
                    Tags = lstTags
                };
                V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);
                if (results.Count > 0)
                    fileID = Convert.ToInt32(results[0].ContentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fileID;
        }

        private int GetMediaGalleryID(string strLibraryMap, string strSPLibrary)
        {
            int MGId = 0;
            try
            {
                // split the string and find the match.
                var keyValuePairs = strLibraryMap.Split(';')
                    .Select(x => x.Split('='))
                    .ToDictionary(x => x.First(), x => x.Last());


                if (keyValuePairs.ContainsKey(strSPLibrary.ToUpper()))
                {
                    MGId = Convert.ToInt32(keyValuePairs[strSPLibrary.ToUpper()].ToString());
                }

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving media gallery id with target group: {0}", strLibraryMap), ex).Log();
            }
            return MGId;
        }

        private string TranslateAudienceTypes(string strAudiences)
        {
            string[] lstAudience = strAudiences.Split(';');
            string strReturnValue = "";
            foreach (String audience in lstAudience)
            {
                strReturnValue += ConvertAudienceTypes(audience) + ",";
            }
            return strReturnValue.TrimEnd(',');
        }
        private string ConvertAudienceTypes(string strSharePointAudince)
        {
            IAudienceTypeService _svc = Telligent.Common.Services.Get<AudienceTypeService>();

            IList<AudienceType> auType = _svc.List();
            string audience = string.Empty;

            foreach (AudienceType type in auType)
            {
                if (type.Value.Equals(strSharePointAudince))
                {
                    audience = type.Id.ToString();
                }
            }

            return audience;
        }

        private string TranslateProductsGroup(string strProducts)
        {
            string[] lstProducts = strProducts.Split(';');
            string strReturnValue = "";
            foreach (string product in lstProducts)
            {
                strReturnValue += ConvertProductsGroup(product) + ",";
            }
            return strReturnValue.TrimEnd(',');
        }
        private string ConvertProductsGroup(string strProductGroup)
        {
            List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group> apiGroups =
                new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group>();
            int? groupId = 5;
            apiGroups = PublicApi.Groups.List(new GroupsListOptions() { ParentGroupId = groupId, PageSize = 100 }).ToList();

            string productId = string.Empty;

            foreach (var group in apiGroups)
            {
                if (group.Name.Equals(strProductGroup))
                {
                    productId = group.Id.ToString();
                }
            }

            return productId;
        }

        private string[] GetMediaGalleryList(string strMap)
        {
            string[] strReturn;
            var keyValuePairs = strMap.Split(';')
                .Select(x => x.Split('='))
                .ToDictionary(x => x.First(), x => x.Last());

            strReturn = keyValuePairs.Keys.ToArray();

            return strReturn;
        }
        private string[] GetSPLibraryList(string strMap)
        {
            string[] strReturn;
            var keyValuePairs = strMap.Split(';')
                .Select(x => x.Split('='))
                .ToDictionary(x => x.First(), x => x.Last());

            strReturn = keyValuePairs.Values.ToArray();

            return strReturn;
        }

        // function to delete any MG files that do not exist in the corresponding SP Library
        public bool BatchDelete()
        {
            bool boolReturn = true;
            string contentId = String.Empty;
            string title = String.Empty;

            try
            {

                // get the list of libraries and mediagalleries
                string[] arrMGs = GetMediaGalleryList(_plgnSP.DLMGMaps);
                string[] arrLibs = GetSPLibraryList(_plgnSP.MoxiMediaGalleryMap);
                // get the list of documents in the MG and the Lib
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                TEntities.PagedList<TEntities.Media> mgdoclist = new TEntities.PagedList<TEntities.Media>();


                for (int i = 0; i < arrLibs.Length; i++)
                {
                   // spdoclist = GetSharepointLibrary(arrMGs[0].ToString());
                    spdoclist = GetSharepointLibrary(_plgnSP.SrcDocLibId);
                    mgdoclist = TApi.PublicApi.Media.List(new TApi.MediaListOptions { GalleryId = Convert.ToInt32(arrLibs[i]), PageSize = int.MaxValue, PageIndex = 0 });
                    WriteMessageToFile(string.Format("Media Gallery Count: {0}, time: {1}", mgdoclist.Count, DateTime.Now.ToString()));
                    // we have two lists, compare them and call the delete on any items that are not in the MG
                    if (spdoclist.Count > 0)
                    {
                        // original code
                        foreach (TEntities.Media doc in mgdoclist)
                        {
                            string docId = doc.Tags.FirstOrDefault(s => s.StartsWith("DocGUID:"));
                            WriteMessageToFile(string.Format("Document Id: {0}, time: {1}", docId, DateTime.Now.ToString()));

                            if (!string.IsNullOrEmpty(docId))
                            {

                                docId = docId.Replace("DocGUID:", "");
                                docId = "{" + docId + "}";
                                Guid docGuid = new Guid(docId);
                                contentId = docId.ToString();

                                if (spdoclist.Any(a => a.ContentId.CompareTo(docGuid) == 0)) // TODO: change condition to check tags for DocID:<GUID>SPDocID
                                {


                                }
                                else
                                {
                                    DeleteMediaGalleryDocument(doc.Id.GetValueOrDefault());
                                    string message = string.Format("Media Document Deleted: {0} : GUID: {1} : group Id: {2}", doc.Title, docGuid, doc.GroupId);
                                    WriteMessageToFile(string.Format("{0} : Time: {1}", message, DateTime.Now.ToString()));
                                    DeleteLogEmail(message);
                                }
                            }
                        }
                     }
                }
            }
            catch (Exception ex)
            {
                boolReturn = false;
                new CSException(CSExceptionType.UnknownError, string.Format("Error deleting document with title : {0}, and contentId: {1}", title, contentId), ex).Log();
            }
            return boolReturn;
        }

        private void GetTargetGroup(string strGroupMap, string metaData, out int groupId, out string targetGroup)
        {
            groupId = 0;
            targetGroup = String.Empty;
            try
            {
                // split the string and find the match.
                var keyValuePairs = strGroupMap.Split(';')
                    .Select(x => x.Split('='))
                    .ToDictionary(x => x.First(), x => x.Last());


                foreach (KeyValuePair<string, string> entry in keyValuePairs)
                {
                    int n = metaData.ToLower().IndexOf(entry.Key.ToLower());

                    if (n != -1)
                    {
                        groupId = Convert.ToInt32(entry.Value);
                        targetGroup = entry.Key;
                        break;
                    }

                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving media gallery id with target group: {0}", strGroupMap), ex).Log();
            }

        }

        private void WriteMessageToFile(string strMessage)
        {
            EventLogEntry pluginEntry = new EventLogEntry();
            pluginEntry.Category = "Plugins";
            pluginEntry.EventType = EventType.Information;
            pluginEntry.Message = strMessage;
            pluginEntry.EventDate = DateTime.Now;
            pluginEntry.EventID = 700;
            pluginEntry.SettingsID = -1;
            pluginEntry.MachineName = Dns.GetHostName();
            EventLogs.Write(pluginEntry); 
        }

        private void DeleteLogEmail(string message)
        {
            Utilities util = new Utilities();
            string fromEmail = "noreply-myomnicell@omnicell.com";
            string toEmail = "matthew.vinson@defak.to";
            string toName = "Matthew Scott Vinson";
            string fromName = "noreply-myomnicell@omnicell.com";
            string subject = "DITA Wiki Deletion";
            string smtp = "sc9-smtp02.omnicell.com";

            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                mail.Subject = subject;
                mail.Body = message;
                mail.To.Add(new System.Net.Mail.MailAddress(toEmail, toName));
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(smtp);
                client.Credentials = CredentialCache.DefaultNetworkCredentials;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error sending email: {0}", subject), ex).Log();
            }

        }

        private static object _runAsUserLock = new object();
        public void RunAsUser(Action a, Telligent.Evolution.Components.IExecutionContext context, Telligent.Evolution.Components.User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }
    }
}
