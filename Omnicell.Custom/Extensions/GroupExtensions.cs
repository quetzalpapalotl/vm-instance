﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Evolution.Extensibility.Api.Entities.Version1;

namespace Omnicell.Custom
{
    public static class GroupExtensions
    {

        public static void SetLastAttendeeRequestDate(this Group g, DateTime lastDate)
        {
            g.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_LAST_ATTENDEE_REQ_DATE, Value = lastDate.ToUniversalTime().ToString() });
        }
        public static DateTime LastAttendeeRequestDate(this Group g)
        {
            DateTime returnValue = new DateTime(2011, 01, 01).ToUniversalTime();
            ExtendedAttribute ea = g.ExtendedAttributes[Constants.GALLERY_LAST_ATTENDEE_REQ_DATE];
            if (ea != null)
                DateTime.TryParse(ea.Value, out returnValue);

            return returnValue;
        }

    }
}
