﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.MediaGalleries.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Search;
using OData = Omnicell.Data.Model;

namespace Omnicell.Custom.Components
{
    public class OmnicellCoursePost : OmnicellMediaPost
    {
        //readonly ICourseTypeService _ctsvc = Telligent.Common.Services.Get<ICourseTypeService>();
        //readonly IMediaFormatService _mfsvc = Telligent.Common.Services.Get<IMediaFormatService>();

        readonly ICourseTypeService _ctsvc = new CourseTypeService();
        readonly IMediaFormatService _mfsvc = new MediaFormatService();


        public OmnicellCoursePost() { }
        public OmnicellCoursePost(int mediaId) : base(mediaId) { }
        public OmnicellCoursePost(Media media) : base(media) { }

        private string _courseInfo = string.Empty;
        private string _action = string.Empty;
        private string _sku = string.Empty;
        private string _brightcoveid = string.Empty;

        public string CourseInfo
        {
            get { return GetValue<string>(OmnicellSearchFields.CourseInfo, string.Empty); }
            set { _courseInfo = value; }
        }
        
        private int _mediaFormatId = -1;
        public int MediaFormatId
        {
            get { return GetValue<int>(OmnicellSearchFields.MediaFormatId, -1); }
            set { _mediaFormatId = value; }
        }
        public string MediaFormat
        {
            get
            {
                string value = string.Empty;
                if (MediaFormatId > 0)
                {
                    MediaFormat format = _mfsvc.Get(MediaFormatId);
                    if (format != null)
                        value = format.Value;
                }
                return value;
            }
        }
        
        public string CourseType
        {
            get
            {
                string value = string.Empty;
                if (CourseTypeId > 0)
                {
                    CourseType ct = _ctsvc.Get(CourseTypeId);
                    if (ct != null)
                        value = ct.Value;
                }
                return value;
            }
        }
        private int _courseTypeId = -1;
        public int CourseTypeId
        {
            get { return GetValue<int>(OmnicellSearchFields.CourseTypeId, -1); }
            set { _courseTypeId = value; }
        }
        
        public Dictionary<int, string> ProductFilters
        {
            get { return GetValueDictionary(OmnicellSearchFields.ProductIdFilter); }
        }
        /// <summary>
        /// Comma delimited list of product group ids
        /// </summary>
        public string ProductFilterIdList
        {
            get;
            set;
        }

        public string Action
        {
            get { return GetValue<string>(OmnicellSearchFields.Action, String.Empty); }
            set { _action = value; }
        }

        public string Sku
        {
            get { return GetValue<string>(OmnicellSearchFields.Sku, String.Empty); }
            set { _sku = value; }
            
        }

        public string BrightCoveID
        {
            get { return GetValue<string>(OmnicellSearchFields.BrightCoveID, String.Empty); }
            set { _brightcoveid = value; }
        }
    }
}
