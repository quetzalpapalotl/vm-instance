﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Custom.Components
{
    public class ServiceTicket
    {
        public int ServiceTicketId { get; set; }
        public bool IsActive { get; set; }
        public DateTime Update { get; set; }
        public string Number { get; set; }
        public string Abstract { get; set; }
        public string Status { get; set; }
        public string SubStatus { get; set; }
        public string CSN { get; set; }
        public string SymptomCode { get; set; }
        public string ResolutionCode { get; set; }
        public string LastActivityUpdate { get; set; }
        public int StatusId { get; set; }
        public int SubStatusId { get; set; }
        public int SymptomCodeId { get; set; }
        public int SymptomAreaId { get; set; }
        public int ResolutionCodeId { get; set; }
        public int ResolutionAreaId { get; set; }
        public DateTime LastActivityUpdateDate { get; set; }
        public string SerialNumber { get; set; }


    

    }
}
