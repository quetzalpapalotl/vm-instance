﻿using System;
using System.Text;
using System.Linq;
using Omnicell.Services;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Components;
using System.Net;
using System.Collections.Specialized;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using HtmlAgilityPack;

namespace Omnicell.Custom
{
    public class OmnicellWikiCommentPlugin : IConfigurablePlugin, IScriptedContentFragmentExtension
    {
        #region IScriptedContentFragmentExtension members

        private Utilities util = new Utilities();
        IPluginConfiguration Configuration { get; set; }

        public object Extension
        {
            get { return new OmnicellWikiComment(Configuration); }
        }

        public string ExtensionName
        {
            get { return "omnicell_v1_wikicomment"; }
        }

        public string Description
        {
            get { return "A plugin that sends an email when a comment is created on a wiki page."; }
        }

        public void Initialize()
        {
            Utilities util = new Utilities();
            PublicApi.Comments.Events.AfterCreate += new CommentAfterCreateEventHandler(Events_AfterCreate);
            PublicApi.Comments.Events.AfterUpdate += new CommentAfterUpdateEventHandler(Events_AfterUpdate);
        }

        public string Name
        {
            get { return "Omnnicell Wiki Comment Plugin"; }
        }

        #endregion IScriptedContentFragmentExtension members

        #region IConfigurablePlugin members

        public PropertyGroup[] ConfigurationOptions
        {
            get
            {
                PropertyGroup[] groups = new[] { new PropertyGroup("email", "API Email Options", 0) };

                Property commentEmailAddress = new Property("commentEmailAddress", "Wiki Comment Email Address", PropertyType.String, 0, "documentationrequests@omnicell.com");
                commentEmailAddress.DescriptionText = "Enter the email address to receive the notification.";
                groups[0].Properties.Add(commentEmailAddress);

                Property commentSMTPServer = new Property("commentSMTPServer", "Wiki Comment SMTP Server", PropertyType.String, 1, "localhost");
                commentSMTPServer.DescriptionText = "Enter the SMTP server that will process the comment notification.";
                groups[0].Properties.Add(commentSMTPServer);


                return groups;
            }
        }

        public void Update(IPluginConfiguration configuration)
        {
            Configuration = configuration;

            EmailAddress = configuration.GetString("commentEmailAddress");
            SMTPSever = configuration.GetString("commentSMTPServer");
        }

        public string EmailAddress { get; private set; }
        public string SMTPSever { get; private set; }

        #endregion members

        void Events_AfterCreate(CommentAfterCreateEventArgs e)
        {
            try
            {
                var comment = PublicApi.Comments.Get(e.CommentId);
                Guid wikiTypeId = new Guid("393e7426-ce8b-4921-9949-0c0b60cb1f1e");
                Guid mediaTypeId = new Guid("a0753cfb-923b-4975-ad2a-42e5282a6d5d");
               
                string fromName = "myOmnicell.com";
                string fromEmail = "noreply-myomnicell@omnicell.com";
                string toEmail = EmailAddress;
                string toName = "MOXI Administrator";
                var content = new StringBuilder();

                if (comment != null && comment.Content.Application.ApplicationTypeId == wikiTypeId)
                {
                    string wikiPageName = string.Empty;
                    var wikiPage = PublicApi.WikiPages.Get(comment.Content.ContentId);

                   

                    if (wikiPage != null)
                    {
                        wikiPageName = wikiPage.Title.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(').Replace("&amp;", "&").Replace("&#39;", @"'");
                   

                       
                        string subject = string.Format("Comment Left for {0}", wikiPageName);

                        content.Append("<p>");
                        content.Append(comment.User.Username);
                        content.Append(" left the following comment for: ");
                        content.AppendLine(wikiPageName);
                        content.AppendLine("with the following URL: ");
                        content.Append(wikiPage.Url);
                        content.Append("</p>");
                        content.Append("<span><b>Posted Comment:</b></span>");
                        content.Append(comment.Body("raw"));

                        util.SendEmail(fromEmail, fromName, toEmail, toName, subject, content.ToString(), SMTPSever, "", true);
                    }


                }
                else if(comment != null && comment.Content.Application.ApplicationTypeId == mediaTypeId)
                {
                    var media = PublicApi.Media.Get(comment.Content.ContentId);

                    if (media != null)
                    {
                        string mediaName = media.Title.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(').Replace("&amp;", "&").Replace("&#39;", @"'");

                        string subject = string.Format("Comment Left for {0}", mediaName);

                        content.Append("<p>");
                        content.Append(comment.User.Username);
                        content.Append(" left the following comment for: ");
                        content.AppendLine(mediaName);
                        content.AppendLine("with the following URL: ");
                        content.AppendLine(media.Url);
                        content.Append("</p>");
                        content.Append("<span><b>Posted Comment:</b></span>");
                        content.Append(comment.Body("raw"));

                        util.SendEmail(fromEmail, fromName, toEmail, toName, subject, content.ToString(), SMTPSever, "", true);

                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error sending wiki comment email."), ex).Log();
            }
        }

        void Events_AfterUpdate(CommentAfterUpdateEventArgs e)
        {
            try
            {
                var comment = PublicApi.Comments.Get(e.CommentId);
                Guid applicationTypeId = new Guid("393e7426-ce8b-4921-9949-0c0b60cb1f1e");
                Guid mediaTypeId = new Guid("a0753cfb-923b-4975-ad2a-42e5282a6d5d");

                string fromName = "myOmnicell.com";
                string fromEmail = "noreply-myomnicell@omnicell.com";
                string toEmail = EmailAddress;
                string toName = "MOXI Administrator";

                var content = new StringBuilder();

                string wikiPageName = string.Empty;

                if (comment != null && comment.Content.Application.ApplicationTypeId == applicationTypeId)
                {
                    var wikiPage = PublicApi.WikiPages.Get(comment.Content.ContentId);


                    if (wikiPage != null)
                    {
                        wikiPageName = wikiPage.Title.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(').Replace("&amp;", "&").Replace("&#39;", @"'");
                    }

                   
                    string subject = string.Format("Comment Edited for {0}", wikiPageName);

                    content.Append("<p>");
                    content.Append(comment.User.Username);
                    content.Append(" edited the following comment for: ");
                    content.AppendLine(wikiPageName);
                    content.AppendLine("with the following URL: ");
                    content.Append(wikiPage.Url);
                    content.Append("</p>");
                    content.Append("<span><b>Posted Comment:</b></span>");
                    content.Append(comment.Body("raw"));

                    util.SendEmail(fromEmail, fromName, toEmail, toName, subject, content.ToString(), SMTPSever, "", true);
                }
                else if (comment != null && comment.Content.Application.ApplicationTypeId == mediaTypeId)
                {
                    var media = PublicApi.Media.Get(comment.Content.ContentId);

                    if (media != null)
                    {
                        string mediaName = media.Title.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(').Replace("&amp;", "&").Replace("&#39;", @"'");

                        string subject = string.Format("Comment Left for {0}", mediaName);

                        content.Append("<p>");
                        content.Append(comment.User.Username);
                        content.Append(" left the following comment for: ");
                        content.AppendLine(mediaName);
                        content.AppendLine("with the following URL: ");
                        content.Append(media.Url);
                        content.Append("</p>");
                        content.Append("<span><b>Posted Comment:</b></span>");
                        content.Append(comment.Body("raw"));

                        util.SendEmail(fromEmail, fromName, toEmail, toName, subject, content.ToString(), SMTPSever, "", true);

                    }
                }
                
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error sending wiki comment email."), ex).Log();
            }
        }
    }

    public class OmnicellWikiComment
    {
        public OmnicellWikiComment(IPluginConfiguration configuration)
        {
         
        }
    }
}
