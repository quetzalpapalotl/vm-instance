﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom;
using Omnicell.Custom.Components;
using Omnicell.Custom.Data;
using Omnicell.Fetch.Components.Events;
using Omnicell.Fetch.Data;

namespace Omnicell.Fetch.Components
{
    public interface IProjectHealthService
    {
        ProjectHealthReport Get(int id);
        ProjectHealthReport Get(Guid id);
        ProjectHealthReport Create(ProjectHealthReport healthReport);
        ProjectHealthReport Create(int groupId, DateTime reportDate, string reportStatus, string reportStatusCode, string healthReportCompletionStatus, string healthReportCompletionStatusCode, int createdByUserId, int productGroupId);
        ProjectHealthReport CreateInitial(int groupId);
        ProjectHealthReport Update(ProjectHealthReport healthReport);
        ProjectHealthReport Update(int id, int groupId, DateTime reportDate, string reportStatus, string reportStatusCode, string healthReportCompletionStatus, string healthReportCompletionStatusCode, int createdByUserId, int productGroupId);
        AdditionalInfo Delete(int id);
        AdditionalInfo DeleteByGroupId(int groupId);
        void SetIndexStatus(IEnumerable<Guid> healthReportIds, bool isIndexed);
        void SetIndexStatus(int groupId, bool isIndexed);
        PagedList<ProjectHealthReport> ListPollsToReindex(int pageSize, int pageIndex);
        ProjectHealthReportEvents ProjectHealthEvents { get; }
    }

    public class ProjectHealthService : IProjectHealthService
    {
        private readonly ICacheService _cache = null;
        private static readonly ProjectHealthReportEvents _projectHealthEvents = new ProjectHealthReportEvents();

        public ProjectHealthReportEvents ProjectHealthEvents
        {
            get { return _projectHealthEvents; }
        }

        public ProjectHealthService()
        {
            _cache = Services.Get<ICacheService>();
        }

        public ProjectHealthReport Get(int id)
        {
            ProjectHealthReport healthReport = GetProjectHealthReportFromCache(id);
            if (healthReport == null)
            {
                healthReport = ProjectHealthReportDataProvider.Instance.Get(id);
                if (healthReport != null)
                {
                    string name = healthReport.ImplementationRoom.Name;
                    PutProjectHealthReportInCache(id, healthReport);
                }
            }
            return healthReport;
        }

        public ProjectHealthReport Get(Guid id)
        {
            ProjectHealthReport healthReport = GetProjectHealthReportFromCache(id);
            if (healthReport == null)
            {
                healthReport = ProjectHealthReportDataProvider.Instance.Get(id);
                if (healthReport != null)
                {
                    string name = healthReport.ImplementationRoom.Name;
                    PutProjectHealthReportInCache(id, healthReport);
                }
            }
            return healthReport;
        }

        public List<ProjectHealthReport> GetList(int groupId)
        {
            List<ProjectHealthReport> healthReports = ProjectHealthReportDataProvider.Instance.GetList(groupId);

            return healthReports;
        }

        public ProjectHealthReport Create(ProjectHealthReport healthReport)
        {
            int id = ProjectHealthReportDataProvider.Instance.Add(healthReport);
            ProjectHealthReport healthReportNew = ProjectHealthReportDataProvider.Instance.Get(id);
            ProjectHealthEvents.OnAfterCreate(healthReportNew);

            return healthReportNew;
        }

        public ProjectHealthReport CreateInitial(int groupId)
        {
            ProjectHealthReport healthReport = new ProjectHealthReport();
            healthReport.ContentId = Guid.NewGuid();
            healthReport.ImplementationRoomGroupId = groupId;
            healthReport.ProductId = groupId;  //NOTE - No product available yet
            healthReport.HealthReportDate = DateTime.Today;
            healthReport.HealthReportStatus = "Green";
            healthReport.HealthReportStatusCode = "1";
            healthReport.HealthReportCompletionStatus = "0% Implementation Room Created";
            healthReport.HealthReportCompletionStatusCode = "1";
            healthReport.CreatedByUserId = CSContext.Current.User.UserID;
            
            return Create(healthReport);
        }

        public ProjectHealthReport Create(int groupId, DateTime reportDate, string reportStatus, string reportStatusCode, string healthReportCompletionStatus, string healthReportCompletionStatusCode, int createdByUserId, int productGroupId)
        {
            try
            {
                ProjectHealthReport healthReport = new ProjectHealthReport();
                healthReport.ContentId = Guid.NewGuid();
                healthReport.ImplementationRoomGroupId = groupId;
                healthReport.HealthReportDate = reportDate.Add(new TimeSpan(0, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
                healthReport.HealthReportStatus = reportStatus;
                healthReport.HealthReportStatusCode = reportStatusCode;
                healthReport.HealthReportCompletionStatus = healthReportCompletionStatus;
                healthReport.HealthReportCompletionStatusCode = healthReportCompletionStatusCode;
                healthReport.CreatedByUserId = createdByUserId;
                healthReport.ProductId = productGroupId;

                return Create(healthReport);

            }
            catch (Exception ex)
            {
               return new ProjectHealthReport(new AdditionalInfo(new Error(ex.GetType().FullName, ex.Message)));
            }
        }
                                   
        public ProjectHealthReport Update(int id, int groupId, DateTime reportDate, string reportStatus, string reportStatusCode, string healthReportCompletionStatus, string healthReportCompletionStatusCode, int createdByUserId, int productGroupId)
        {
            try
            {
                ProjectHealthReport healthReport = Get(id);
                healthReport.ImplementationRoomGroupId = groupId;
                healthReport.HealthReportDate = reportDate;
                healthReport.HealthReportStatus = reportStatus;
                healthReport.HealthReportStatusCode = reportStatusCode;
                healthReport.HealthReportCompletionStatus = healthReportCompletionStatus;
                healthReport.HealthReportCompletionStatusCode = healthReportCompletionStatusCode;
                healthReport.CreatedByUserId = createdByUserId;
                healthReport.ProductId = productGroupId;

                return Update(healthReport);

            }
            catch (Exception ex)
            {
                return new ProjectHealthReport(new AdditionalInfo(new Error(ex.GetType().FullName, ex.Message)));
            }
        }

        public ProjectHealthReport Update(ProjectHealthReport healthReport)
        {
            try
            {
                int id = ProjectHealthReportDataProvider.Instance.Update(healthReport);
                ProjectHealthReport healthReportNew = ProjectHealthReportDataProvider.Instance.Get(id);
                return healthReportNew;

            }
            catch (Exception ex)
            {
                return new ProjectHealthReport(new AdditionalInfo(new Error(ex.GetType().FullName, ex.Message)));
            }
        }


        public AdditionalInfo DeleteByGroupId(int groupId)
        {
            AdditionalInfo additionalInfo = new AdditionalInfo();
            try
            {
                List<ProjectHealthReport> healthReports = GetList(groupId);
                if (healthReports != null)
                {
                    foreach(ProjectHealthReport healthReport in healthReports)
                    {
                        Delete(healthReport);
                    }
                }
                return additionalInfo;
            }
            catch (Exception ex)
            {
                return new AdditionalInfo(new Error(ex.GetType().FullName, ex.Message));
            }
        }

        public AdditionalInfo Delete(ProjectHealthReport healthReport)
        {
            AdditionalInfo additionalInfo = new AdditionalInfo();
            try
            {
                if (healthReport != null)
                {
                    ProjectHealthReportDataProvider.Instance.Delete(healthReport.ProjectHealthReportId);
                    RemoveProjectHealthReportFromCache(healthReport.ProjectHealthReportId);
                    RemoveProjectHealthReportFromCache(healthReport.ContentId);
                    ProjectHealthEvents.OnAfterDelete(healthReport);
                }
                else
                {
                    additionalInfo.Warnings.Add(new Warning("Invalid Health Report", "Health Report not found"));
                }
                return additionalInfo;
            }
            catch (Exception ex)
            {
                return new AdditionalInfo(new Error(ex.GetType().FullName, ex.Message));
            }
        }

        public AdditionalInfo Delete(int id)
        {
            ProjectHealthReport healthReport = Get(id);
            return Delete(healthReport);
        }

        public void SetIndexStatus(IEnumerable<Guid> healthReportIds, bool isIndexed)
		{
            ProjectHealthReportDataProvider.Instance.SetIndexStatus(healthReportIds, isIndexed);
		}

        public void SetIndexStatus(int groupId, bool isIndexed)
        {
            ProjectHealthReportDataProvider.Instance.SetIndexStatus(groupId, isIndexed);
        }

        public PagedList<ProjectHealthReport> ListPollsToReindex(int pageSize, int pageIndex)
        {
            return ProjectHealthReportDataProvider.Instance.GetListToReIndex(pageSize, pageIndex);
        }


        #region Caching

        public string GetProjectHealthReportsCacheKey(int implementationRoomGroupId)
        {
            return string.Format("FETCH-ROOM-HEALTH-REPORTS:{0}", implementationRoomGroupId);
        }
        public string GetProjectHealthReportCacheKey(int id)
        {
            return string.Format("FETCH-ROOM-HEALTH-REPORT:{0}", id);
        }
        public string GetProjectHealthReportCacheKey(Guid id)
        {
            return string.Format("FETCH-ROOM-HEALTH-REPORT:{0}", id);
        }

        public List<ProjectHealthReport> GetProjectHealthReportsFromCache(int implementationRoomGroupId)
        {
            return _cache.Get(GetProjectHealthReportsCacheKey(implementationRoomGroupId), CacheScope.All) as List<ProjectHealthReport>;
        }
        public ProjectHealthReport GetProjectHealthReportFromCache(int id)
        {
            return _cache.Get(GetProjectHealthReportCacheKey(id), CacheScope.All) as ProjectHealthReport;
        }
        public ProjectHealthReport GetProjectHealthReportFromCache(Guid id)
        {
            return _cache.Get(GetProjectHealthReportCacheKey(id), CacheScope.All) as ProjectHealthReport;
        }

        public void PutProjectHealthReportsInCache(int implementationRoomGroupId, List<ProjectHealthReport> healthReports)
        {
            _cache.Put(GetProjectHealthReportsCacheKey(implementationRoomGroupId), healthReports, CacheScope.All);
        }
        public void PutProjectHealthReportInCache(int id, ProjectHealthReport healthReport)
        {
            _cache.Put(GetProjectHealthReportCacheKey(id), healthReport, CacheScope.All);
        }
        public void PutProjectHealthReportInCache(Guid id, ProjectHealthReport healthReport)
        {
            _cache.Put(GetProjectHealthReportCacheKey(id), healthReport, CacheScope.All);
        }

        public void RemoveProjectHealthReportsFromCache(int implementationRoomGroupId)
        {
            _cache.Remove(GetProjectHealthReportsCacheKey(implementationRoomGroupId), CacheScope.All);
        }
        public void RemoveProjectHealthReportFromCache(int id)
        {
            _cache.Remove(GetProjectHealthReportCacheKey(id), CacheScope.All);
        }
        public void RemoveProjectHealthReportFromCache(Guid id)
        {
            _cache.Remove(GetProjectHealthReportCacheKey(id), CacheScope.All);
        }

        #endregion Caching

    }
}

