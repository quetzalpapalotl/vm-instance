(function ($) {
    if (typeof $.telligent === 'undefined') { $.telligent = {}; }
    if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
    if (typeof $.omnicell === 'undefined') { $.omnicell = {}; }
    if (typeof $.omnicell.widgets === 'undefined') { $.omnicell.widgets = {}; }


    createProjectHealthReport = function(context) {
           
    $.telligent.evolution.post({
        url: context.createUrl,
        data: {
            ReportDate: $(context.projectHealthDate).val(),
            HealthStatus: $(context.healthList).find('option:selected').text(),
            HealthStatusCode: $(context.healthList).find('option:selected').val(),
            HealthReportCompletionStatus: $(context.percentagecompleteList).find('option:selected').text(),
            HealthReportCompletionStatusCode: $(context.percentagecompleteList).find('option:selected').val(),
            ProductGroupId: $(context.productList).val()
        },
    success: function (response) {
        //window.location.reload(true);
        window.location = '?phr=1'
        //alert('success');
    },
    error: function(xhr, desc, ex)
    {
    	$.telligent.evolution.notifications.show(desc,{type:'error'});
        //alert(xhr.status + ': ' +  desc);
        //alert(ex);
    }
});    	
};



 setupCreate = function (context) {  
     
    context.saveButton.click(function()
    {
        //alert('clicked Update');
        //alert(context.productList.val());
    });  
      
    context.saveButton
        .evolutionValidation({
            onValidated: function (isValid, buttonClicked, c) {
                if (isValid) {
                    //alert('isValid');
                }
                else
                {   
                    //alert('ERROR');
                }
        },
        onSuccessfulClick: function (e) {
            e.preventDefault();
            //alert('onSuccessfulClick');
            createProjectHealthReport(context);
            return false;
        }})
          

    context.saveButton.evolutionValidation('addCustomValidation',
        'ProductSelectedValidator',
        function () {
            return context.productList.val() > 0;
        }, 
        'Required', '.field-item-validation.products', null);
        
    context.saveButton.evolutionValidation('addCustomValidation',
        'HealthSelectedValidator',
        function () {
            return context.healthList.val() > 0;
        }, 
        'Required', '.field-item-validation.health', null);
     
    context.saveButton.evolutionValidation('addCustomValidation',
        'PercentCompleteSelectedValidator',
        function () {
            return context.percentagecompleteList.val() > 0;
        }, 
        'Required', '.field-item-validation.percentcomplete', null);
        
};


 setupColumnSort = function (context) {  

    var $currentCol =  $("td.tablecol a[rel='" + context.sortkey + "']") 
    if(context.sortdir == 'desc')
    {
        $currentCol.addClass("sort-button-desc");
    }
    else
    {
        $currentCol.addClass("sort-button-asc");
    }

    $('td.tablecol a').click(function() {
        var $this = $(this).closest('td');
   
       var $sortdir = context.sortdir; 
       
    if ( $(this).is( ".sort-button-asc" ) ) {
       $("td.tablecol a").removeClass("sort-button-asc");
       $("td.tablecol a").removeClass("sort-button-desc");
       $(this).addClass("sort-button-desc");
        $sortdir = "desc";
    }
    else
    {
         if ( $(this).is( ".sort-button-desc" ) ) {
           $("td.tablecol a").removeClass("sort-button-asc");
           $("td.tablecol a").removeClass("sort-button-desc");
           $(this).addClass("sort-button-asc");
           $sortdir = "asc";
        }
        else
        {
            $("td.tablecol a").removeClass("sort-button-asc");
            $("td.tablecol a").removeClass("sort-button-desc");
            $(this).addClass("sort-button-asc");
            $sortdir = "asc";
        }
    }
     var $sortkey = $(this).attr('rel');
        window.location = '?sort=' + encodeURIComponent($sortkey) + '&sdir=' + encodeURIComponent($sortdir) ;
        return false;
    });
};



	$.omnicell.widgets.projecthealthreport = {
		register : function (context) {
            
            context.projectHealthDate.glowDateTimeSelector({
                            pattern:"<01,02,03,04,05,06,07,08,09,10,11,12>/<1-31>/<0001-9999>",
                            yearIndex:2,
                            monthIndex:0,
                            dayIndex:1,
                            hourIndex:-1,
                            minuteIndex:-1,
                            amPmIndex:-1,
                            showPopup:true,
                            allowBlankValue:false
                            });
            
            setTimeout(function(){
                context.projectHealthDate.glowDateTimeSelector('val', new Date());
            }, 10);
            
 
            
            setupCreate(context);
            setupColumnSort(context);
  
		}
	};

})(jQuery);
