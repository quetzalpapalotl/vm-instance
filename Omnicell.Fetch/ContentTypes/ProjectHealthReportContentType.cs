﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Components.Search;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TEApi = Telligent.Evolution.Extensibility.Api.Version1.PublicApi;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Content.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;


using Omnicell.Custom;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;
using Omnicell.Fetch.Components;
using Omnicell.Fetch.Components.Events;


namespace Omnicell.Fetch.Components.ContentTypes
{
    public static partial class Constants
    {
        public static Guid ProjectHealthReportContentTypeId = new Guid("DFCC05668FAF4611866BDF259E39EDB3");
        public static string ProjectHealthReportContentTypeName = "OmnicellProjectHealthReport";
    }

    public class ProjectHealthReportContentType : IContentType, ISearchableContentType
    {
        private IContentStateChanges _contentState;
        private IProjectHealthService _projectHealthService;
        private IFactSheetService _svcFactSheet = null;

        public ProjectHealthReportContentType() 
        {
            _projectHealthService = new ProjectHealthService();
            _svcFactSheet = new FactSheetService();
            //Services.Get<IFactSheetService>();
        }


        #region IContentType Members

        Guid[] IContentType.ApplicationTypes
        {
            get { return new Guid[] { TEApi.Groups.ApplicationTypeId }; }
        }

        void IContentType.AttachChangeEvents(IContentStateChanges stateChanges)
        {
            _contentState = stateChanges;
        }

        Guid IContentType.ContentTypeId
        {
            get { return ContentTypes.Constants.ProjectHealthReportContentTypeId; }
        }

        string IContentType.ContentTypeName
        {
            get { return ContentTypes.Constants.ProjectHealthReportContentTypeName; }
        }

        Telligent.Evolution.Extensibility.Content.Version1.IContent IContentType.Get(Guid contentId)
        {
            return _projectHealthService.Get(contentId);
        }

        string IPlugin.Description
        {
            get { return "Omnicell Project Health Report"; }
        }

        void IPlugin.Initialize()
        {
            TEApi.Groups.Events.AfterDelete += new GroupAfterDeleteEventHandler(Events_AfterDelete);
            _projectHealthService.ProjectHealthEvents.AfterDelete += Events_AfterDelete;
        }

        //TODO: Implement removing content from search
        void Events_AfterDelete(GroupAfterDeleteEventArgs e)
        {
            if (e.Id.HasValue)
            {
                _projectHealthService.DeleteByGroupId(e.Id.Value);
            }
        }

        void Events_AfterDelete(ProjectHealthReportAfterDeleteEventArgs e)
        {
           if(_contentState != null)
            {
            _contentState.Deleted(e.ContentId);
            }
            TEApi.SearchIndexing.Delete(new V1Entities.SearchIndexDeleteOptions(){Id = e.ContentId});

            TEApi.Blogs.Update(1, new BlogsUpdateOptions() {  });
        }

        string IPlugin.Name
        {
            get { return "Omnicell Project Health Report"; }
        }

        #endregion IContentType Members

        #region ISearchableContentType Members

        IList<V1Entities.SearchIndexDocument> ISearchableContentType.GetContentToIndex()
        {
            var projectHealthReportsToIndex =  _projectHealthService.ListPollsToReindex(500, 0);
            List<V1Entities.SearchIndexDocument> searchDocuments = new List<V1Entities.SearchIndexDocument>();

            foreach (var projectHealthReport in projectHealthReportsToIndex)
            {
                var searchDocument = TEApi.SearchIndexing.NewDocument(projectHealthReport.ContentId, ContentTypes.Constants.ProjectHealthReportContentTypeId, ContentTypes.Constants.ProjectHealthReportContentTypeName, projectHealthReport.ImplementationRoomGroup.Url, projectHealthReport.Name, projectHealthReport.Description);
                
                searchDocument.AddField(TEApi.SearchIndexing.Constants.IsContent, true.ToString());
                searchDocument.AddField(TEApi.SearchIndexing.Constants.ContentID, projectHealthReport.ContentId.ToString());
                searchDocument.AddField(TEApi.SearchIndexing.Constants.Date, TEApi.SearchIndexing.FormatDate(projectHealthReport.HealthReportDate));
                searchDocument.AddField(TEApi.SearchIndexing.Constants.UserID, projectHealthReport.CreatedByUserId.ToString());
                searchDocument.AddField(OmnicellSearchFields.ProductId, projectHealthReport.ProductId.ToString());
                if (projectHealthReport.ProductGroup != null)
                    searchDocument.AddField(OmnicellSearchFields.ProductName, projectHealthReport.ProductGroup.Name);
                else
                {
                    searchDocument.AddField(OmnicellSearchFields.ProductName, "");
                }

                searchDocument.AddField(OmnicellSearchFields.HealthReportStatus, projectHealthReport.HealthReportStatus);
                searchDocument.AddField(OmnicellSearchFields.HealthReportStatusCode, projectHealthReport.HealthReportStatusCode);
                searchDocument.AddField(OmnicellSearchFields.HealthReportCompletionStatus, projectHealthReport.HealthReportCompletionStatus);
                searchDocument.AddField(OmnicellSearchFields.HealthReportCompletionStatusCode, projectHealthReport.HealthReportCompletionStatusCode);
                searchDocument.AddField(TEApi.SearchIndexing.Constants.Category, "projecthealthreports");

                var user = TEApi.Users.Get(new UsersGetOptions() { Id = projectHealthReport.CreatedByUserId });
                if (user != null && !user.HasErrors())
                    searchDocument.AddField(TEApi.SearchIndexing.Constants.Username, user.Username);

                var group = TEApi.Groups.Get(new GroupsGetOptions() { Id = projectHealthReport.ImplementationRoomGroupId });
                if (group != null && !group.HasErrors())
                {
                    searchDocument.AddField(TEApi.SearchIndexing.Constants.ApplicationId, group.ApplicationId.ToString());
                    searchDocument.AddField(TEApi.SearchIndexing.Constants.ContainerId, group.ContainerId.ToString());
                    searchDocument.AddField(TEApi.SearchIndexing.Constants.GroupID, group.Id.ToString());
                    searchDocument.AddField(OmnicellSearchFields.IRGroup, group.Name);
                    if (group.ParentGroupId.HasValue)
                        searchDocument.AddField(TEApi.SearchIndexing.Constants.ParentGroupID, group.ParentGroupId.Value.ToString());
                    
                    FactSheet factSheet = _svcFactSheet.GetByGroup((int) group.Id);
                    if(factSheet != null)
                    {
                        if (factSheet.TerritoryOpMgr > 0)
                        {
                            searchDocument.AddField(OmnicellSearchFields.FactSheetTOM, factSheet.TerritoryOpMgr.ToString());
                        }
                        if (factSheet.DivisionalOpDir > 0)
                        {
                            searchDocument.AddField(OmnicellSearchFields.FactSheetDOD, factSheet.DivisionalOpDir.ToString());
                        }
                        if (!string.IsNullOrEmpty(factSheet.FacilityCsn))
                        {
                            searchDocument.AddField(OmnicellSearchFields.FactSheetCSN, factSheet.FacilityCsn);
                        }
                        if (!string.IsNullOrEmpty(factSheet.ContactCity))
                        {
                            searchDocument.AddField(OmnicellSearchFields.FactSheetCity, factSheet.ContactCity);
                        }
                        if (!string.IsNullOrEmpty(factSheet.ContactState))
                        {
                            searchDocument.AddField(OmnicellSearchFields.FactSheetState, factSheet.ContactState);
                        }
                        if (!string.IsNullOrEmpty(factSheet.ContactCountry))
                        {
                            searchDocument.AddField(OmnicellSearchFields.FactSheetCountry, factSheet.ContactCountry);
                        }
                    }
                }

                searchDocuments.Add(searchDocument);
            }

            return searchDocuments;
        }

        string ISearchableContentType.GetViewHtml(Telligent.Evolution.Extensibility.Content.Version1.IContent content, Target target)
        {
            throw new NotImplementedException();
        }

        int[] ISearchableContentType.GetViewSecurityRoles(Guid contentId)
        {
            var projectHealth = _projectHealthService.Get(contentId);
            if (projectHealth != null)
            {
                var group = TEApi.Groups.Get(new GroupsGetOptions() { Id = projectHealth.ImplementationRoomGroupId });
                if (!group.HasErrors())
                {
                    var groupSearchType = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<ISearchableContentType>().FirstOrDefault(x => x.ContentTypeId == TEApi.Groups.ContentTypeId);
                    if (groupSearchType != null)
                        return groupSearchType.GetViewSecurityRoles(group.ContainerId);
                }
            }
            return new int[0];
        }

        bool ISearchableContentType.IsCacheable
        {
            get { return true; }
        }

        void ISearchableContentType.SetIndexStatus(Guid[] contentIds, bool isIndexed)
        {
            if (isIndexed)
            _projectHealthService.SetIndexStatus(contentIds, isIndexed);
        }

        bool ISearchableContentType.VaryCacheByUser
        {
            get { return true; }
        }

        #endregion ISearchableContentType Members
    }
}
