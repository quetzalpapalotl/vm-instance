﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom;
using Omnicell.Fetch;
using Omnicell.Fetch.Components;

namespace Omnicell.Fetch
{
    public class OmnicellFactSheetPlugin : IScriptedContentFragmentExtension
    {

        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Fact Sheet Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Fact Sheet"; }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_factsheet"; }
        }
        public object Extension
        {
            get { return new OmnicellFactSheetWidgetExtension(); }
        }
    }
    public class OmnicellFactSheetWidgetExtension
    {
        private IFactSheetService _svcFactSheet = null;

        public OmnicellFactSheetWidgetExtension()
        {
            try
            {
                _svcFactSheet = new FactSheetService();
            }
            catch (Exception ex)
            {
                // handle the exception here
            }
        }
        public FactSheet Get(int groupId)
        {
            return _svcFactSheet.GetByGroup(groupId);
        }
        public FactSheet Update(int factSheetId,
            [
                Documentation(Name = "GroupId", Type = typeof(int), Description = "The Id of the Implementation Group"),
                Documentation(Name = "FacilityName", Type = typeof(string), Description = "Name of Facility"),
                Documentation(Name = "FacilityCSN", Type = typeof(string), Description = "CSN of Facility"),
                Documentation(Name = "FacilityTimeZone", Type = typeof(string), Description = "Time Zone of Facility"),
                Documentation(Name = "IDNParent", Type = typeof(string), Description = "Name of IDN Parent"),
                Documentation(Name = "IDNCSN", Type = typeof(string), Description = "CSN of IDN Parent"),
                Documentation(Name = "TerritoryOpMgr", Type = typeof(int), Description = "UserId for Territory Operations Manager"),
                Documentation(Name = "DivisionalOpDir", Type = typeof(int), Description = "UserId for Divisional Operations Manager"),
                Documentation(Name = "SystemSalesDir", Type = typeof(int), Description = "UserId for System Sales Director"),
                Documentation(Name = "ProjectDeployMgr", Type = typeof(int), Description = "UserId for Project Deployment Manager"),
                Documentation(Name = "ContactName", Type = typeof(string), Description = "Name of Facility Contact"),
                Documentation(Name = "ContactTitle", Type = typeof(string), Description = "Title of Facility Contact"),
                Documentation(Name = "ContactPhone", Type = typeof(string), Description = "Phone Number of Facility Contact"),
                Documentation(Name = "ContactEmail", Type = typeof(string), Description = "Email Address for Facility Contact"),
                Documentation(Name = "ContactStreet", Type = typeof(string), Description = "Street Address for Factility Contact"),
                Documentation(Name = "ContactCity", Type = typeof(string), Description = "City of Facility Contact"),
                Documentation(Name = "ContactState", Type = typeof(string), Description = "State of Facility Contact"),
                Documentation(Name = "ContactZip", Type = typeof(string), Description = "Zip Code of Facility Contact"),
                Documentation(Name = "ContactCountry", Type = typeof(string), Description = "Country of Facility Contact"),
                Documentation(Name = "Notes", Type = typeof(string), Description = "Notes for fact sheet")
            ] IDictionary options = null)
        {
            FactSheet updatedSheet = null;
            FactSheet sheet = _svcFactSheet.Get(factSheetId);
            if (sheet != null)
            {
                if (options.FieldExists("GroupId"))
                    sheet.GroupId = options.GetValue<int>("GroupId");
                if (options.FieldExists("FacilityName"))
                    sheet.FacilityName = options.GetValue("FacilityName");
                if (options.FieldExists("FacilityCSN"))
                    sheet.FacilityCsn = options.GetValue("FacilityCSN");
                if (options.FieldExists("FacilityTimeZone"))
                    sheet.FacilityTimeZone = options.GetValue("FacilityTimeZone");
                if (options.FieldExists("IDNParent"))
                    sheet.IdnParent = options.GetValue("IDNParent");
                if (options.FieldExists("IDNCSN"))
                    sheet.IdnCsn = options.GetValue("IDNCSN");
                if (options.FieldExists("TerritoryOpMgr"))
                {
                    int? territoryOpMgrId = GetUserId(options.GetValue("TerritoryOpMgr"));
                    if (territoryOpMgrId.HasValue)
                        sheet.TerritoryOpMgr = territoryOpMgrId.Value;
                }
                if (options.FieldExists("DivisionalOpDir"))
                {
                    int? divisionalOpDirId = GetUserId(options.GetValue("DivisionalOpDir"));
                    if (divisionalOpDirId.HasValue)
                        sheet.DivisionalOpDir = divisionalOpDirId.Value;
                }
                if (options.FieldExists("SystemSalesDir"))
                {
                    int? systemSalesDirId = GetUserId(options.GetValue("SystemSalesDir"));
                    if (systemSalesDirId.HasValue)
                        sheet.SystemSalesDir = systemSalesDirId.Value;
                }
                if (options.FieldExists("ProjectDeployMgr"))
                {
                    int? projectDeployMgrId = GetUserId(options.GetValue("ProjectDeployMgr"));
                    if (projectDeployMgrId.HasValue)
                        sheet.ProjectDeployMgr = projectDeployMgrId.Value;
                }
                if (options.FieldExists("ContactName"))
                    sheet.ContactName = options.GetValue("ContactName");
                if (options.FieldExists("ContactTitle"))
                    sheet.ContactTitle = options.GetValue("ContactTitle", sheet.ContactTitle);
                if (options.FieldExists("ContactPhone"))
                    sheet.ContactPhone = options.GetValue("ContactPhone", sheet.ContactPhone);
                if (options.FieldExists("ContactEmail"))
                    sheet.ContactEmail = options.GetValue("ContactEmail", sheet.ContactEmail);
                if (options.FieldExists("ContactStreet"))
                    sheet.ContactStreet = options.GetValue("ContactStreet", sheet.ContactStreet);
                if (options.FieldExists("ContactCity"))
                    sheet.ContactCity = options.GetValue("ContactCity", sheet.ContactCity);
                if (options.FieldExists("ContactState"))
                    sheet.ContactState = options.GetValue("ContactState", sheet.ContactState);
                if (options.FieldExists("ContactZip"))
                    sheet.ContactZip = options.GetValue("ContactZip", sheet.ContactZip);
                if (options.FieldExists("ContactCountry"))
                    sheet.ContactCountry = options.GetValue("ContactCountry", sheet.ContactCountry);
                if (options.FieldExists("Notes"))
                    sheet.Notes = options.GetValue("Notes", sheet.Notes);

                updatedSheet = _svcFactSheet.Save(sheet);
            }
            else
            {
                updatedSheet = new FactSheet();
                updatedSheet.Errors.Add(new V1Entities.Error("Unknown Error", "The requested fact sheet does not exist. Fact sheets can only be created by creating an implementation room."));
            }

            return updatedSheet;
        }

        private int? GetUserId(string value)
        {
            int? userId = null;

            if (!string.IsNullOrEmpty(value))
            {
                string[] segs = value.Split(":".ToCharArray());
                int id = -1;
                if (segs.Length > 1)
                {
                    if (int.TryParse(segs[1], out id))
                        userId = id;
                }
            }

            return userId;
        }
    }
    
}
