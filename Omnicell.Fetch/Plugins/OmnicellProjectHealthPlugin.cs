﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Components.Search;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;


using Omnicell.Custom;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;

using Omnicell.Fetch;
using Omnicell.Fetch.Components;



namespace Omnicell.Fetch
{
    public class OmnicellProjectHealthPlugin : IScriptedContentFragmentExtension
    {
        #region IScriptedContentFragmentExtension Members

        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Project Health Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Project Health."; }
        }
        public object Extension
        {
            get { return new OmnicellProjectHealthWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_projecthealth"; }
        }

        public int ProductGroupId { get; private set; }
        public int FetchGroupId { get; private set; }

        #endregion IScriptedContentFragmentExtension Members
    }

    public class OmnicellProjectHealthWidgetExtension
    {
        private IProjectHealthService _svcProjectHealth = null;

        public OmnicellProjectHealthWidgetExtension()
        {
            _svcProjectHealth = new ProjectHealthService();
        }

        public V1Entities.PagedList<ProjectHealthReport> List(int groupId,
        [
          Documentation(Name = "FetchId", Type = typeof(int), Description = "Specify to search all implementation rooms within the Fetch for Operations group", Default = 0),
          Documentation(Name = "Product", Type = typeof(string), Description = "Specify Product GroupId(s) to be found."),
          Documentation(Name = "StartDate", Type = typeof(DateTime), Description = "Specify Health Report Start date to be found"),
          Documentation(Name = "EndDate", Type = typeof(DateTime), Description = "Specify Health Report End date to be found"),
          Documentation(Name = "AllDates", Type = typeof(bool), Description = "When set to true ignores StartDate and EndDate"),
          Documentation(Name = "HealthReportStatus", Type = typeof(string), Description = "Specify Health Report Status to be found"),
          Documentation(Name = "HealthReportCompletionStatus", Type = typeof(string), Description = "Specify Health Report Completion Status to be found"),
          Documentation(Name = "TerritoryOpMgr", Type = typeof(string), Description = "Specify the TOM to be found."),
          Documentation(Name = "DivisionalOpDir", Type = typeof(string), Description = "Specify the DOD to be found."),
          Documentation(Name = "CSN", Type = typeof(string), Description = "Specify CSN to be found"),
          Documentation(Name = "City", Type = typeof(string), Description = "Specify City to be found"),
          Documentation(Name = "State", Type = typeof(string), Description = "Specify State to be found"),
          Documentation(Name = "Country", Type = typeof(string), Description = "Specify Country to be found"),
          Documentation(Name = "PageIndex", Type = typeof(int), Description = "Specify the page index to return.", Default = 20),
          Documentation(Name = "PageSize", Type = typeof(int), Description = "Specify the number of results to return per page.", Default = 20),
          Documentation(Name = "Query", Type = typeof(string), Description = "Query is not required but you should use either Query or Filters otherwise you'll get all documents in the search index."),
          Documentation(Name = "Sort", Type = typeof(string), Description = "Sort", Default = "date+desc", Options = new string[] { "date", "date+asc", "date+desc", "titlesort", "titlesort+asc", "titlesort+desc", "coursebegindate", "coursebegindate+asc", "coursebegindate+desc" }),
        ]
            IDictionary options)
        {
            SearchResultsListOptions searchOptions = new SearchResultsListOptions();

            int parentId = options.GetValue<int>("FetchId", 0);
            string productId = options.GetValue("Product");
            DateTime? startDate = options.GetValue<DateTime>("StartDate");
            DateTime? endDate = options.GetValue<DateTime>("EndDate");
            string healthReportStatus = options.GetValue("HealthReportStatus");
            string healthReportCompletionStatus = options.GetValue("HealthReportCompletionStatus");
            bool allDates = options.GetValue<bool>("AllDates", false);
            string territoryOpMgr = options.GetValue<string>("TerritoryOpMgr");
            string divisionalOpDir = options.GetValue<string>("DivisionalOpDir");
            string csn = options.GetValue("CSN");
            string city = options.GetValue("City");
            string state = options.GetValue("State");
            string country = options.GetValue("Country");

            int pageIndex = options.GetValue<int>("PageIndex", 0);
            int pageSize = options.GetValue<int>("PageSize", 20);
            string query = options.GetValue("Query");
            string sort = options.GetValue("Sort", "").Replace(":", " ");

            int currId = 0;
            List<FieldFilter> fieldFilters = new List<FieldFilter>();

            fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = "type", FieldValue = Omnicell.Fetch.Components.ContentTypes.Constants.ProjectHealthReportContentTypeName });

            if (parentId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = SearchFields.ParentGroupID, FieldValue = parentId.ToString() });
            else
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = SearchFields.GroupID, FieldValue = groupId.ToString() });
            

            if (!string.IsNullOrEmpty(query))
                searchOptions.Query = query;

            if (!string.IsNullOrEmpty(productId))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.ProductIdFilter, FieldValue = productId });

            if (!string.IsNullOrEmpty(healthReportStatus) && healthReportStatus != "0")
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.HealthReportStatusCode, FieldValue = healthReportStatus });

            if (!string.IsNullOrEmpty(healthReportCompletionStatus) && healthReportCompletionStatus != "0")
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.HealthReportCompletionStatusCode, FieldValue = healthReportCompletionStatus });

            if (!allDates)
            {
                if (startDate == DateTime.MinValue)
                {
                    startDate = DateTime.Today.AddDays(-7);
                }
                if (endDate == DateTime.MinValue)
                {
                    endDate = DateTime.Today;
                }

                if (endDate < startDate)
                {
                    endDate = startDate;
                }
           

                if (startDate > DateTime.MinValue && endDate > DateTime.MinValue)
                {
                    startDate = ((DateTime)startDate).AddDays(-1).AddTicks(-1);
                    endDate = ((DateTime)endDate).AddTicks(1);

                    searchOptions.DateRangeFilters.Add(new DateRangeFilter { FieldName = SearchFields.Date, Start = startDate, End = endDate, Inclusive = false });
                }
            }

            if (!string.IsNullOrEmpty(territoryOpMgr))
            {
                string[] territoryOpMgrToken = territoryOpMgr.Split(new char[] { ':' });
                if (territoryOpMgrToken.Length == 2)
                {
                    fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.FactSheetTOM, FieldValue = territoryOpMgrToken[1] });
                }
            }

            if (!string.IsNullOrEmpty(divisionalOpDir))
            {
                string[] divisionalOpDirToken = divisionalOpDir.Split(new char[] { ':' });
                if (divisionalOpDirToken.Length == 2)
                {
                    fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.FactSheetDOD, FieldValue = divisionalOpDirToken[1] });
                }
            }


            if (!string.IsNullOrEmpty(csn))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.FactSheetCSN, FieldValue = csn });

            if (!string.IsNullOrEmpty(city))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.FactSheetCity, FieldValue = city });

            if (!string.IsNullOrEmpty(state) && state != "0")
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.FactSheetState, FieldValue = state });

            if (!string.IsNullOrEmpty(country) && country != "0")
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.FactSheetCountry, FieldValue = country });

   
            //default sort is "date desc", all other sort items will have secondary sort of "date desc"
            if (string.IsNullOrEmpty(sort.Trim()))
            {
                sort = "date desc";
                //sort = "date+desc";
            }
            else
            {
                if (sort != "date desc")
                {
                     sort += ",date desc";
                   // sort = "date+desc";
                }
            }

            searchOptions.PageIndex = pageIndex;
            searchOptions.PageSize = pageSize < 100 ? pageSize : 100;
            searchOptions.FieldFilters = fieldFilters;
            searchOptions.Category = "projecthealthreports";
            searchOptions.Sort = sort;
            searchOptions.FieldFilters = fieldFilters;


            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);

            List<ProjectHealthReport> healthReports = new List<ProjectHealthReport>();
            ProjectHealthService projectHealthService = new ProjectHealthService();
            results.ToList().ForEach(r => healthReports.Add(projectHealthService.Get((Guid) r.GuidId)));

            return new V1Entities.PagedList<ProjectHealthReport>(healthReports, pageSize, pageIndex, results.TotalCount);
        }

        public void Create(int groupId, DateTime reportDate, string reportStatus, string reportStatusCode, string healthReportCompletionStatus, string healthReportCompletionStatusCode, int createdByUserId, int productGroupId)
        {
            _svcProjectHealth.Create(groupId, reportDate, reportStatus, reportStatusCode, healthReportCompletionStatus, healthReportCompletionStatusCode, createdByUserId, productGroupId);
        }

        public void Delete(int id)
        {
            _svcProjectHealth.Delete(id);
        }

        public void DeleteByGroupId(int groupId)
        {
            _svcProjectHealth.DeleteByGroupId(groupId);
        }
    }
}
