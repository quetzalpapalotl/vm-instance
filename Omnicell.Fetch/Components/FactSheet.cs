﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Search;

namespace Omnicell.Fetch.Components
{
    public class FactSheet : TEntities.ApiEntity
    {
        public int FactSheetId {get;set;}
        public int GroupId {get; set;}
        public string FacilityName {get; set;}
        public string FacilityCsn {get; set;}
        public string FacilityTimeZone {get; set;}
        public string IdnParent {get; set;}
        public string IdnCsn {get; set;}
        public int TerritoryOpMgr {get; set;}
        public int DivisionalOpDir {get; set;}
        public int SystemSalesDir {get; set;}
        public int? ProjectDeployMgr {get; set;}
        public string ContactName {get; set;}
        public string ContactTitle {get; set;}
        public string ContactPhone {get; set;}
        public string ContactEmail {get; set;}
        public string ContactStreet {get; set;}
        public string ContactCity {get; set;}
        public string ContactState {get; set;}
        public string ContactZip {get; set;}
        public string ContactCountry {get; set;}
        public string Notes {get; set;}

        public TEntities.Group _group = null;
        public TEntities.Group Group
        {
            get
            {
                if (_group == null && GroupId > 0)
                {
                    _group = TApi.PublicApi.Groups.Get(new TApi.GroupsGetOptions { Id = GroupId });
                }
                return _group;
            }
        }

        TEntities.User _territoryOpMgrUser = null;
        public TEntities.User TerritoryOpMgrUser
        {
            get
            {
                if (_territoryOpMgrUser == null && TerritoryOpMgr > 0)
                {
                    _territoryOpMgrUser = TApi.PublicApi.Users.Get(new TApi.UsersGetOptions { Id = TerritoryOpMgr });
                }
                return _territoryOpMgrUser;
            }
        }

        TEntities.User _divisionalOpDirUser = null;
        public TEntities.User DivisionalOpDirUser
        {
            get
            {
                if (_divisionalOpDirUser == null && DivisionalOpDir > 0)
                {
                    _divisionalOpDirUser = TApi.PublicApi.Users.Get(new TApi.UsersGetOptions { Id = DivisionalOpDir });
                }
                return _divisionalOpDirUser;
            }
        }

        TEntities.User _systemSalesDirUser = null;
        public TEntities.User SystemSalesDirUser
        {
            get
            {
                if (_systemSalesDirUser == null && SystemSalesDir > 0)
                {
                    _systemSalesDirUser = TApi.PublicApi.Users.Get(new TApi.UsersGetOptions { Id = SystemSalesDir });
                }
                return _systemSalesDirUser;
            }
        }

        TEntities.User _projectDeployMgrUser = null;
        public TEntities.User ProjectDeployMgrUser
        {
            get
            {
                if (_projectDeployMgrUser == null && ProjectDeployMgr.HasValue && ProjectDeployMgr > 0)
                {
                    _projectDeployMgrUser = TApi.PublicApi.Users.Get(new TApi.UsersGetOptions { Id = ProjectDeployMgr.Value });
                }
                return _projectDeployMgrUser;
            }
        }

    }
}
