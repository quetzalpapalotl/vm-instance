﻿using System;
namespace Omnicell.Data.Model
{
    interface ITicket
    {
        string Abstract { get; set; }
        string CSN { get; set; }
        int Id { get; set; }
        bool IsActive { get; set; }
        string LastActivityUpdate { get; set; }
        string Number { get; set; }
        string ResolutionCode { get; set; }
        string Status { get; set; }
        string SubStatus { get; set; }
        string SymptomCode { get; set; }
        System.Data.Objects.DataClasses.EntityCollection<Omnicell_TicketSubscription> TicketSubscriptions { get; set; }
        DateTime? Updated { get; set; }
        string SerialNumber { get; set; }
    }
}
