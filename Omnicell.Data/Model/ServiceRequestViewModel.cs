﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class ServiceRequestViewModel
    {
        public List<OmnicellTicket> Tickets { get; set; }
        public int? UserId { get; set; }
        public string CSNSearchText { get; set; }
        public int? IsImmediateUpdate { get; set; }
        public string SendToEmail { get; set; }
        public bool? HasAllUpdates { get; set; }
    }
}
