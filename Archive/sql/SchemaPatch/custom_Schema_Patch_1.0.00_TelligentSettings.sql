IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='telligent_Settings')  
BEGIN 
	CREATE TABLE [dbo].[telligent_Settings](
		[SettingsType] [nvarchar](256) NOT NULL,
		[SettingsXML] [ntext] NULL,
	 CONSTRAINT [PK_telligent_Settings] PRIMARY KEY CLUSTERED 
	(
		[SettingsType] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	CREATE TABLE [dbo].[telligent_SchemaVersion]
	(
	[Major] [int] NOT NULL,
	[Minor] [int] NOT NULL,
	[Patch] [int] NOT NULL,
	[InstallDate] [datetime] NOT NULL,
	[ComponentId] [uniqueidentifier] NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000'
	)
	INSERT [telligent_SchemaVersion] ([Major],[Minor],[Patch],[InstallDate]) VALUES (1,0,0, getdate())
END