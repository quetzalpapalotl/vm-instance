﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.BasePageViewModel>" %>
<div class="title-container">
    <div class="title-panel">
        
        <% if (!String.IsNullOrEmpty(Model.BackButtonText) && Model.BackButtonRouteValue != null){%>
            <a href="<%= Url.RouteUrl(Model.BackButtonRouteValue)%>" class="internal-link view-previous"><span class="first"></span><span class="second"><%= Model.BackButtonText%></span></a>
        <%} %>
        <div class="title-text">
            <%= !String.IsNullOrEmpty(Model.PageTitle) ? Html.Encode(Model.PageTitle) : "&nbsp;"%>
        </div>
        <% if (Model.ShowCreateMessageButton) { %>
            <% if (Model.CreateMessageButtonInfo != null) {%>
                <%= Html.ScriptLinkButton(Model.CreateMessageButtonInfo.Text, Model.CreateMessageButtonInfo.Action , Model.CreateMessageButtonInfo.HtmlAttributes, "internal-link create-status")%>
            <% } else { %>
                <%= Html.CreateStatusMessageButton(Html.LocalizedString("Core_Master_CreateStatus"), "internal-link create-status", null, null)%>
            <% } %>
        <% } %>
    </div>
    <div class="search-form">
        <a href="#" class="internal-link cancel-search" onclick="hideSearchForm(); return false;"><span class="first"></span><span class="second"><%=Html.LocalizedString("Core_Master_CancelSearch") %></span></a>
        <% using (Html.BeginForm("SearchResults", "Search", new { area = "Search" })) %>
        <% { %>
            <script type="text/javascript">
		    // <![CDATA[
                function showSearchForm(value, withoutEffect) {
                    if (typeof (value) != undefined)
                        $('#searchInput').val(value);

                    showClearSearchButton();
                
                    $('.title-text').hide();
                    $('.search-form').fadeIn(withoutEffect ? 'fast' : 'slow', function () { $('#searchInput').focus(); });
                    $('#showSearchButton').hide();
                }

                function hideSearchForm() {
                    $('.search-form').hide();
                    $('.title-text').show();
                    $('#showSearchButton').show();
                }

                function redirectToSearch() {
                    var searchEndpoint = '<%= Url.Content(Url.Action("Index", "Search", new { area = "", queryterm = "", filtertype = Model.SearchFilterType, filtervalue = Model.SearchFilterValue })) %>';
                    var queryTerm = $('#searchInput').val();
                    if (searchEndpoint.search(/\?/) > -1)
                        searchEndpoint += "&";
                    else
                        searchEndpoint += "?";
                    window.location = searchEndpoint + "queryterm=" + encodeURIComponent(queryTerm);
                }

                function clearSearchInput(event) {
                    $('#searchInput').val('');
                    $('.clear-search').hide();
                    $('#searchInput').focus();
                    if (event && event.preventDefault) event.preventDefault();
                }

                function showClearSearchButton() {
                    if ($('#searchInput').val().length > 0)
                        $('.clear-search:hidden').show();
                    else
                        $('.clear-search:visible').hide();
                }

                function submitSearchFormOnEnter(event) {
                    if (event.keyCode == 13 || event.keyCode == 10) {
                        redirectToSearch();
                        event.cancel = true;
                        return false;
                    }
                    else
                        return true;
                }
		    // ]]>
            </script>
            <input type="text" id="searchInput" name="SearchInput" onkeyup="showClearSearchButton()" onkeydown="return submitSearchFormOnEnter(event)" />
            <%= Html.ScriptLinkButton("", "clearSearchInput(event)", "internal-link clear-search")%>
            <%= Html.ScriptLinkButton(Html.LocalizedString("Core_Master_Search"), "redirectToSearch()", "internal-link view-search")%>
        <% } %>
    </div>
</div>
