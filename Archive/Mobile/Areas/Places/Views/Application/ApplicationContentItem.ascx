<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.ApplicationContent>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>
<li class="list-item">
    <div class="item-author">
		<span class="avatar"><%= Html.UserProfileLink(Model.Author, true)%></span>
	</div>
    <div class="item-header">          
        <div class="item-title">
            <a href='<%= Model.Url %>' class="internal-link view-application-content"><%: Model.Title %></a>
        </div>
    </div>
    <div class="item-date"><%= Html.FormatAgoDate(Model.PublishedDate)%></div>
    <% if (Model.ApplicationType == TelligentEvolution.Mobile.Web.Data.ApplicationTypes.Forum) { %>
        <div class="reply-count">
            <div class="view-message">
                <%= Model.ReplyCount %>
            </div>
        </div>
    <% } %>
    <% if (Model.ThreadType == ThreadTypes.QuestionAndAnswer && !String.IsNullOrEmpty(Model.ThreadStatus) && Model.ThreadStatus != ThreadStatuses.NotSet) {%>
		<span class="item-status <%= (Model.ThreadStatus == ThreadStatuses.Answered) ? "answered" : (Model.ThreadStatus == ThreadStatuses.NotAnswered) ? "not-answered" : "suggested" %>"></span>
	<% } %>
</li>