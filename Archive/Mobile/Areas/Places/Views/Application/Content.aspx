﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.ApplicationContentViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Models" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CreateMessage" runat="server">
    <% if (Model.ShowCreateMessageButton){ 
           if (Model.UserCanPostMessage) {%>
                <% Html.RenderPartial(Model.CreatePostViewName, Model.CreatePostModel); %>
            <% } else { %>
                <% Html.RenderPartial("CreateMessage", new CreateMessageViewModel() { ShowForm = Model.ShowCreateMessageButton }); %>
            <% } %>
     <% } %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="MainContent" runat="server">
   
    <%  var threadInfo = new 
        { 
            AnsweredCss = "answered",
            NotAnsweredCss = "not-answered",
            SuggestedCss = "suggested",
            
            AnsweredText = Html.LocalizedString("ApplicationContent_Thread_Answered"),
            NotAnsweredText = Html.LocalizedString("ApplicationContent_Thread_NotAnswered"),
            SuggestedText = Html.LocalizedString("ApplicationContent_Thread_AnsweredNotVerified")            
        };

        bool IsQuestionAndAnswerThread = Model.ApplicationContent.ThreadType == ThreadTypes.QuestionAndAnswer && Model.ApplicationContent.ApplicationType == ApplicationTypes.Forum;
        %>

	<script type="text/javascript">
	// <![CDATA[
        
        var replyCount = <%= Model.ApplicationContent.ReplyCount %>;
        $(function ()
        {
		    $('#createCommentContainer').bind('telligent_mobile_createmessage_show', function () { $('.add-reply-area').hide(); })
                                        .bind('telligent_mobile_createmessage_close', function () { $('.add-reply-area').show(); })
                                        .bind('telligent_mobile_createmessage_success', commentCreated );
            
            <% if (IsQuestionAndAnswerThread) {%>

                var url = '<%=Url.Action("MarkReplyAs", "Application", new { area = "Places", forumID = Model.ApplicationContent.ApplicationId, threadId = Model.ApplicationContent.Id })%>';
                $('#applicationContent').thread(url, '<%=threadInfo.AnsweredCss%>', '<%=threadInfo.NotAnsweredCss%>', '<%=threadInfo.SuggestedCss%>',
                                                     '<%=threadInfo.AnsweredText%>', '<%=threadInfo.NotAnsweredText%>', '<%=threadInfo.SuggestedText%>');
            <%} %>
        });

		function commentCreated(event, comment) 
        {            
            replyCount++;
			var link = $('.reply-count .add-reply');
            link.hide().unbind('click').attr('onclick', '').html('<span></span>' + replyCount).show();

			<% if (Model.ApplicationContent.ApplicationType == ApplicationTypes.Forum || Model.ApplicationContent.ApplicationType == ApplicationTypes.Weblog) { %>

                $('.empty-list').removeClass('empty-list');
                $('.list-view').trigger("telligent_mobile_list_reload", []);
                link.click(scrollToComments);
                <% if (IsQuestionAndAnswerThread) { %>
                    if (comment.CustomParams != null && !$.stringIsNullOrEmpty(comment.CustomParams.ThreadStatus))
                        $('#applicationContent').trigger("telligent_mobile_thread_update_status", [comment.CustomParams.ThreadStatus]);
                <% } %>

			<% } else { %>

                var url = '<%= Url.Action("Comments", new { area = "Places", controller = "Application", contentId = Model.ApplicationContent.Id, applicationId = Model.ApplicationContent.ApplicationId, applicationType = Model.ApplicationContent.ApplicationType }) %>';
                link.unbind('click').attr('onclick', '').attr('href', url);

			<% } %>
		}

        function scrollToComments()
        {
            var list = $('#CommentList .list-view-container .list-item');
            if (list.length > 0)
            {
                list[0].scrollIntoView(false);
            }
        }

	// ]]>
    </script>

    <div class="application-content" id="applicationContent">
        <div class="content-header">
            <span class="content-title"><%: Model.ApplicationContent.Title %></span>
            <span class="avatar"><img src='<%= Model.ApplicationContent.Author.AvatarUrl %>' border="0" alt="<%= Model.ApplicationContent.Author.DisplayName %>" /></span>
            <span class="user-name"><%= Html.LocalizedString("ApplicationContent_Index_CreatedBy") %> <%= Html.UserProfileLink(Model.ApplicationContent.Author, false)%></span>
            <span class="content-date"><%= Html.FormatAgoDate(Model.ApplicationContent.PublishedDate) %></span>
            <span class="reply-count">
			    <%= Html.ReplyCommentCountButton(Model.ApplicationContent, Model.UserCanReply) %>
			</span>
            <% if (IsQuestionAndAnswerThread && !String.IsNullOrEmpty(Model.ApplicationContent.ThreadStatus)) {
                   string threadStatus = Model.ApplicationContent.ThreadStatus;
                   string statusCss = threadStatus == ThreadStatuses.Answered ? threadInfo.AnsweredCss : (threadStatus == ThreadStatuses.NotAnswered) ? threadInfo.NotAnsweredCss : threadInfo.SuggestedCss;%>
		        <span style="<%=Model.ApplicationContent.ThreadStatus == ThreadStatuses.NotSet ? "display:none" : ""%>" class="item-status <%= statusCss %>"><%= threadStatus == ThreadStatuses.Answered ? threadInfo.AnsweredText :  threadStatus == ThreadStatuses.NotAnswered ? threadInfo.NotAnsweredText : threadInfo.SuggestedText%></span>
	        <% } %>
        </div>
		<% if (Model.UserCanReply && Model.ApplicationContent.ReplyCount == 0 && 
                Model.ApplicationContent.ApplicationType != ApplicationTypes.Forum && Model.ApplicationContent.ApplicationType != ApplicationTypes.Weblog) { %>
		    <% Html.RenderPartial("CreateComment", new CreateCommentViewModel() { ThreadType = Model.ApplicationContent.ThreadType }); %>
		<% } %>
        <div class="content-container">
            <% if (!String.IsNullOrEmpty(Model.ApplicationContent.AttachmentUrl) || Model.ApplicationContent.ApplicationType == ApplicationTypes.MediaGallery) {
                string url = Model.ApplicationContent.AttachmentUrl ?? Model.ApplicationContent.FullSiteUrl; %>
                <div class="content-attachment">
                    <div class="content-attachment-header"><span></span></div>
                    <div class="content-attachment-name">
                        <a href='<%= url %>' class="internal-link download-attachment"><span></span><%= string.Format(Html.LocalizedString("ApplicationContent_Index_Download"), Model.ApplicationContent.AttachmentName)%></a>
                    </div>
                    <div class="content-attachment-footer"><span></span></div>
                </div>
            <% } %>
            <div class="content-body"><%= Model.ApplicationContent.Body %></div>
        </div>
    </div>
	<% if (Model.ApplicationContent.ApplicationType == ApplicationTypes.Forum || Model.ApplicationContent.ApplicationType == ApplicationTypes.Weblog)
    { %>
        <div class="<%= (Model.ApplicationContent.ReplyCount == 0) ? "empty-list" : "" %>">
            <% Html.RenderAction("CommentList", "Application", new { area = "Places", id = Model.ApplicationContent.Id, applicationId = Model.ApplicationContent.ApplicationId, applicationType = Model.ApplicationContent.ApplicationType, showHeader = true }); %>
        </div>

        <% if (Model.UserCanReply) { %>
		    <div class="add-reply-area">
                <%= Html.ReplyCommentCountButton(Model.ApplicationContent, Html.LocalizedString("ApplicationContent_Thread_AddReply"))%>
	        </div>
            <% Html.RenderPartial("CreateComment", new CreateCommentViewModel() { ThreadType = Model.ApplicationContent.ThreadType }); %>
        <% } %>
	<% } %>
</asp:Content>
