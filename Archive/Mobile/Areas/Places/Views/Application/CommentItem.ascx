﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.ApplicationContentReply>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Areas.Places.Controllers" %>

<li class="list-item">
	<div class="item-author">
		<span class="avatar"><%= Html.UserProfileLink(Model.Author, true)%></span>
	</div>
	<span class="item-user-name"><%= Html.LocalizedString("ApplicationContent_Index_CreatedBy") %> <%= Html.UserProfileLink(Model.Author, false)%></span>
	<span class="item-date"><%= Html.FormatAgoDate(Model.Date)%></span>
	<% if (Model.IsAnswer || Model.IsSuggestedAnswer) {%>
		<span class="item-status <%= (Model.IsAnswer) ? "answer" : "suggested-answer" %>"></span>
	<% } %>
	<div class="item-body user-defined-markup"><%= Model.Body%></div>
    <% if (Model.ThreadType == ThreadTypes.QuestionAndAnswer) {%>
        <div class="item-actions-area" id="<%= Model.Id %>">
            <% if (!Model.IsAnswer && !Model.IsSuggestedAnswer && Model.UserCanModify){ %>
                <%= Html.ThreadReplyActionLink(Model, Html.LocalizedString("ApplicationContent_Thread_SuggestAsAnswer"), "internal-link suggest-as-answer", (int)ThreadReplyActionType.SuggestAsAnswer)%>
            <% } %>

            <% if (!Model.IsAnswer && Model.UserCanVerify) { %>
                <span class='action-delimiter'>|</span>
                <%= Html.ThreadReplyActionLink(Model, Html.LocalizedString("ApplicationContent_Thread_VerifyAsAnswer"), "internal-link verify-as-answer", (int)ThreadReplyActionType.VerifyAsAnswer)%>
            <% } %>

            <% if ((Model.IsAnswer && Model.UserCanVerify) || (Model.IsSuggestedAnswer && Model.UserCanModify)) {%>
                <span class='action-delimiter'>|</span>
                <%= Html.ThreadReplyActionLink(Model, Html.LocalizedString("ApplicationContent_Thread_UnmarkAsAnswer"), "internal-link unmark-as-answer", (int)ThreadReplyActionType.UnmarkAsAnswer)%>
            <% } %>
            <span class="thread-status" style="display:none" id="<%= Model.ThreadStatus %>"></span>
        </div>
    <% } %>
</li>
