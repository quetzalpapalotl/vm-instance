<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.StatusMessageViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Models" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
	// <![CDATA[
	    $(function ()
	    {
	        $('#createMessageContainer').bind('telligent_mobile_createmessage_show', function () { $('.add-reply-area').hide(); })
                                        .bind('telligent_mobile_createmessage_close', function () { $('.add-reply-area').show(); })
                                        .bind('telligent_mobile_createmessage_success', function () { $('.list-view').trigger("telligent_mobile_list_reload", []) });
	        $('.list-view').bind('telligent_mobile_list_loaded', function (result) { $('.reply-count .add-reply').text($(this).find('li').length); });
	    });
        
	// ]]>
	</script>
	<ul class="item-list status parent">
		<li class="list-item">
			<div class="item-author">
				<span class="avatar"><%= Html.UserProfileLink(Model.StatusMessage.Author, true)%></span>
			</div>
			<div class="message-user-name"><%= Html.UserProfileLink(Model.StatusMessage.Author, false)%>:</div>
			<div class="message-body user-defined-markup"><%= Model.StatusMessage.MessageBody %></div>
			<div class="item-date"><%= Html.FormatAgoDate(Model.StatusMessage.CreatedDate)%></div>
			<div class="reply-count">
                <% if (Model.UserCanReply) { %> 
                    <%= Html.CreateStatusMessageButton(Model.StatusMessage.ReplyCount.ToString(), "internal-link add-reply", Model.StatusMessage, "replyArea")%>    
                <% } else {%>
				    <span class="internal-link add-reply"><%= Model.StatusMessage.ReplyCount%></span>
                <%} %>
			</div>
		</li>
	</ul>
	<ul class="item-list status replies">
		<% Html.RenderAction("ReplyList", "Status", new { area = "Activity", id = Model.StatusMessage.Id }); %>
	</ul>

    <% if(Model.UserCanReply) { %>
	    <div class="add-reply-area" id="replyArea">
            <%= Html.CreateStatusMessageButton(Html.LocalizedString("Status_AddAReply"), "internal-link add-reply", Model.StatusMessage, "replyArea")%>
	    </div>
    <% } %>

</asp:Content>
