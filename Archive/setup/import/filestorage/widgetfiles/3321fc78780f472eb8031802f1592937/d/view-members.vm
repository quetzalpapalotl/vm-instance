#set($sortBy = false)
#set($sortBy = $core_v2_page.GetQueryStringValue('SortBy'))
#if (!$sortBy)
    #set($sortBy = 'Username')
#end

#set($show = false)
#set($show = $core_v2_page.GetQueryStringValue('Show'))
#if (!$show)
	#set($show = '')
#end
#set($title = $core_v2_user.Accessing.ProfileFields.Get("Title").Value)

<div class="filter">
	<div class="query-filter">
		<span class="filter-label">$core_v2_language.GetResource('Hubs_SortBy')</span>
		<span class="filter-option#if($sortBy !='Activity' && $sortBy != 'Recent' && $sortBy != 'MembershipType' && $show != 'Pending') selected#end"><a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "SortBy=Username&$core_v2_ui.PageIndexQueryStringKey=1&Show="))">$core_v2_language.GetResource('Hubs_Username')</a></span>
		<span class="separator">|</span>
		<span class="filter-option#if($sortBy =='Activity') selected#end"><a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "SortBy=Activity&$core_v2_ui.PageIndexQueryStringKey=1&Show="))">$core_v2_language.GetResource('Hubs_RecentActivity')</a></span>
		<span class="separator">|</span>
		<span class="filter-option#if($sortBy =='Recent') selected#end"><a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "SortBy=Recent&$core_v2_ui.PageIndexQueryStringKey=1&Show="))">$core_v2_language.GetResource('Hubs_RecentUsers')</a></span>
		<span class="separator">|</span>
		<span class="filter-option#if($sortBy =='MembershipType') selected#end"><a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "SortBy=MembershipType&$core_v2_ui.PageIndexQueryStringKey=1&Show="))">$core_v2_language.GetResource('Hubs_MembershipType')</a></span>
		#if ($core_v2_nodePermission.Get('groups',$group.Id,'Group_ModifyMembership').IsAllowed)
			<span class="separator">|</span>
			<span class="filter-option#if($show =='Pending') selected#end"><a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "SortBy=&$core_v2_ui.PageIndexQueryStringKey=1&Show=Pending"))">$core_v2_language.GetResource('Groups_GroupMembershipList_PendingUsers')</a></span>
		#end
	</div>
</div>

#set($canModifyMembership = false)
#if($core_v2_nodePermission.Get('groups',$group.Id,'Group_ModifyMembership').IsAllowed)
	#set($canModifyMembership = true)
#end

#if($show == 'Pending')
	#set($options = "%{PageSize=1000,MembershipType='PendingMember',SortBy='Username',SortOrder='Ascending'}")
	$options.Add('PageIndex', $core_v2_ui.GetCurrentPageIndex())

	#set($members = $core_v2_groupUserMember.List($group.Id, $options))
	#foreach ($member in $members)
	#beforeall
		<div class="table-list-header"></div>
		<table class="table-list">
			<col width="40%" align="left" />
			<col width="40%" align="left" />
			<col width="20%" align="right" />
			<tbody>
	#each
        #if($member.User.ProfileFields.Get("Title").Value == $title)
		<tr class="table-item">
			<td class="table-column">
				<span class="avatar">
					#if ($member.User.ProfileUrl)
						<a class="internal-link view-user-profile" href="$core_v2_encoding.HtmlAttributeEncode($member.User.ProfileUrl)">
							$core_v2_ui.GetResizedImageHtml($member.User.AvatarUrl,29,29,"%{ style='border-width: 0px;', alt=$member.User.DisplayName}")
						</a>
					#else
						$core_v2_ui.GetResizedImageHtml($member.User.AvatarUrl,29,29,"%{ style='border-width: 0px;', alt=$member.User.DisplayName}")
					#end
				</span>
				<span class="user-name">
					#if ($member.User.ProfileUrl)
						<a class="internal-link view-user-profile" href="$core_v2_encoding.HtmlAttributeEncode($member.User.ProfileUrl)">$member.User.DisplayName</a>
					#else
						$member.User.DisplayName
					#end
					#if($member.User.Username != $member.User.DisplayName)
						($member.User.Username)
					#end
				</span>
			</td>
			<td class="table-column membership-status">$core_v2_language.GetResource('Hubs_PendingMember')</td>
			<td class="table-column">
				#if ($canModifyMembership)
					<div class="field-list-header"></div>
					<fieldset class="field-list">
						<ul class="field-list">
							<li class="field-item accept-member">
								<span class="field-item-input"><a href="#" data-userid="$member.User.Id" data-membershiptype="Member">$core_v2_language.GetResource('FriendRequest_Accept')</a></span> |
							</li>
							<li class="field-item reject-member">
								<span class="field-item-input"><a href="#" data-userid="$member.User.Id">$core_v2_language.GetResource('FriendRequest_Reject')</a></span>
							</li>
						</ul>
					</fieldset>
					<div class="field-list-footer"></div>
				#end
			</td>
		</tr>
        #end
	#afterall
			</tbody>
		</table>
		<div class="table-list-footer"></div>
	#nodata
		<div class="message norecords">$core_v2_language.GetResource('Groups_Membership_NoPendingRequests')</div>
	#end

	$core_v2_ui.Pager($members.PageIndex, $members.PageSize, $members.TotalCount)
#else
	#set($options = "%{PageSize=1000,MembershipType='Manager,Member,Owner'}")
	$options.Add('PageIndex', $core_v2_ui.GetCurrentPageIndex())

	#if($sortBy == 'Activity')
		$options.Add('SortBy', 'UserLastActiveDate')
		$options.Add('SortOrder', 'Descending')
	#elseif ($sortBy == 'Recent')
		$options.Add('SortBy', 'UserJoinedDate')
		$options.Add('SortOrder', 'Descending')
	#elseif ($sortBy == 'MembershipType')
		$options.Add('SortBy', 'MembershipType')
		$options.Add('SortOrder', 'Descending')
	#else
		$options.Add('SortBy', 'Username')
		$options.Add('SortOrder', 'Ascending')
	#end

	#set($members = $core_v2_groupUserMember.List($group.Id, $options))
	#foreach ($member in $members)
	#beforeall
		<div class="table-list-header"></div>
		<table class="table-list">
			<col width="40%" align="left" />
			<col width="40%" align="left" />
			<col width="20%" align="right" />
			<tbody>
	#each
		#if ($member && $member.User && $member.User.ProfileFields.Get("Title").Value == $title)
			<tr class="table-item">
				<td class="table-column">
					<span class="avatar">
						#if ($member.User.ProfileUrl)
							<a class="internal-link view-user-profile" href="$core_v2_encoding.HtmlAttributeEncode($member.User.ProfileUrl)">
								$core_v2_ui.GetResizedImageHtml($member.User.AvatarUrl,29,29,"%{ style='border-width: 0px;', alt=$member.User.DisplayName}")
							</a>
						#else
							$core_v2_ui.GetResizedImageHtml($member.User.AvatarUrl,29,29,"%{ style='border-width: 0px;', alt=$member.User.DisplayName}")
						#end
					</span>
					<span class="user-name">
						#if ($member.User.ProfileUrl)
							<a class="internal-link view-user-profile" href="$core_v2_encoding.HtmlAttributeEncode($member.User.ProfileUrl)">$member.User.DisplayName</a>
						#else
							$member.User.DisplayName
						#end
						#if($member.User.Username != $member.User.DisplayName)
							($member.User.Username)
						#end
					</span>
				</td>
				<td class="table-column membership-status">
					#if ($canModifyMembership && $group.GroupType != 'Joinless')
						<span class="field-item-input"><select class="membership-status" id="$core_v2_widget.UniqueId('EditUserMembershipType')" name="$core_v2_widget.UniqueId('EditUserMembershipType')" data-userid="$member.User.Id">
							<option value="Owner"#if($member.MembershipType == 'Owner') selected="selected"#end>$core_v2_language.GetResource('Hubs_MembershipType_Owner')</option>
							<option value="Manager"#if($member.MembershipType == 'Manager') selected="selected"#end>$core_v2_language.GetResource('Hubs_MembershipType_Manager')</option>
							<option value="Member"#if($member.MembershipType == 'Member') selected="selected"#end>$core_v2_language.GetResource('Hubs_MembershipType_Member')</option>
						</select></span>
					#else
						#if ($member.MembershipType == 'Owner') $core_v2_language.GetResource('Hubs_MembershipType_Owner')
						#elseif ($member.MembershipType == 'Manager') $core_v2_language.GetResource('Hubs_MembershipType_Manager')
						#elseif ($member.MembershipType == 'Member') $core_v2_language.GetResource('Hubs_MembershipType_Member')
						#else $member.MembershipType
						#end
					#end
				</td>
				<td class="table-column">
					#if ($canModifyMembership)
						<div class="field-list-header"></div>
						<fieldset class="field-list">
							<ul class="field-list">
								<li class="field-item delete-member">
									<span class="field-item-input"><a href="#" data-userid="$member.User.Id">$core_v2_language.GetResource('Remove')</a></span>
								</li>
							</ul>
						</fieldset>
						<div class="field-list-footer"></div>
					#end
				</td>
			</tr>
		#end
	#afterall
			</tbody>
		</table>
		<div class="table-list-footer"></div>
	#end
	$core_v2_ui.Pager($members.PageIndex, $members.PageSize, $members.TotalCount)
#end