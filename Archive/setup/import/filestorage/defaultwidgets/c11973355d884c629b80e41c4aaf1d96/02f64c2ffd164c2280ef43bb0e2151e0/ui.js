(function($)
{
	if (typeof $.telligent === 'undefined') $.telligent = {};
	if (typeof $.telligent.evolution === 'undefined') $.telligent.evolution = {};
	if (typeof $.telligent.evolution.widgets === 'undefined') $.telligent.evolution.widgets = {};

	var saveTags = function(context, tags, successFn)
		{
			var data = {
				Tags: tags.length > 0 ? tags.join(',') : '',
				FileId: context.mediaId,
				MediaGalleryId: context.galleryId
			};

			$.telligent.evolution.put({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/media/' + context.galleryId + '/files/' + context.mediaId + '.json',
				data: data,
				dataType: 'json',
				success: function(response)
				{
					successFn();
					context.currentTags = tags;

					var tagsHtml = '';
					for(var i = 0; i < context.currentTags.length; i++)
					{
						var tagName = $.trim(context.currentTags[i]);
						var tagUrl = context.tagUrlTemplate.replace(/TAGNAME/g, encodeURIComponent(tagName.replace(/ /g, '+')));
						tagsHtml = tagsHtml + '<a href="' + tagUrl + '">' + $('<div/>').text(tagName).html() + '</a>';
						if (i + 1 < context.currentTags.length)
							tagsHtml = tagsHtml + ', ';
					}

					context.tagListContainer.html(tagsHtml);
				}
			});
		},
		ratePost = function(context, value)
		{
			$.telligent.evolution.post({
				url: context.ratingUrl,
				data: {rating:value,mediaId:context.mediaId},
				dataType: 'json',
				success: function(response)
				{
					context.ratingControl.evolutionStarRating('val', response.ratingAverage);
				}
			});
		};

	$.telligent.evolution.widgets.mediaGalleryPost =
	{
		register: function(context)
		{
			if (context.tagEditorButton)
				context.tagEditorButton.evolutionInlineTagEditor({
					allTags: context.allTags,
					currentTags: context.currentTags,
					selectTagsText: context.selectTagsText,
					onSave: function(tags, successFn) {
						saveTags(context, tags, successFn);
					}
				});

			if (context.ratingControl)
				context.ratingControl.evolutionStarRating({
						value: context.averageRating,
						isReadOnly: context.hasRatePermission,
						onRate: function(value, success) {
							ratePost(context, value, success);
						}
				});
		}
	};
})(jQuery);
