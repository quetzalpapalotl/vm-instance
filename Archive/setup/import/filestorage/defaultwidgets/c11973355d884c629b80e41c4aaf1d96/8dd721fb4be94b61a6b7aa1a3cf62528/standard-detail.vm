#if($sortOrder == 'Ascending')
	#set($alternateSortOrder = 'Descending')
#else
	#set($alternateSortOrder = 'Ascending')
#end

<div class="filter detail">
	<div class="view-type">
		<span class="filter-option#if($viewType=='Thumbnail') selected#end"><a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, 'GalleryViewType=Thumbnail'))" class="internal-link view-thumbnail-list">$core_v2_language.GetResource('MediaGalleryViewType_Thumbnail_Link')</a></span>
		<span class="filter-option#if($viewType=='List') selected#end"><a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, 'GalleryViewType=List'))" class="internal-link view-detail-list">$core_v2_language.GetResource('MediaGalleryViewType_List_Link')</a></span>
	</div>
</div>

#foreach($media in $medias)
#beforeall
	<div class="table-list-header"></div>
	<table class="table-list">
		<thead>
			<tr class="table-header">
				<th class="table-header-column post-name"><a class="internal-link#if($sortBy=='Subject' && $sortOrder=='Ascending') sort-ascending#else sort-descending#end#if($sortBy=='Subject') selected#end" href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "GalleryPostSort=Subject&PageIndex=1&SortOrder=$alternateSortOrder"))"><span></span>$core_v2_language.GetResource('MediaGalleries_FileList_Title')</a></span></th>
				<th class="table-header-column post-type"><a name="post-type" class="internal-link sort-ascending selected">$core_v2_language.GetResource('MediaGalleries_FileList_Type')</a></th>
				<th class="table-header-column post-group">Product</a></th>
				<th class="table-header-column post-date"><a class="internal-link#if($sortBy=='PostDate' && $sortOrder=='Ascending') sort-ascending#else sort-descending#end#if($sortBy=='PostDate') selected#end" href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "GalleryPostSort=PostDate&PageIndex=1&SortOrder=$alternateSortOrder"))"><span></span>$core_v2_language.GetResource('MediaGalleries_FileList_Date')</a></span></th>
				<th class="table-header-column post-downloads"><a class="internal-link#if($sortBy=='Downloads' && $sortOrder=='Ascending') sort-ascending#else sort-descending#end#if($sortBy=='Downloads') selected#end" href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "GalleryPostSort=Downloads&PageIndex=1&SortOrder=$alternateSortOrder"))"><span></span>$core_v2_language.GetResource('MediaGalleries_FileList_Downloads')</a></span></th>
				<th class="table-header-column post-rating"><a class="internal-link#if($sortBy=='Rating' && $sortOrder=='Ascending') sort-ascending#else sort-descending#end#if($sortBy=='Rating') selected#end" href="$core_v2_encoding.HtmlAttributeEncode($core_v2_page.AdjustQueryString($core_v2_page.Url, "GalleryPostSort=Rating&PageIndex=1&SortOrder=$alternateSortOrder"))"><span></span>$core_v2_language.GetResource('MediaGalleries_FileList_Rating')</a></span></th>
				<th class="table-header-column post-group">Size (KB)</a></th>
			</tr>
		</thead>
		<tbody>
#each
	<tr class="table-item">
		<td class="table-column">
			<div class="abbreviated-post-header"></div>
			<div class="abbreviated-post">
				<h4 class="post-name"><a href="$core_v2_encoding.HtmlAttributeEncode($media.Url)" class="internal-link view-post"><span></span>$media.Title</a></h4>
				<div class="post-author">
					<span class="avatar">$core_v2_ui.GetResizedImageHtml($media.Author.AvatarUrl,32,32,"%{alt=$media.Author.DisplayName}")</span>
					<span class="user-name">
						#if ($activity.Author.ProfileUrl)
							<a href="$core_v2_encoding.HtmlAttributeEncode($activity.Author.ProfileUrl)" class="internal-link view-user-profile">
								$activity.Author.DisplayName
							</a>
						#else
							$activity.Author.DisplayName
						#end
					</span>
				</div>
				<div class="post-thumbnail"><a href="$core_v2_encoding.HtmlAttribute($media.Url)">$core_v2_ui.GetPreviewHtml($media.File.FileUrl, "%{width=114,height=114}")</a></div>
				<div class="post-summary">$core_v2_language.Truncate($media.Body(), 100, '...')</div>
				#foreach($tag in $media.Tags)
				#beforeall
					<div class="post-tags">
				#each
					<a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_mediaUrls.Tags($core_v2_gallery.Current.Id, $core_v2_utility.MakeList($tag)))" rel="tag">$tag</a>
				#afterall
					</div>
				#end
				<div class="post-application">
					<span class="label">$core_v2_language.GetResource('Postedto'):</span>
					<span class="value"><a href="$core_v2_encoding.HtmlAttributeEncode($core_v2_gallery.Current.Url)" class="internal-link view-application"><span></span>$core_v2_gallery.Current.Name</a></span>
				</div>
			</div>
			<div class="abbreviated-post-footer"></div>
		</td>
		<td class="table-column">
			<a href="$core_v2_encoding.HtmlAttributeEncode($media.Url)" class="internal-link view-post"><span></span><div class="post-preview">$!core_v2_ui.GetPreviewHtml($media.File.FileUrl, "%{width=32,height=32}")</div></a>
		</td>
		<td class="table-column">
			#set ($group = $core_v2_group.Get("%{Id = $media.GroupId}"))
			<a href="$core_v2_encoding.HtmlAttributeEncode($group.Url)" class="internal-link view-post">$group.Name</a>
		</td>
		<td class="table-column">
			<span class="post-date">$core_v2_language.FormatDate($media.Date)</span>

			<span class="user-name">
				$core_v2_language.GetResource('by') #if ($media.Author.ProfileUrl)
					<a href="$core_v2_encoding.HtmlAttributeEncode($media.Author.ProfileUrl)" class="internal-link view-user-profile">
						<span></span>$media.Author.DisplayName
					</a>
				#else
					<span></span>$media.Author.DisplayName
				#end
			</span>
		</td>
		<td class="table-column post-attachment-download-count">
			<span class="post-attachment-download-count">$!media.Downloads</span>
		</td>
		<td class="table-column post-rating">
			<div class="post-rating">
				#set($rating = false)
				#set($rating = $media.RatingSum / $media.RatingCount)
				#if($rating)
					<span id="$core_v2_widget.UniqueId('rating')-$media.Id"></span>
					#registerEndOfPageHtml()
						<script type="text/javascript">
						jQuery(function(j){
							j('#$core_v2_widget.UniqueId('rating')-$media.Id').evolutionStarRating({
								value: $rating,
								isReadOnly: true,
								imagesPathUrl: '$core_v2_encoding.JavascriptEncode($core_v2_page.AbsoluteUrl('~/Utility/images/small-stars/'))'
							});
						});
						</script>
					#end
				#end
			</div>
		</td>
		<td class="table-column">
			$media.File.FileSize
		</td>
	</tr>
#nodata
	<div class="message norecords">$core_v2_language.GetResource('NoRecords')</div>
#afterall
		</tbody>
		</table>
		<div class="table-list-footer"></div>
#end