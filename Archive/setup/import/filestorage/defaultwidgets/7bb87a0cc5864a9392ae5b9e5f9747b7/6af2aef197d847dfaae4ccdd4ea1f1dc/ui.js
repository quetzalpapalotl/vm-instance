(function($, global)
{
    if (typeof $.telligent === 'undefined')
            $.telligent = {};

    if (typeof $.telligent.evolution === 'undefined')
        $.telligent.evolution = {};

    if (typeof $.telligent.evolution.widgets === 'undefined')
        $.telligent.evolution.widgets = {};

    var _attachHandlers = function(context) {
        $(context.languageSelector).change(function()
        {
            var selectedIndex = $(context.dateFormatSelector).get(0).selectedIndex;

            $.telligent.evolution.get({
                url: context.getDateFormatsUrl,
                data:
                {
                    w_language: $(this).val()
                },
                success: function(response)
                {
                    $(context.dateFormatSelector).html(response);
                    $(context.dateFormatSelector).get(0).selectedIndex = selectedIndex;
                }
            });
        });

        $(':radio', $(context.birthdayOptionSelector)).click(function()
        {
            $(context.birthdaySelector).glowDateTimeSelector('disabled', $(':radio:checked', $(context.birthdayOptionSelector)).val() != 'Set');
        });

        if (!context.hasPassword)
        {
            $(context.resetPasswordSelector).click(function()
            {
                $.telligent.evolution.post({
                    url: context.resetPasswordUrl,
                    data: { },
                    dataType: 'json',
                    success: function(response)
                    {
                        if (response.message)
                        {
                            $(context.resetPasswordMessageSelector).html(response.message).show();
                        }
                    },
                    defaultErrorMessage: context.resetPasswordError
                });

                return false;
            });
        }

        $('input.notificationType').on('change', function(){
            var row = $(this).closest('tr');
            if($(this).is(':checked')) {
                row.removeClass('disabled');
                row.find('.distributionType').removeAttr('disabled');
            } else {
                row.addClass('disabled');
                row.find('.distributionType').attr('disabled','disabled');
            }
        });
    },
    _addValidation = function(context) {
        var w = $('#' + context.wrapperId);
        var saveButton = $('a.submit-button', w);
        var tabbedPanes = $('#' + context.tabId);

        saveButton.evolutionValidation({
            onValidated: function(isValid, buttonClicked, c) {
                if (isValid)
                    saveButton.removeClass('disabled');
                else {
                    saveButton.addClass('disabled');

                    if (c)
                    {
                        var tabbedPane = tabbedPanes.glowTabbedPanes('getByIndex', c.tab);
                        if (tabbedPane)
                            tabbedPanes.glowTabbedPanes('selected', tabbedPane);
                    }
                }
            },
            onSuccessfulClick: function(e) {
                $('.processing', saveButton.parent()).css("visibility", "visible");
                saveButton.addClass('disabled');
                _save(context);
            }
        });

        saveButton.evolutionValidation('addField',
            context.publicEmailSelector,
            {
                email: true
            },
            $(context.publicEmailSelector).closest('.setting-item').find('.setting-item-validation'), {tab: 0 });

        saveButton.evolutionValidation('addField',
            context.privateEmailSelector,
            {
                required: true,
                email: true
            },
            $(context.privateEmailSelector).closest('.email-setting-item-group').find('.email-setting-item-validation'), {tab: 3 });

        saveButton.evolutionValidation('addCustomValidation', context.rssFeedsSelector, function()
            {
                var allFeeds = $(context.rssFeedsSelector).val();
                if (allFeeds) {
                    var regexp = new RegExp("^(http|https)\://.*", "i");
                    allFeeds = allFeeds.split("\n");
                    for (var i = 0; i < allFeeds.length; i++) {
                        if (!regexp.test($.trim(allFeeds[i])))
                            return false;
                    }
                }
                return true;
            },
            context.rssFeedsValidationText, $(context.rssFeedsSelector).closest('.setting-item').find('.setting-item-validation'), { tab: 0 }
        );
        $(context.rssFeedsSelector).blur(function() {
            saveButton.evolutionValidation('validate');
        });

        if (context.signatureMaxLength > 0)
        {
            context.attachSignatureChangeScript(saveButton.evolutionValidation('addCustomValidation', 'signature', function()
                {
                    return context.getSignature().length < context.signatureMaxLength;
                },
                context.signatureMaxLengthText, $(context.signatureValidationSelector), { tab: 0 }
            ));
        }
    },
    _save = function(context) {
        _saveNotificationPreferences(context).then(function(){
            _saveUser(context)
                .done(function(response){
                    $(context.saveSucessSelector).show();
                    $('#' + context.wrapperId + ' .processing').css("visibility", "hidden");
                    $('#' + context.wrapperId + ' a.submit-button').removeClass('disabled');
                    global.scrollTo(0, 0);
                })
                .fail(function(xhr, desc, ex){
                    $.telligent.evolution.notifications.show(desc,{type:'error'});
                    $('#' + context.wrapperId + ' .processing').css("visibility", "hidden");
                    $('#' + context.wrapperId + ' a.submit-button').removeClass('disabled');
                });
        });
    },
    _saveNotificationPreference = function(preference) {
        return $.telligent.evolution.put({
            url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/notificationpreference.json',
            data: preference,
            dataType: 'json'
        });
    },
    _collectNotificationPreferences = function(context) {
        var wrapper = $('#' + context.wrapperId),
            preferences = [];
        wrapper.find('input.notificationType').each(function(){
            var notificationTypeCheckbox = $(this),
                notificationTypeId = notificationTypeCheckbox.data('notificationtype');

            if(notificationTypeCheckbox.is(':checked')) {
                // notification type enabled

                // if not a change from previous, don't update
                if(!notificationTypeCheckbox.data('enabled')) {
                    preferences.push(function(){
                        return _saveNotificationPreference({
                            NotificationTypeId: notificationTypeId,
                            IsEnabled: true
                        });
                    });
                }
            } else if(!notificationTypeCheckbox.is(':checked') && notificationTypeCheckbox.data('enabled')) {
                // notification not enabled
                preferences.push(function(){
                    return _saveNotificationPreference({
                        NotificationTypeId: notificationTypeId,
                        IsEnabled: false
                    });
                });
            }

            // find distribution types for enabled notification type
            wrapper.find('input.distributionType[data-notificationtype="' + notificationTypeId + '"]').each(function(){
                var distributionTypeCheckbox = $(this),
                    distributionTypeId = distributionTypeCheckbox.data('distributiontype'),
                    enabled = distributionTypeCheckbox.is(':checked');

                // if not a change from previous, don't update
                if(enabled == distributionTypeCheckbox.data('enabled')) {
                    return;
                }

                // distribution type enabled or disabled
                preferences.push(function(){
                    return _saveNotificationPreference({
                        NotificationTypeId: notificationTypeId,
                        DistributionTypeId: distributionTypeId,
                        IsEnabled: enabled
                    });
                });
            });
        });

        return preferences;
    },
    _saveNotificationPreferences = function(context) {
        var preferenceUpdates = $.map(_collectNotificationPreferences(context), function(instruction) { return instruction(); });
        // return a promise which completes when all preferences are done
        return $.when.apply($, preferenceUpdates);
    },
    _saveUser = function(context) {
        $(context.saveSucessSelector).hide();

        var rssFeeds = [];
        var allFeeds = $(context.rssFeedsSelector).val();
        if (allFeeds)
        {
            allFeeds = allFeeds.split('\n');
            for (var i = 0; i < allFeeds.length; i++)
            {
                var feedUrl = $.trim(allFeeds[i]);
                if (feedUrl)
                {
                    rssFeeds[rssFeeds.length] = feedUrl;
                }
            }
        }

        var data = {
            Language: $(context.languageSelector).val(),
            PrivateEmail: $(context.privateEmailSelector).val(),
            PublicEmail: $(context.publicEmailSelector).val(),
            EnableFavoriteSharing: $('input:checked', $(context.sharedFavoritesSelector)).val() == 'on',
            RssFeeds: rssFeeds.join(','),
            AllowSiteToContact: $(context.allowSiteContactSelector).is(':checked'),
            AllowSitePartnersToContact: $(context.allowSitePartnersContactSelector).is(':checked'),
            EditorType: $(context.editorListSelector).val(),
            EnableDisplayName: $(context.enableDisplayNamesSelector).is(':checked'),
            EnableDisplayInMemberList: $(context.enableDisplayInMembersSelector).is(':checked'),
            EnableEmoticons: $(context.enableEmoticonsSelector).is(':checked'),
            TimeZoneId: $(context.timezoneSelector).val(),
            DateFormat: $(context.dateFormatSelector).val(),
            UserId : context.userId
        };

        if ($(context.receiveEmailsSelector).length > 0)
            data.ReceiveEmails = $(context.receiveEmailsSelector).is(':checked');
        if ($(context.enableHtmlEmailSelector).length > 0)
            data.EnableHtmlEmail = $(context.enableHtmlEmailSelector).is(':checked');
        if ($(context.enableEmailTrackingSelector).length > 0)
            data.EnableTracking = $(context.enableEmailTrackingSelector).is(':checked');
        if ($(context.enableCommentNotificationSelector).length > 0)
            data.EnableCommentNotifications = $(context.enableCommentNotificationSelector).is(':checked');
        if ($(context.enableConversationNotificationSelector).length > 0)
            data.EnableConversationNotifications = $(context.enableConversationNotificationSelector).is(':checked');

        if ($(context.commonNameSelector).length > 0)
        {
            data.DisplayName = $(context.commonNameSelector).val();
        }

        if (context.getSignature)
        {
            data.Signature = context.getSignature();
        }

        if ($(context.enableUserSignaturesSelector).length > 0)
        {
            data.EnableUserSignatures = $(context.enableUserSignaturesSelector).is(':checked');
        }

        if ($(':radio:checked', $(context.birthdayOptionSelector)).val() == 'Set')
        {
            data.Birthday = $.telligent.evolution.formatDate($(context.birthdaySelector).glowDateTimeSelector('val'));
            data.IsBirthdaySet = true;
        }
        else
        {
            data.IsBirthdaySet = false;
        }

        if ($(context.genderSelector).length > 0)
        {
            data.Gender = $('input:checked', $(context.genderSelector)).val();
        }

        if ($(context.conversationContactType).length > 0)
        {
            data.ConversationContactType = $('input:checked', $(context.conversationContactType)).val();
        }

        return $.telligent.evolution.put({
            url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{UserId}.json?IncludeFields=User.Id',
            data: data,
            dataType: 'json',
            defaultErrorMessage: context.saveErrorText
        });
    },
    _attachMultiSelectors = function(context) {
        var wrapper = $('#' + context.wrapperId);
        wrapper.find('.multiSelector').each(function(){
            var multiSelector = $(this),
                selectables = wrapper.find('.multiSelectable[data-category="' + multiSelector.data('category') + '"]'),
                allSelected = selectables.filter(':checked').length === selectables.length;
            if(multiSelector.hasClass('all')) {
                // selects all
                var alternate = wrapper.find('.multiSelector.none[data-category="' + multiSelector.data('category') + '"]');
                multiSelector.on('click', function(){
                    selectables.attr('checked','checked').change();
                    alternate.show();
                    multiSelector.hide();
                    return false;
                });
                if(!allSelected) {
                    multiSelector.show();
                } else {
                    alternate.show();
                }
            } else {
                // selects none
                var alternate = wrapper.find('.multiSelector.all[data-category="' + multiSelector.data('category') + '"]');
                multiSelector.on('click', function(){
                    selectables.removeAttr('checked').change();
                    alternate.show();
                    multiSelector.hide();
                    return false;
                });
            }
        });
    };

    $.telligent.evolution.widgets.editUser = {
        register: function(context) {
            var w = $('#' + context.wrapperId)

            $('#' + context.tabId, w).glowTabbedPanes({
                cssClass:'',
                tabSetCssClass:'tab-set with-panes',
                tabCssClasses:['tab'],
                tabSelectedCssClasses:['tab selected'],
                tabHoverCssClasses:['tab hover'],
                enableResizing:false,
                tabs:
                [
                    [context.basicTabId, context.basicTabText, null],
                    [context.advancedTabId, context.advancedTabText, null],
                    [context.signinTabId, context.signinTabText, null],
                    [context.emailTabId, context.emailTabText, null],
                    [context.notificationTabId, context.notificationTabText, null]
                ]
            });

            $(context.birthdaySelector).glowDateTimeSelector({
                'showPopup':true,
                'allowBlankvalue':false,
                'pattern': '<1000-3000>-<01-12>-<01-31>',
                'yearIndex' : 0,
                'monthIndex': 1,
                'dayIndex': 2
            });

            $('#' + context.emailDigestTabsId, w).glowTabbedPanes({
                cssClass:'',
                tabSetCssClass:'tab-set with-panes',
                tabCssClasses:['tab'],
                tabSelectedCssClasses:['tab selected'],
                tabHoverCssClasses:['tab hover'],
                enableResizing:false,
                tabs:
                [
                    [context.emailDigestGroupTabId, context.emailDigestGroupTabText, null]
                ]
            });

            _addValidation(context);
            _attachHandlers(context);
            _attachMultiSelectors(context);
        }
    };
})(jQuery, window);
