(function(j, global){

	if (typeof j.telligent === 'undefined')
		j.telligent = {};

	if (typeof j.telligent.evolution === 'undefined')
		j.telligent.evolution = {};

	if (typeof j.telligent.evolution.widgets === 'undefined')
		j.telligent.evolution.widgets = {};

	j.telligent.evolution.widgets.groupMembership = {
		register: function(options) {
			// activate group joining buttons
			options.joinButton.click(function(){
				var data = {
					GroupId: options.groupId,
					UserId: options.userId,
					GroupMembershipType: 'Member'
				};
				// if there was a message to include, then
				// this include the message and also declare this is to create a PendingMembership
				if(options.messageTextArea.is(':visible')) {
					j.extend(data, {
						Message: options.messageTextArea.val(),
						GroupMembershipType: 'PendingMember'
					});
				}
				j.telligent.evolution.put({
					url: options.joinUrl,
					data: data,
					success: function() { global.location.reload(); }
				});
				return false;
			});

			// activate membership cancellation links
			options.cancelLink.click(function(){
				if(confirm(options.cancelConfirmMessage)) {
					j.telligent.evolution.del({
						url: options.cancelUrl,
						data: {GroupId:options.groupId, UserId:options.userId},
						success: function(){ global.location.href = options.groupUrl; }
					});
				}
				return false;
			});
		}
	};

})(jQuery, window);
