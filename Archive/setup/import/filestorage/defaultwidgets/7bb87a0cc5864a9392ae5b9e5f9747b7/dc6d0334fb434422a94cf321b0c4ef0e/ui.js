(function($, global) {
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var AppealService = (function(){
		var updateAppeal = function(appealId, reason, state, complete) {
			$.telligent.evolution.put({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/abuseappeals/{AppealId}.json',
				data: {
					AppealId: appealId,
					BoardResponse: reason,
					AppealState: state
				},
				success: function(response) {
					complete(response);
				}
			})
		};
		return {
			accept: function(appealId, reason, complete) {
				updateAppeal(appealId, reason, 'Accepted', complete);
			},
			reject: function(appealId, reason, complete) {
				updateAppeal(appealId, reason, 'Rejected', complete);
			}
		}
	})();

	var buildContext = function(context) {
		context.appealStateFilter = $(context.appealStateFilter);
		initGroupSelector(context);
		context.sortBy = $(context.sortBy);
		context.pagedContentWrapper = $(context.pagedContentWrapper);
		return context;
	},
	bindEvents = function(context) {
		context.appealStateFilter.bind('change', function(){ filter(context); });
		context.groupSelector.bind('glowLookUpTextBoxChange', function(){ filter(context); });
		context.sortBy.bind('change', function(){ filter(context); });
		context.pagedContentWrapper.delegate('a.begin-reject', 'click', function(e){
			e.preventDefault();
			var appeal = $(e.target).closest('.content-item.appeal'),
				beginActions = appeal.find('.post-actions.begin'),
				rejectActions = appeal.find('.post-actions.reject'),
				hiddenContent = appeal.find('.post-content.content'),
				contentResponse = appeal.find('.post-content.field-list');
			hiddenContent.hide();
			contentResponse.show();
			beginActions.hide();
			rejectActions.show();
		});
		context.pagedContentWrapper.delegate('a.begin-accept', 'click', function(e){
			e.preventDefault();
			var appeal = $(e.target).closest('.content-item.appeal'),
				beginActions = appeal.find('.post-actions.begin'),
				acceptActions = appeal.find('.post-actions.accept'),
				hiddenContent = appeal.find('.post-content.content'),
				contentResponse = appeal.find('.post-content.field-list');
			hiddenContent.hide();
			contentResponse.show();
			beginActions.hide();
			acceptActions.show();
		});
		context.pagedContentWrapper.delegate('a.back', 'click', function(e){
			e.preventDefault();
			var appeal = $(e.target).closest('.content-item.appeal'),
				beginActions = appeal.find('.post-actions.begin'),
				rejectActions = appeal.find('.post-actions.reject'),
				acceptActions = appeal.find('.post-actions.accept'),
				hiddenContent = appeal.find('.post-content.content'),
				contentResponse = appeal.find('.post-content.field-list');
			contentResponse.hide();
			acceptActions.hide();
			rejectActions.hide();
			hiddenContent.show();
			beginActions.show();
		});
		context.pagedContentWrapper.delegate('a.reject','click',function(e){
			e.preventDefault();
			handleReject(context, $(this).closest('li.appeal'));
		});
		context.pagedContentWrapper.delegate('a.accept','click',function(e){
			e.preventDefault();
			handleAccept(context, $(this).closest('li.appeal'));
		});
		context.pagedContentWrapper.delegate('a.expander', 'click', function(e){
			e.preventDefault();
			var link = $(e.target),
				appealItem = link.closest('.content-item.appeal');
			if(appealItem.hasClass('expanded')) {
				appealItem.removeClass('expanded');
				recalculatePosition(appealItem.parent());
			} else {
				loadAppeal(appealItem.data('appealid'), appealItem, context);
			}
		});
	},
	handleReject = function(context, appealListItem) {
		var reason = appealListItem.find('textarea');
		reason.attr('disabled','disabled');
		AppealService.reject(appealListItem.data('appealid'), reason.val(), function(response) {
			loadAppeal(appealListItem.data('appealid'), appealListItem, context);
		});
	},
	handleAccept = function(context, appealListItem) {
		var reason = appealListItem.find('textarea');
		reason.attr('disabled','disabled');
		AppealService.accept(appealListItem.data('appealid'), reason.val(), function(response) {
			loadAppeal(appealListItem.data('appealid'), appealListItem, context);
		});
	},
	loadAppeal = function(appealId, li, context) {
		$.telligent.evolution.get({
			url: context.singleUrl,
			data: { AppealId: appealId },
			success: function(response) {
				var ul = li.parent();
				li.replaceWith(response);
				recalculatePosition(ul);
			}
		})
	},
	filter = function(context) {
		var data = {};
		if(context.appealStateFilter.val().length > 0) {
			data.w_appealState = context.appealStateFilter.val();
		}
		if(context.groupSelector.val().length > 0) {
			data.w_groupId = context.groupSelector.val();
		}
		if(context.sortBy.val().length > 0) {
			data.w_sortBy = context.sortBy.val();
		}
		$.telligent.evolution.get({
			url: context.listUrl,
			data: data,
			success: function (response) {
				context.pagedContentWrapper.html(response);
			}
		});
	},
	spinner = '<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>',
	searchGroups = function(context, textbox, searchText) {
		if(searchText && searchText.length >= 2) {

			textbox.glowLookUpTextBox('updateSuggestions', [
				textbox.glowLookUpTextBox('createLookUp', '', spinner, spinner, false)
			]);

			$.telligent.evolution.get({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups.json',
				data: {
					GroupNameFilter: searchText,
					Permission: 'Group_ReviewAbuseAppeals'
				},
				success: function(response) {
					if (response && response.Groups.length >= 1) {
						textbox.glowLookUpTextBox('updateSuggestions',
							$.map(response.Groups, function(group, i) {
								return textbox.glowLookUpTextBox('createLookUp', group.ContainerId, group.Name, group.Name, true);
							}));
					} else {
						textbox.glowLookUpTextBox('updateSuggestions', [
							textbox.glowLookUpTextBox('createLookUp', '', context.noGroupsMatchText, context.noGroupsMatchText, false)
						]);
					}
				}
			});
		}
	},
	initGroupSelector = function(context) {
		context.groupSelector
			.glowLookUpTextBox({
				delimiter: ',',
				allowDuplicates: true,
				maxValues: 1,
				onGetLookUps: function(tb, searchText) {
					searchGroups(context, tb, searchText);
				},
				emptyHtml: '',
				selectedLookUpsHtml: [],
				deleteImageUrl: ''});

		if(context.defaultGroupName && context.defaultGroupId) {
			context.groupSelector.glowLookUpTextBox('add',
				context.groupSelector.glowLookUpTextBox('createLookUp',
					context.defaultGroupId,
					context.defaultGroupName,
					context.defaultGroupName,
					true));
		}
	},
	recalculatePosition = function (ulElem) {
		if ($.browser.msie && $.browser.version == '7.0') {
			$.each(ulElem.children('li'), function(i, n) {
				$(n).css('display', 'block');
				$(n).css('display', 'inline-block');
			});
		}
	};

	$.telligent.evolution.widgets.appealQueue = {
		register: function(context) {
			context = buildContext(context);
			bindEvents(context);
		}
	};
}(jQuery, window));
