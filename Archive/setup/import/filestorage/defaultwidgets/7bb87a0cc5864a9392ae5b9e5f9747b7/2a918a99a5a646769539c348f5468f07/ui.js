(function($) {
    var _attachHandlers = function(context) {
            var loadMore = $(context.wrapper + ' .load-more a');
            if (context.endlessScroll) {
                context.hasMore = loadMore.length > 0;
                loadMore.parents('li').hide();
            } else {
                loadMore.click(function() {
                    _loadNextPage(context, context.pageIndex + 1);
                    return false;
                });
            }
        },
        _isLoadingMore = false,
        _loadNextPage = function(context, pageIndex) {
            _isLoadingMore = true;
            $.telligent.evolution.get({
                url: context.loadNextPageUrl,
                data: {
            		UserId: context.userId,
                    PageIndex: pageIndex
            	},
            	success: function(response) {
                    $(context.wrapper + ' .load-more a').parents('li').show().replaceWith(response);
                    context.pageIndex = pageIndex;
                    _attachHandlers(context);
                    _isLoadingMore = false;
            	}
            });
        };
	var api = {
		register: function(context) {
            context.pageIndex = 0;
			_attachHandlers(context);

            if (context.endlessScroll) {
                $(document).bind('scrollend', function() {
                    if (context.hasMore && !_isLoadingMore) {
                        _loadNextPage(context, context.pageIndex + 1);
                    }
                });
            }
		}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.userMentionList = api;

}(jQuery));