(function($) {

    $.telligent = $.telligent || {};
    $.telligent.evolution = $.telligent.evolution || {};
    $.telligent.evolution.widgets = $.telligent.evolution.widgets || {};

    var messaging = $.telligent.evolution.messaging,
        events = {
            conversationListPaged: 'widget.conversationlist.conversations.paged',
            messageListPaged: 'widget.conversationlist.messages.paged',
            conversationDeleted: 'widget.conversationlist.messages.deleted',
            messageAdded: 'widget.conversationlist.messages.added',
            conversationAdd: 'widget.conversationList.messages.add',
            conversationCountChanged: 'widget.conversationList.conversations.conversationCountChanged',
            notificationRaised: 'notification.raised',
            chatReceived: 'chat.messageReceived',
            messageRead: 'ui.messageread'
        };

    /*
     * Conversation Model
     * Constructor:
     * Methods:
     *   list
     *   get
     *   del
     *   add
     */
    var ConversationModel = (function(){
        var defaults = {
            conversationListUrl: '',
            messageListUrl: '',
            deleteUrl: ''
        };
        return function(options) {
            var settings = $.extend({}, defaults, options || {});

            return {
                list: function(pageIndex) {
                    return $.telligent.evolution.get({
                        url: settings.conversationListUrl,
                        data: {
                            pageIndex: pageIndex || 0
                        },
                        cache: false,
                    });
                },
                get: function(conversationId, pageIndex) {
                    return $.telligent.evolution.get({
                        url: settings.messageListUrl,
                        data: {
                            conversationId: conversationId,
                            pageIndex: pageIndex || 0
                        },
                        cache: false,
                    });
                },
                del: function(conversationId) {
                    return $.telligent.evolution.post({
                        url: settings.deleteUrl,
                        data: {
                            conversationId: conversationId
                        }
                    });
                },
                reply: function(conversationId, body) {
                    return $.telligent.evolution.post({
                        url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/conversations/{ConversationId}/messages.json',
                        data: {
                            ConversationId: conversationId,
                            Body: body
                        },
                        cache: false,
                    });
                }
            };
        };
    })();

    /*
     * ConversationListView
     * Constructor:
     *   view
     * Events
     *   paged (pageIndex)
     *   conversationCountChanged (count)
     * Methods
     *   render(renderedConversations)
     *   append(renderedConversations)
     *   removeConversation(conversationId)
     *   viewing(conversationId)
     *   scale()
     */
    var ConversationListView = (function(){
        var defaults = {
            view: ''
        };

        // determine if this content is enough to cause scrolling
        // if not, try asking for more pages until it does or max attempts reached
        function pageUntilScrolls(context) {
            if(!context.hasMore)
                return;
            var contentHeight = 0;
            context.view.find('li.content-item.conversation').each(function(){
                contentHeight += $(this).height();
            });
            if(contentHeight < context.view.height() && context.pagedContentAttempts < 10) {
                context.pagedContentAttempts++;
                context.pageIndex++;
                messaging.publish(events.conversationListPaged, {
                    pageIndex: context.pageIndex
                });
            } else {
                context.pagedContentAttempts = 0;
            }
        }

        function handleEvents(context) {
            // infinite paging when scrolling down
            context.view.on('scrollend', function(){
                if(!context.hasMore)
                    return;
                context.pageIndex++;
                messaging.publish(events.conversationListPaged, {
                    pageIndex: context.pageIndex
                });
            });

            context.view.on('click', 'li.conversation', function(e){
                e.preventDefault();
                // treat it as if it was like clicking the title
                var conversationUrl = $(this).find('.post-name a').attr('href').replace(/\?/,'#');
                window.location = conversationUrl.substr(conversationUrl.indexOf('#'));
                return false;
            });

            context.view.closest('.conversation-browser-column').find('.new').on('click', function(e){
                e.preventDefault();
                messaging.publish(events.conversationAdd)
                return false;
            });

            // block scrolling of outer
            blockScrollingWhileOver(context.view);
        }

        function handleResize(context) {
            var viewContainer = context.view.parent();
            context.nameContainer = context.nameContainer || $('.content-list-name.conversations', viewContainer);
            context.view.css({ height: (viewContainer.height() - context.nameContainer.height()) });
        }

        return function(options) {
            var context = $.extend({}, defaults, options || {});
            context.pageIndex = 0;
            context.view = $(context.view);

            // set up event handlers
            handleEvents(context);

            function raiseConversationCountChange() {
                messaging.publish(events.conversationCountChanged, { count: context.view.find('li.content-item.conversation').length });
            }

            return {
                // replaces all the content of the view
                // if no content passed, re-initiates with the view's current contents

                render: function(renderedConversations) {
                    handleResize(context);
                    context.pageIndex = 0;
                    context.hasMore = true;
                    context.pagedContentAttempts = 0;
                    if(renderedConversations)
                        context.view.html(renderedConversations);
                    context.hasMore = context.view.find('li.content-item.conversation:last').data('hasmore');
                    raiseConversationCountChange();
                    pageUntilScrolls(context);
                },
                append: function(renderedConversations) {
                    $(renderedConversations).each(function(){
                        // only append conversations to the list which aren't already in the list
                        if($(this.id).length === 0) {
                            context.view.append(this);
                        }
                    });
                    raiseConversationCountChange();
                    context.hasMore = context.view.find('li.content-item.conversation:last').data('hasmore');
                    pageUntilScrolls(context);
                },
                removeConversation: function(conversationId) {
                    context.view.find('li[data-conversationid="' + conversationId + '"]').remove();
                    raiseConversationCountChange();
                },
                viewing: function(conversationId) {
                    context.view.find('li.selected').removeClass('selected')
                    context.view.find('li[data-conversationid="' + conversationId + '"]').addClass('selected')
                    context.view.find('li[data-conversationid="' + conversationId + '"]').removeClass('unread')
                },
                scale: function() {
                    handleResize(context);
                }
            };
        };
    })();

    /*
     * MessageListView
     * Constructor:
     *   view
     * Events
     *   paged (pageIndex)
     *   deleted (conversationId)
     *   replied (conversationId, body)
     *   added
     * Methods
     *   render(conversationId, renderedMessages)
     *   prepend(conversationId, renderedMessages)
     *   append(conversationId, renderedMessages)
     *   hide()
     *   scale()
     *   setCurrentConversationCount(count)
     */
    var MessageListView = (function(){
        var defaults = {
            view: {
                list: '',
                deleteLink: '',
                subject: '',
                replyLink: '',
                replyForm: '',
            },
            currentConversationCount: 0
        };

        // determine if this content is enough to cause scrolling
        // if not, try asking for more pages until it does or max attempts reached
        function pageUntilScrolls(context) {
            if(!context.hasMore)
                return;
            var contentHeight = 0;
            context.view.list.find('li.conversation-message').each(function(){
                contentHeight += $(this).height();
            });
            if(contentHeight < context.view.list.height() && context.pagedContentAttempts < 10) {
                context.pagedContentAttempts++;
                context.pageIndex++;
                messaging.publish(events.messageListPaged, {
                    pageIndex: context.pageIndex,
                    conversationId: context.conversationId,
                    scrollToBottom: true
                });
            } else {
                context.pagedContentAttempts = 0;
            }
        }

        function handleEvents(context) {
            // block scrolling of outer sections
            blockScrollingWhileOver(context.view.list);

            // infinite paging when scrolling up
            context.view.list.on('scrolltop', function(){
                if(!context.hasMore)
                    return;
                context.pageIndex++;
                messaging.publish(events.messageListPaged, {
                    pageIndex: context.pageIndex,
                    conversationId: context.conversationId
                });
            });

            // clicking delete
            context.view.deleteLink.on('click', function(e){
                e.preventDefault();
                if(confirm(context.confirmDelete)) {
                    messaging.publish(events.conversationDeleted, {
                        conversationId: context.conversationId
                    });
                }
                return false;
            });

            // clicking reply link
            context.view.replyLink.on('click', function(e){
                e.preventDefault();
                // get the body
                var body = $.trim(context.messageReplyContent());
                if(body.length === 0)
                    return;
                // add reply
                context.messageReplyClear();
                messaging.publish(events.messageAdded, {
                    conversationId: context.conversationId,
                    body: body
                });
                return false;
            });
        }

        function scale(context) {
            var headerHeight = context.view.header.height(),
                containerHeight = context.view.wrapper.height();

            context.view.list.css({ height: (containerHeight - headerHeight - (context.view.list.outerHeight() - context.view.list.height())) + 'px' })
        }

        return function(options) {
            var context = $.extend({}, defaults, options || {}),
                viewWrapper = $(context.view);
            context.pageIndex = 0;

            // capture view pieces
            context.view = {
                wrapper: viewWrapper,
                list: viewWrapper.find('.content-list.messages'),
                deleteLink: viewWrapper.find('.delete'),
                replyLink: viewWrapper.find('a.add-reply'),
                subject: viewWrapper.find('.subject'),
                unselectedMessage: viewWrapper.find('.nodata.select'),
                noMessages: viewWrapper.find('.nodata.empty'),
                header: viewWrapper.find('.content-list-header.messages'),
                replyForm: viewWrapper.find('.reply-form'),
                replyFormFields: viewWrapper.find('.field-list.reply')
            }

            // set up event handlers
            handleEvents(context);

            // small helper to show conversation-specific ui elements for message list view
            function show() {
                context.view.unselectedMessage.hide();
                context.view.noMessages.hide();
                context.view.list.show();
                context.view.deleteLink.show();
                context.view.subject.show();
                context.view.replyForm.show();
            }

            // small helper to hide conversation-specific ui elements in message list view
            function hide() {
                context.view.list.hide();
                context.view.deleteLink.hide();
                context.view.subject.hide();
                context.view.replyForm.hide();
                if(context.currentConversationCount > 0) {
                    context.view.unselectedMessage.show();
                } else {
                    context.view.noMessages.show();
                }
            }

            return {
                // replaces all the content of the view
                // if no content passed, re-initiates with the view's current contents
                render: function(conversationId, renderedMessages) {
                    // meta
                    context.conversationId = conversationId;
                    context.pageIndex = 0;
                    context.hasMore = true;
                    context.pagedContentAttempts = 0;
                    // hide and show parts of the view
                    if(conversationId) {
                        show();
                    } else {
                        hide();
                    }

                    // render messages
                    if(renderedMessages) {
                        //context.view.list.html(renderedMessages);
                        context.view.list.find('li.conversation-message,li.day').remove();
                        context.view.list.prepend(renderedMessages);
                    }
                    var firstMessage = context.view.list.find('li.conversation-message:first');
                    context.hasMore = firstMessage.data('hasmore');

                    pageUntilScrolls(context);

                    // set subject
                    context.view.subject.html(firstMessage.data('subject'));
                    context.view.subject.attr('title', firstMessage.data('fullsubject'))

                    scale(context);

                    // keep scrolled down or retain current scroll position
                    var viewElement = context.view.list.get(0);
                    setTimeout(function(){
                        viewElement.scrollTop = viewElement.scrollHeight;
                    }, 50);

                    context.messageReplyClear();
                },
                prepend: function(conversationId, renderedMessages, scrollToBottom) {
                    context.conversationId = conversationId;
                    var viewElement = context.view.list.get(0),
                        offsetHeight = viewElement.scrollTop;
                    renderedMessages = $(renderedMessages).toArray().reverse();
                    $(renderedMessages).each(function(i){
                        // only append conversations to the list which aren't already in the list
                        var id = this.id,
                            isDay = $(this).is('.day');
                        if(isDay || (id && $('#' + id).length === 0)) {
                            context.view.list.prepend(this);
                            offsetHeight += $(this).outerHeight(true);
                        }
                    });
                    // keep scrolled down or retain current scroll position
                    viewElement.scrollTop = scrollToBottom ? viewElement.scrollHeight : offsetHeight;

                    context.hasMore = context.view.list.find('li.conversation-message:first').data('hasmore');

                    pageUntilScrolls(context);
                },
                append: function(conversationId, renderedMessages) {
                    context.conversationId = conversationId;
                    var viewElement = context.view.list.get(0);
                    $(renderedMessages).each(function(){
                        var isDay = $(this).is('.day');
                        // only append conversations to the list which aren't already in the list
                        if(!isDay && $('#' + this.id).length === 0) {
                            context.view.list.find('li.reply-form').before(this);
                            // keep scrolled down
                            viewElement.scrollTop = viewElement.scrollHeight;
                        }
                    });
                },
                hide: function() {
                    hide();
                    context.conversationId = false;
                    context.conversationList = null;
                    context.messageReplyClear();
                },
                scale: function() {
                    scale(context);
                },
                setCurrentConversationCount: function(count) {
                    context.currentConversationCount = count;
                }
            };
        };
    })();

    /*
     * ConversationController
     * Constructor:
     *   context
     */
    var ConversationController = (function(){

        // refrehses the data in conversation and message lists
        function refresh(context) {
            context.model.list().done(function(renderedConversations){
                context.conversationListView.render(renderedConversations);
                if(context.conversationId)
                    context.conversationListView.viewing(context.conversationId);
            });
            if(context.conversationId) {
                context.model.get(context.conversationId, 0).done(function(renderedMessages){
                    context.messageListView.append(context.conversationId, renderedMessages);
                    messaging.publish(events.messageRead, {
                        conversationId: context.conversationId
                    });
                });
            }
        }

        function handleEvents(context) {
            var loadingConversations = false,
                loadingMessages = false;

            // conversation list paging
            messaging.subscribe(events.conversationListPaged, function(data) {
                if(loadingConversations)
                    return;
                loadingConversations = true;

                context.model.list(data.pageIndex).then(function(pageContent){
                    loadingConversations = false;
                    context.conversationListView.append(pageContent);
                    context.conversationListView.viewing(context.conversationId);
                });
            });

            // message list paging
            messaging.subscribe(events.messageListPaged, function(data) {
                if(loadingMessages)
                    return;
                loadingMessages = true;
                context.model.get(data.conversationId, data.pageIndex).then(function(pageContent){
                    loadingMessages = false;
                    context.messageListView.prepend(data.conversationId, pageContent, data.scrollToBottom || data.pageIndex === 0);
                });
            });

            // conversation deleting
            messaging.subscribe(events.conversationDeleted, function(data) {
                context.model.del(data.conversationId).then(function(){
                    context.conversationId = null;
                    context.messageListView.hide();
                    context.conversationListView.removeConversation(data.conversationId);
                });
            });

            // starting a new conversation
            messaging.subscribe(events.conversationAdd, function(){
                $.glowModal(context.addConversationModalUrl, {
                    width: 550,
                    height: 360
                });
            });

            // replying to a conversation
            messaging.subscribe(events.messageAdded, function(data) {
                context.model.reply(data.conversationId, data.body).then(function(){
                    refresh(context);
                });
            });

            // receiving a conversation notification
            messaging.subscribe(events.notificationRaised, function(data) {
                if(data.typeId === context.conversationNotificationTypeId) {
                    refresh(context);
                }
            });

            messaging.subscribe(events.chatReceived, function(data){
                refresh(context);
            });

            // loading of conversations via hashchange
            $(window).bind('hashchange', function() {
                loadAnyRequestedConversation(context);
            });

            // conversation count changed
            messaging.subscribe(events.conversationCountChanged, function(data){
                context.messageListView.setCurrentConversationCount(data.count);
            });
        };

        function loadAnyRequestedConversation(context) {
            var hashdata = $.telligent.evolution.url.hashData();
            if(hashdata.ConversationID) {
                context.conversationId = hashdata.ConversationID;
                context.model.get(hashdata.ConversationID, 0).then(function(renderedMessages){
                    context.messageListView.render(hashdata.ConversationID, renderedMessages);
                    messaging.publish(events.messageRead, {
                        conversationId: hashdata.ConversationID
                    });
                });
                context.conversationListView.viewing(hashdata.ConversationID);
            }
        }

        return function(options) {
            var context = options.context;
            // context.model
            // context.conversationListView
            // context.messageListView

            handleEvents(context);

            return {
                init: function() {
                    context.conversationListView.render();
                    context.messageListView.render();
                    loadAnyRequestedConversation(context);
                }
            };
        };
    })();

    var deleteConversation = function(context, conversationId) {
            return $.telligent.evolution.put({
                url: context.deleteUrl,
                data: { ConversationId: conversationId },
                success: function(response) {
                    window.location = context.conversationsUrl;
                }
            });
        },
        initSimpleView = function(context) {
            // starting conversations
            $(context.wrapperSelector + ' a.start-conversation').live('click', function(){
                var url = $(this).attr('href');
                Telligent_Modal.Open(url,550,360,null);
                return false;
            });

            var init = function() {
                $(context.wrapperSelector)
                    .find('.internal-link.delete-conversation')
                    .click(function(){
                        if (confirm(context.deleteConversationText)) {
                            var p = $(this).parents('.content-item');
                            if (p.length > 0) {
                                var id = p.attr('data-conversation-id');
                                deleteConversation(context, id);
                            } else {
                                p = $(this).parents('.table-item');
                                if (p.length > 0) {
                                    var id = p.attr('data-conversation-id');
                                    deleteConversation(context, id);
                                }
                            }
                        }
                        return false;
                    });
            };
            $.telligent.evolution.messaging.subscribe(context.pagedMessage, init);
            init();
        },
        resizeBrowser = (function(){
            var toMeasure = null,
                pageFragments = null;
            return function(context) {
                var total = 0;
                pageFragments = pageFragments || $('.content-fragment-page');
                toMeasure = $(toMeasure || $.map(['admin-bar-header','admin-bar','admin-bar-footer','header-fragments-header','header-fragments','header-fragments-footer'], function(sel){
                    return $('.' + sel);
                })).each(function(){
                    total += $(this).outerHeight(true);
                });

                total += (pageFragments.outerHeight(true) - pageFragments.innerHeight());

                $(context.conversationBrowser).height(($(window).innerHeight() - total) * .98);
            };
        })(),
        blockScrollingWhileOver = function(element) {
            var el = $(element),
                verticalPadding = el.outerHeight() - el.height();

            el.on('mouseenter', function(){
                el.on('DOMMouseScroll mousewheel', function(e){
                    var e = e || window.event,
                        originalEvent = e.originalEvent || e,
                        delta = 0,
                        scrollTop = el.get(0).scrollTop,
                        scrollHeight = el.get(0).scrollHeight,
                        height = el.height();

                    if (originalEvent.wheelDelta)
                        delta = originalEvent.wheelDelta/120;
                    if (originalEvent.detail)
                        delta = -originalEvent.detail/3;

                    if((Math.abs(scrollTop - (scrollHeight - height)) <= verticalPadding && delta < 0) || (scrollTop === 0 && delta > 0)) {
                        if(e.preventDefault)
                            e.preventDefault();
                        e.returnValue = false;
                    }
                })
            })
            .on('mouseleave', function(){
                el.off('DOMMouseScroll mousewheel');
            });
        },
        initStandardView = function(context) {
            // set up scaled shell
            $(window).resize(function(){
                resizeBrowser(context);
                context.messageListView.scale();
                context.conversationListView.scale();
            });
            resizeBrowser(context);

            // set up model
            context.model = ConversationModel({
                conversationListUrl: context.conversationListUrl,
                messageListUrl: context.messageListUrl,
                deleteUrl: context.deleteUrl
            });

            // controller
            context.controller = ConversationController({
                context: context
            });

            // views
            context.conversationListView = ConversationListView({
                view: $(context.conversationList)
            });
            context.messageListView = MessageListView({
                view: $(context.messageList),
                confirmDelete: context.deleteConversationText,
                messageReplyContent: context.messageReplyContent,
                messageReplyClear: context.messageReplyClear,
                messageReplyFocus: context.messageReplyFocus
            });

            context.controller.init();
        }

    $.telligent.evolution.widgets.conversationList = {
        register: function(context) {
            if(context.viewType === 'simple') {
                initSimpleView(context);
            } else if(context.viewType == 'standard') {
                initStandardView(context);
            }
        }
    };
}(jQuery));
