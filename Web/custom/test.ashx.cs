﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Linq;
using System.Web;
using Omnicell.Services;
using System.Xml;
using System.Net;

namespace Omnicell.Web.custom
{
    /// <summary>
    /// Summary description for customer
    /// </summary>
    public class test : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            StringBuilder sbTrace = new StringBuilder();
            sbTrace.AppendLine("1 handler start for: " + context.Request.UserAgent.ToString() + " from: " + context.Request.UserHostAddress.ToString());

            try
            {
                //string fileName = string.Format("customer-{0:yyyy-MM-dd_hh-mm-ss-tt}.xml", DateTime.Now);

                Utilities util = new Utilities();

                string url = context.Request["url"];
                //string apiKey = context.Request.QueryString["apikey"];

                // trace - write inputstream to log
                //System.IO.StreamReader reader = new System.IO.StreamReader(context.Request.InputStream);
                //string requestFromPost = reader.ReadToEnd();
                //string fileNameWithPathx = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + "customerdata.log");
                //using (StreamWriter sw = File.AppendText(fileNameWithPathx))
                //{
                //    sw.WriteLine(requestFromPost);
                //}

                sbTrace.AppendLine("2 get api key");


                //if (apiKey != "gmjak34qvtveqvyg0ua")
                //{
                //    sbTrace.AppendLine("2.1 invalid api key");

                //    context.Response.Write("invalid api key");
                //}
                //else
                //{
                //    sbTrace.AppendLine("2.2 api key ok");
                //    sbTrace.AppendLine("3 ParseCustomerXmlFile InputStream: ");
                //    var stream = context.Request.InputStream;
                //    stream.Position = 0;
                //     var result = util.ParseCustomerXmlFile(stream);
                //    // var result = util.ParseCustomerXmlFile(context.Request.GetBufferlessInputStream());
                //    context.Response.Write(result);

                //    sbTrace.AppendLine("4 ParseCustomerXmlFile result: " + result.ToString());

                    /*
                    foreach (string file in context.Request.Files)
                    {
                        HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
                        if (hpf.ContentLength == 0)
                            continue;

                        // util.UploadDataFeed(hpf, fileName);
                        var result = util.ParseCustomerXmlFile(hpf.InputStream);
                        context.Response.Write(result);
                    }
                     */
                var hpf = context.Request.Files[0] as HttpPostedFile;
                var stream = hpf.InputStream;
                var reader = new System.IO.StreamReader(stream);
                var requestFromPost = reader.ReadToEnd();
                reader.BaseStream.Position = 0;
                stream.Position = 0;
                //var string = stream.R
                //var uri = new Uri(url, UriKind.Relative);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.UserAgent = "HUNT JONEZ";
                request.ContentLength = requestFromPost.Length;
                //request.ContentType = "application/x-www-form-urlencoded"; // or whatever - application/json, etc, etc
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());

                requestWriter.Write(stream);
                requestWriter.Close();
                requestWriter = null;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    context.Response.Write(sr.ReadToEnd());
                }
                    context.Response.Write("completed.");
                //}

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Message:  {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

                sbTrace.AppendLine("4.5 ParseCustomerXmlFile error: " + sb.ToString());

                context.Response.Write(sb.ToString());
            }

            // write trace log
            string fileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + "customer.log");
            using (StreamWriter sw = File.AppendText(fileNameWithPath))
            {
                sw.WriteLine(sbTrace.ToString());
            }

        }


        //private bool ParseCustomerXmlFile(Stream fileStream)
        //{
        //    try
        //    {
        //        var now = DateTime.Now;
        //        var reader = new XmlTextReader(fileStream);
        //        reader.WhitespaceHandling = WhitespaceHandling.None;
        //        reader.Read();
        //        using (var Db = new OmnicellEntities())
        //        {
        //            while (!reader.EOF)
        //            {
        //                if (reader.Name == "Customers" && !reader.IsStartElement())
        //                {
        //                    break;
        //                }

        //                while (reader.Name != "Customer" || !reader.IsStartElement())
        //                {
        //                    reader.Read(); // advance to <Customer> tag
        //                }

        //                reader.Read(); // advance to CSN tag

        //                var csn = reader.ReadElementString("CSN");

        //                var customer = Db.Customers.SingleOrDefault(x => csn.Equals(x.CSN));

        //                if (customer == null)
        //                {
        //                    customer = new Customer
        //                    {
        //                        CSN = csn
        //                    };
        //                    Db.Customers.AddObject(customer);
        //                }

        //                customer.IsActive = true;
        //                customer.Updated = now;
        //                customer.AccountName = reader.ReadElementString("Account");
        //                customer.City = reader.ReadElementString("City");
        //                customer.State = reader.ReadElementString("State");
        //                customer.BusinessPartnerType = reader.ReadElementString("BusinessPartnerType");
        //                customer.IDN = int.Parse(reader.ReadElementString("IDN"));
        //                customer.IDName = reader.ReadElementString("IDNName");
        //                customer.GPO = int.Parse(reader.ReadElementString("GPO"));
        //                customer.GPOName = reader.ReadElementString("GPOName");

        //                // now we should be at the </row> tag

        //                reader.Read(); // now at either </rows> or <row>
        //            }

        //            var missingCustomers = Db.Customers.Where(x => x.IsActive && x.Updated != now);
        //            foreach (var missingCustomer in missingCustomers)
        //            {
        //                missingCustomer.Updated = now;
        //                missingCustomer.IsActive = false;
        //            }

        //            Db.SaveChanges();
        //        }

        //        reader.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        // log the problem
        //        return false;
        //    }

        //    return true;
        //}

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}